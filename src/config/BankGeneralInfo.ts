import sbmBankLogo from "assets/Banks/BankLogo/SBM_logo_260x73.png";

import sbmCardLogo from "assets/Banks/CardLogo/sbmLogo.png";
import bobCardLogo from "assets/Banks/CardLogo/BOBLogo.png";
import csbCardLogo from "assets/Banks/CardLogo/CSBLogo.png";
import fedralCardLogo from "assets/Banks/CardLogo/FEDERALLogo.png";
import sibCardLogo from "assets/Banks/CardLogo/sibLogo.png";

import bobBankLogo from "assets/Banks/BankLogo/BOB_bank_logo_updated.png";
import csbBankLogo from "assets/Banks/BankLogo/CSB_logo_260x73.png";
import fedralBankLogo from "assets/Banks/BankLogo/Federal_logo_260x73.png";
import sibBankLogo from "assets/Banks/BankLogo/SIB_logo_260x73.png";
import idbiBankLogo from "assets/Banks/BankLogo/Indian_bank_logo.png";

export interface bankModel {
  [key: string]: string;
}
// SBM : 3650712217 ; SIB : 4165374739 ; BFSL : 1877335677 ; FED : 9916649609 ; CSB : 6451955689
export const BankInformation: bankModel = {
  "3650712217": sbmBankLogo,
  "1877335677": bobBankLogo,
  "6451955689": csbBankLogo,
  "9916649609": fedralBankLogo,
  "4165374739": sibBankLogo,
  "8964592742": idbiBankLogo,
  fpl: sibBankLogo,

  //for uat below data is used
  "1112048109": sbmBankLogo,
  "9484504852": bobBankLogo,
  "6650082059": csbBankLogo,
  "6971008863": fedralBankLogo,
  "6981008863": sibBankLogo,
  "5275555304": idbiBankLogo,
};
export const BankCardInformation: bankModel = {
  "3650712217": sbmCardLogo,
  "1877335677": bobCardLogo,
  "6451955689": csbCardLogo,
  "9916649609": fedralCardLogo,
  "4165374739": sibCardLogo,

  //for uat below data is used
  "1112048109": sbmCardLogo,
  "9484504852": bobCardLogo,
  "6650082059": csbCardLogo,
  "6971008863": fedralCardLogo,
  "6981008863": sibCardLogo,
};

// <!-- BANK -    PROD    -     UAT    -->
// <!-- SBM  - 3650712217 - 1112048109 -->
// <!-- SIB  - 4165374739 - 6981008863 -->
// <!-- FED  - 9916649609 - 6971008863 -->
// <!-- BOB  - 1877335677 - 9484504852 -->
// <!-- CSB - 6451955689 - 6650082059 -->
