export const base_prefix = `${process.env.REACT_APP_PUBLIC_URL || ""}/`;
export const bre_platform_Hosted = `${process.env.REACT_APP_BRE_PLATFORM || ""}`;
export const base_prefix_for_api = `${process.env.REACT_APP_BRE_URL || ""}`;
export const base_Host_path = `${process.env.REACT_APP_URL || ""}`;
