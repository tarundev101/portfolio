import React from "react";
import "./App.css";
import AppNavigation from "modules/navigation/views/AppNavigation";
import "components/common/style/hover.css";
function App() {
  return (
    <>
      <AppNavigation />
    </>
  );
}

export default App;
