import { base_prefix_for_api } from "config/common.config";

const baseUrl = base_prefix_for_api ? `${base_prefix_for_api}/BRE/` : "";
const baseUrlForTest = base_prefix_for_api
  ? `${base_prefix_for_api}/BRE_TEST/`
  : "";
const API_URLS = Object.freeze({
  graph_Data_url: (path: string) => `${baseUrl}policy/${path}`,
  lock_unlock_policy_url: () => `${baseUrl}policy/update/lockStatus`,

  login_setup_For_Bank: () => `${baseUrl}api/1/session/setup`,
  // login_setup_For_OnePanel: () => `https://int.fplabs.tech/onepanel/auth/me`,//  for  uncomment this

  login_setup_For_OnePanel: () => `${baseUrl}onepanel/auth/me`, //for live comment this

  // login_setup_For_OnePanel: () => `onepanel/auth/me`, //for live uncomment this

  // api/1/session/setup

  folder_list_url: (path: string) => `${baseUrl}policy${path}`,
  // `${baseUrl}policy/cerebro/bob/CC/cibil/policies`,

  saveDraft: (path: string) => `${baseUrl}policy/${path}/updatePolicy`,
  // simulatorTestRun: () => `${baseUrl}decisions`, //for live comment this
  simulatorTestRun: () => `${baseUrlForTest}decisions`,
  reviewerListUrl: (path: string) => `${baseUrl}policy/${path}/reviewers`,
});

export default API_URLS;
