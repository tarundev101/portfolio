// import React, { useState, createContext } from 'react'
// import { createPortal } from 'react-dom'
// import Snackbar from '@mui/material/Snackbar'
// import CloseIcon from '@mui/icons-material/Close'
// import { IconButton } from '@mui/material'
// export const ToastContext = createContext<any>(null)

// export const ToastProvider = (props: any) => {
//   const [toasts, setToasts] = useState<string | undefined | null>(null)

//   const open = (content: string) => {
//     setToasts(content)
//     if (toasts !== null || toasts != '' || toasts !== undefined) {
//       hideToast()
//     }
//   }
//   const hideToast = () => {
//     setTimeout(() => {
//       setToasts(null)
//     }, 2000)
//   }
//   const handleClose = () => {
//     setToasts(null)
//   }

//   const action = (
//     <>
//       <IconButton
//         size="small"
//         aria-label="close"
//         color="inherit"
//         onClick={handleClose}
//       >
//         <CloseIcon fontSize="small" />
//       </IconButton>
//     </>
//   )

//   return (
//     <ToastContext.Provider value={{ showToast: open }}>
//       {props.children}

//       {createPortal(
//         <div>
//           <Snackbar
//             open={toasts === null ? false : true}
//             message={toasts}
//             action={action}
//             anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
//           />
//         </div>,
//         document.body
//       )}
//     </ToastContext.Provider>
//   )
// }
