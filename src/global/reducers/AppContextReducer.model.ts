import { loginCredentialType } from "modules/navigation/api/models/Auth.model";

export enum AppContextActions {
  SET_LOGIN_CREDENTIAL,
}
export type AppStateType = {
  login: Partial<loginCredentialType> | null;
};
