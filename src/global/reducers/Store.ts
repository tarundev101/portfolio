import { create } from "zustand";
interface IContextAppType {
  selectedSectionIndex: number;
}
const useContextForBre = create<IContextAppType>((set) => {
  return {
    selectedSectionIndex: 0,
  };
});
export const setSectionIndex = (value: number) => {
  useContextForBre.setState({ selectedSectionIndex: value });
};

export const getSectionIndex = () =>
  useContextForBre.getState()["selectedSectionIndex"];
