import React, { Dispatch } from "react";
import ContextCreator, { Action } from "../../utilities/context/ContextCreator";
import { AppContextActions, AppStateType } from "./AppContextReducer.model";

const initialAppState = (): AppStateType => {
  return {
    login: null,
  }; // TODO: After integration of Bank Apis put initial value of adminType to null
};

const appReducer = (state: AppStateType, action: Action): AppStateType => {
  switch (action.type) {
    case AppContextActions.SET_LOGIN_CREDENTIAL:
      return {
        ...state,
        login: action.payload as AppStateType["login"],
      };

    default:
      return state;
  }
};

const setLogin = (dispatch: Dispatch<Action>) => {
  return (login: AppStateType["login"]) => {
    dispatch({
      type: AppContextActions.SET_LOGIN_CREDENTIAL,
      payload: login,
    });
  };
};

export const { Context, Provider } = ContextCreator(
  appReducer,
  {
    setLogin,
  },
  initialAppState()
);
