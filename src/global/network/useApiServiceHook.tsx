import { AxiosError } from "axios";
import { useCallback, useEffect, useMemo, useState } from "react";
import { apiHookErrorType, apiResponseType } from "./apiUtilities.types";

function useApiServiceHook<T>(
  initialLoading = false
  // initialError = { hasError: false }
) {
  const controller = useMemo(() => new AbortController(), []);
  const [isLoading, setIsLoading] = useState(initialLoading);
  const [error, setError] = useState<{
    hasError: boolean;
    error?: string;
    status?: number | undefined;
  } | null>(null);

  const [response, setResponse] = useState<
    undefined | (apiHookErrorType & { data?: T })
  >(undefined);

  useEffect(() => {
    return () => controller.abort();
  }, [controller]);

  const getData = useCallback(
    async (apiCallMethod: () => Promise<apiResponseType<T>>) => {
      try {
        setIsLoading(true);

        const responseData = await apiCallMethod();
        if (responseData.error != null) {
          const errorResponse = {
            hasError: true,
            error: responseData.error,
            status: responseData.status,
          };
          setResponse(errorResponse as apiHookErrorType & { data?: T });
          setError(errorResponse);
          setIsLoading(false);
          return errorResponse as apiHookErrorType & { data?: T };
        }
        setError(null);
        setResponse({ data: responseData.data } as apiHookErrorType & {
          data?: T;
        });
        setIsLoading(false);
        return { data: responseData.data } as apiHookErrorType & { data?: T };
      } catch (error) {
        const axiosError = error as AxiosError;
        const errorResponse = {
          hasError: true,
          error: axiosError.message,
          status: axiosError.response?.status,
        };
        setError(errorResponse);
        setIsLoading(false);
        return errorResponse as apiHookErrorType & { data?: T };
      } finally {
        setIsLoading(false);
      }
    },
    []
  );

  return [getData, response, isLoading, error] as const;
}

export default useApiServiceHook;
