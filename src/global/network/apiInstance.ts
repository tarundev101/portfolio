import axios from "axios";

const BASE_URL = process.env.REACT_APP_URL;

const apiInstance = axios.create({
  baseURL: BASE_URL,
  timeout: 90000,
  headers: {
    "fpl-auth-user": "sainath.latkar@fplabs.tech",
    Authorization: "Basic aWJmZnVzZXJuYW1lOmliZmZwYXNzd29yZA==",
  },
});

export default apiInstance;
