export type apiResponseType<T> = {
  status: number;
  data?: T;
  error?: string;
};

export type apiHookErrorType = {
  status: number;
  error?: string;
  hasError?: boolean;
};
