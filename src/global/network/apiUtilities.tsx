import { AxiosResponse } from "axios";
import { isEmpty } from "../../utilities/validations/validationUtilities";
import { apiResponseType } from "./apiUtilities.types";
// import apiInstance from "./apiInstance";
import axios from "axios";
import apiInstance from "./apiInstance";

export enum ApiErrorsEnum {
  NO_DATA = "NO_DATA",
  API_ERRORR = "API_ERROR",
}
const BASE_URL = process.env.REACT_APP_URL || "";

// export const apiInstance = () => {
//   return axios.create({
//     baseURL: BASE_URL,
//     timeout: 30000,
//     headers: {
//       // name: `try`,
//       // name: `${AppState.policyId}try`,
//     },
//   });
// };

// apiInstance().interceptors.request.use(
//   (config) => {
//     // if (config && config.url !== '/api/1/onex/home/search') {
//     const configs = {
//       // headers: {
//       //   "Content-Type": "application/json",
//       // },
//       // "Content-Type": "application/json",
//       // name: "tarun",
//     };
//     config.headers = {
//       ...config.params,
//       ...configs,
//     };
//     // }

//     return config;
//   },
//   (error) => {
//     return Promise.reject(error);
//   }
// );
export const handleApiResponse = (
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  response: AxiosResponse<any, any>,
  successCodes: number[] = [200],
  checkForEmptyData = true
): apiResponseType<typeof response.data> => {
  try {
    if (successCodes.includes(response.status)) {
      if (checkForEmptyData && isEmpty(response.data)) {
        throw new Error(ApiErrorsEnum.NO_DATA);
      }
      return {
        status: response.status,
        data: response.data,
      };
    }
    throw new Error(ApiErrorsEnum.API_ERRORR);
  } catch (error) {
    const apiError = error as { message: string };
    response.status;
    return {
      status: response.status,
      error: apiError.message,
    };
  }
};

interface wrapperForMethod {
  method?: "get" | "post";
  url: string;
  params?: {
    [key: string]: string | number | Date | undefined;
  };
  header?: {
    [key: string]: string | number | Date | undefined;
  };
  body?: any;
  statuscode?: number[];
  otherParams?: any;
}
interface getMethodType extends wrapperForMethod {
  url: string;
  params?: wrapperForMethod["params"];
}
interface postMethodType extends wrapperForMethod {
  url: string;
  params?: wrapperForMethod["params"];
  body: wrapperForMethod["body"];
  header: wrapperForMethod["header"];
}
const headerForCerebro = {
  user: "tarun",
  role: "admin",
  tenantId: "1234567",
};
// const getAppState = () => {
//   const { state: appState } = useContext(AppContext);
//   return appState;
// };
export const wrapperForApiMethod = ({
  method,
  url,
  params,
  header,
  body,
  statuscode,
  otherParams,
}: wrapperForMethod) => {
  if (method == "post") {
    return postMethod({ url, params, header, body, statuscode, otherParams });
  }
  return getMethod({ url, params, header, statuscode, otherParams });
};

const getMethod = ({
  url,
  statuscode,
  header,
  params,
  otherParams,
}: getMethodType) => {
  const statuscodeForGet = statuscode || [200];
  return async () => {
    return handleApiResponse(
      await apiInstance.get(url, {
        params,
      }),
      [...statuscodeForGet]
    );
  };
};
const postMethod = ({
  url,
  statuscode,
  params,
  body,
  header,
  otherParams,
}: postMethodType) => {
  const statuscodeForPost = statuscode || [200];

  return async () => {
    return handleApiResponse(
      await apiInstance.post(url, body, { params, ...otherParams }),
      [...statuscodeForPost]
    );
  };
};
