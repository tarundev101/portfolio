/* eslint-disable no-empty */
// import { UrlArrForSearch } from "modules/searchUrl/utils/UrlArray.util";
// import useNavigateForNestedTabController from "navigation/views/NavigationForNestedTab.controller";
import { base_prefix } from "config/common.config";
import { useCallback, useEffect, useState } from "react";
import { onInPutEnterClickType } from "components/common/dropDown/OneDropDown.model";
import useNavigateForNestedTabController from "modules/navigation/views/NavigationForNestedTab.controller";
import { UrlArrForSearch } from "modules/navigation/utils/UrlArray.util";
export interface useAppLayoutType {
  data?: any;
}
function useAppLayoutController() {
  const [showSearchUrlInput, setShowSearchUrlInput] = useState(false);
  const { NavigateForNestedTab } = useNavigateForNestedTabController();
  const cmdKFunction = useCallback((event: any) => {
    if (event.code == "KeyK" && event.metaKey == true) {
      setShowSearchUrlInput(true);
    }
  }, []);

  useEffect(() => {
    document.addEventListener("keydown", cmdKFunction, false);
    return () => {
      document.removeEventListener("keydown", cmdKFunction, false);
    };
  }, [cmdKFunction, showSearchUrlInput]);

  const handleSelectedInput = ({
    value,
    selectedIndex,
    filteredDataForValue,
  }: onInPutEnterClickType) => {
    if (selectedIndex !== -1) {
      const UrlIndex: number | undefined =
        typeof selectedIndex == "string"
          ? parseInt(selectedIndex)
          : selectedIndex;
      if (
        UrlIndex &&
        filteredDataForValue &&
        filteredDataForValue.length > 0 &&
        UrlIndex < filteredDataForValue.length
      ) {
        NavigateForNestedTab(
          `${base_prefix}${filteredDataForValue[UrlIndex].value}`
        );
      }
    } else {
      if (filteredDataForValue && filteredDataForValue.length > 0) {
        NavigateForNestedTab(`${base_prefix}${filteredDataForValue[0].value}`);
      }
    }

    setShowSearchUrlInput(false);
  };
  return {
    showSearchUrlInput,
    handleSelectedInput,
    setShowSearchUrlInput,
    UrlArrForSearch,
  };
}

export default useAppLayoutController;
