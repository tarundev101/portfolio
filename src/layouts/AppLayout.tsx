import React, { useCallback, useContext, useState } from "react";
import { NavLink, Outlet } from "react-router-dom";
import {
  Collapse,
  ListItem,
  ListItemButton,
  ListItemIcon,
  ListItemText,
  styled,
} from "@mui/material";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import { NavComponent } from "components/common/sidenav/NavComponent";
import { Context as AppContext } from "global/reducers/AppContextReducer";
import { globelStyle } from "components/common/CommonStyleSheetVariable";

type SideNavListType = {
  title: string;
  sublist?: SideNavListType[];
  icon: JSX.Element;
  navLinkTo?: string;
};

type AppLayoutProps = {
  hideSideNav?: boolean;
};

const DRAWER_WIDTH = 260;

styled("main", { shouldForwardProp: (prop) => prop !== "open" })<{
  open?: boolean;
}>(({ theme, open }) => ({
  flexGrow: 1,
  transition: theme.transitions.create("margin", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  marginLeft: `0px`,
  ...(open && {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: DRAWER_WIDTH,
  }),
}));

const ListItemComponent = ({
  listItem,
  leftPadding = 2,
  closeSideNav,
}: {
  listItem: SideNavListType;
  leftPadding?: number;
  closeSideNav: () => void;
}) => {
  const [showCollapsedList, setShowCollapsedList] = useState(false);

  const addNavLink = useCallback(
    (element: JSX.Element) => {
      const listActiveStyles = {
        textDecoration: "none",
        color: "#001D6E",
      };

      const listInActiveStyles = {
        textDecoration: "none",
        color: "black",
      };

      return (
        <NavLink
          // onClick={closeSideNav}
          to={listItem.navLinkTo as string}
          style={({ isActive }) =>
            isActive ? listActiveStyles : listInActiveStyles
          }
        >
          {element}
        </NavLink>
      );
    },
    [listItem.navLinkTo]
  );

  const LIST_ITEM_TO_RENDER = (
    <>
      <ListItem key={listItem.title} disablePadding>
        <ListItemButton
          sx={{ pl: leftPadding }}
          onClick={() => setShowCollapsedList(!showCollapsedList)}
        >
          <ListItemIcon>{listItem.icon}</ListItemIcon>
          <ListItemText primary={listItem.title} />
          {listItem.sublist != null && (
            <>{showCollapsedList ? <ExpandLess /> : <ExpandMore />}</>
          )}
        </ListItemButton>
      </ListItem>
      {listItem.sublist != null && (
        <Collapse in={showCollapsedList} timeout="auto" unmountOnExit>
          {listItem.sublist.map((subListItem, index) => {
            return (
              <div key={`sublist_nav_item_${subListItem.title}_${index}`}>
                {ListItemComponent({
                  listItem: subListItem,
                  leftPadding: leftPadding + 2,
                  closeSideNav,
                })}
              </div>
            );
          })}
        </Collapse>
      )}
    </>
  );

  if (listItem.navLinkTo != null) {
    return addNavLink(LIST_ITEM_TO_RENDER);
  }
  return LIST_ITEM_TO_RENDER;
};

function AppLayout(props: AppLayoutProps) {
  const { hideSideNav = false } = props;
  const { state: AppState } = useContext(AppContext);

  return (
    <div>
      {hideSideNav ? (
        <div
          style={{
            width: "100vw",
            minHeight: "100vh",
            position: "absolute",
            top: "0",
            // overflow: "scroll",
          }}
        >
          <Outlet />
        </div>
      ) : (
        <>
          <div style={{ display: "flex" }}>
            <div
              style={{
                minWidth: "240px",
                maxWidth: "20%",
                // marginRight:'30px',
                // backgroundColor: globelStyle,
                backgroundColor: globelStyle.backgroundColor,

                borderRight: "1px solid rgb(233, 239, 255)",
              }}
            >
              <NavComponent />
            </div>

            <div
              style={{
                width: "85%",
                minWidth: "900px",
                // overflow: "scroll",
                // marginLeft:'30px',
              }}
            >
              {/* <Main> */}
              <Outlet />
              {/* </Main> */}
            </div>
          </div>
        </>
      )}
    </div>
  );
}

export default AppLayout;
