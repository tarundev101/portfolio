export type GetAmountNumType = number | string

export type GetAmountOptionsType = {
  hideCurrencySymbol?: boolean
  hideCommas?: boolean
  digitsAfterDecimal?: number
}

export type GetAmountReturnType = {
  actualAmount: string
  beforeDecimal: string
  afterDecimal: string
}
