/* eslint-disable @typescript-eslint/no-non-null-assertion */
import {
  GetAmountNumType,
  GetAmountOptionsType,
  GetAmountReturnType,
} from './numberUtilities.types'

export const CURRENCY_SYMBOL = '\u20B9'

const INITAIL_GET_AMOUNT_OPTIONS = {
  hideCurrencySymbol: false,
  hideCommas: false,
  digitsAfterDecimal: 2,
}

/*
 * fn -> getAmount
 * Eg,
 * input -> 123456.789
 * output -> { actualAmount: "1,23,456.789", beforeDecimal: "1,23,456", afterDecimal: "789", }
 */
export const getAmount = (
  num: GetAmountNumType,
  options: GetAmountOptionsType = INITAIL_GET_AMOUNT_OPTIONS
): GetAmountReturnType => {
  try {
    const { hideCurrencySymbol, hideCommas, digitsAfterDecimal } = options

    let input,
      result = ''
    if (typeof num === 'number') input = num.toString()
    else input = num
    let beforeDecimal = ''
    if (hideCommas) {
      beforeDecimal = parseFloat(input).toFixed(2).split('.')[0]
    } else {
      beforeDecimal = parseFloat(input)
        .toFixed(2)
        .replace(/(\d)(?=(\d{2})+\d\.)/g, '$1,')
        .split('.')[0]
    }
    let afterDecimal
    if (digitsAfterDecimal === 0) {
      afterDecimal = input.split('.')[1]
    } else {
      afterDecimal = parseFloat(input).toFixed(digitsAfterDecimal).split('.')[1]
    }
    afterDecimal = afterDecimal == null ? '' : `${afterDecimal}`
    if (hideCurrencySymbol) {
      result = `${beforeDecimal}${
        (digitsAfterDecimal as number) > 0 ? '.' : ''
      }${afterDecimal}`
    } else {
      result = `${CURRENCY_SYMBOL} ${beforeDecimal}${
        (digitsAfterDecimal as number) > 0 ? '.' : ''
      }${digitsAfterDecimal! > 0 ? afterDecimal : ''}`
    }
    return {
      actualAmount: result,
      beforeDecimal,
      afterDecimal,
    }
  } catch (error) {
    return {
      actualAmount: 'unknown',
      beforeDecimal: 'unknown',
      afterDecimal: 'unknown',
    }
  }
}
