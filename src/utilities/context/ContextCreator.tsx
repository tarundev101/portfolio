import React, { Dispatch, Reducer, useReducer } from 'react'

export interface Action {
  type: string | number
  payload: unknown
}

type BoundActions<T> = {
  [K in keyof T]: T[K] extends (d: Dispatch<Action>) => infer R ? R : never
}

type ContextValue<T, S> = {
  state: S
} & BoundActions<T>

// eslint-disable-next-line @typescript-eslint/ban-types
const ContextCreator = <T extends {}, S>(
  reducer: Reducer<S, Action>,
  actions: T,
  initialState: S
) => {
  const Context = React.createContext({ state: initialState } as ContextValue<
    T,
    S
  >)

  const Provider = ({
    children,
  }: {
    children?: React.ReactNode | JSX.Element
  }) => {
    const [state, dispatch] = useReducer(reducer, initialState)

    const contextValue = React.useMemo(() => {
      const boundActions = {} as BoundActions<T>
      Object.keys(actions).forEach((key) => {
        // eslint-disable-next-line @typescript-eslint/ban-ts-comment
        // @ts-ignore
        // eslint-disable-next-line @typescript-eslint/no-unsafe-assignment,@typescript-eslint/no-unsafe-call
        boundActions[key] = actions[key](dispatch)
      })

      return { state, ...boundActions }
    }, [state, dispatch])

    return <Context.Provider value={contextValue}>{children}</Context.Provider>
  }

  return { Context, Provider }
}

export default ContextCreator
