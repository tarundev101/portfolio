import { IsEmptyParam, IsNumberOptionsType } from './validationUtilities.types'

const INITIAL_IS_NUMBER_OPTIONS = {
  includeNegative: false,
  includeZero: true,
}

export const isEmpty = (value: IsEmptyParam) => {
  return (
    value === undefined ||
    value === null ||
    (typeof value === 'object' && Object.keys(value).length === 0) ||
    (typeof value === 'string' && value.toString().trim().length === 0)
  )
}

export function isEmptyAndReturn<T, K>(value: T, returnIfEmpty: K) {
  if (isEmpty(value)) {
    return returnIfEmpty as K
  }
  // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
  return value! as T
}

export const isNumber = (
  num: unknown,
  options: IsNumberOptionsType = INITIAL_IS_NUMBER_OPTIONS
): boolean => {
  const { includeNegative = false, includeZero = true } = options
  if (typeof num !== 'number') return false
  // eslint-disable-next-line no-restricted-globals
  if (isNaN(num)) return false
  if (!includeNegative && num < 0) return false
  if (!includeZero && num === 0) return false
  return true
}

export const isEmail = (email: string) => {
  const re =
    /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
  return re.test(String(email).toLowerCase())
}

export const isStringOnlyDigits = (num: string) => {
  return /^\d+$/.test(num)
}

export enum ValidateCustomerSearchInputTypeEnum {
  EMAIL = 'email',
  MOBILE_NUMBER = 'mobileNumber',
  CUSTOMER_ID = 'customerNumber',
}

export const validateCustomerSearchInput = (
  input: string
): {
  errorMessage?: string
  hasError?: boolean
  type?: ValidateCustomerSearchInputTypeEnum
} => {
  if (input.length <= 0) {
    return {
      errorMessage: 'Please enter a search value',
      hasError: true,
    }
  }
  if (!isStringOnlyDigits(input)) {
    if (!isEmail(input))
      return { errorMessage: 'Email id is not valid.', hasError: true }
    else return { type: ValidateCustomerSearchInputTypeEnum.EMAIL }
  }
  if (input.length === 10)
    return { type: ValidateCustomerSearchInputTypeEnum.MOBILE_NUMBER }
  else if (input.length === 19)
    return { type: ValidateCustomerSearchInputTypeEnum.CUSTOMER_ID }
  else
    return {
      hasError: true,
      errorMessage: 'Enter valid Phone Number or Customer ID',
    }
}
