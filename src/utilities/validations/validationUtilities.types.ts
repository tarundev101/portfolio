export type IsEmptyParam = unknown

export type IsNumberOptionsType = {
  includeNegative?: boolean
  includeZero?: boolean
}
