/* eslint-disable react/jsx-no-undef */
import {
  TableRow,
  TableCell,
  IconButton,
  Collapse,
  SxProps,
  Typography,
} from "@mui/material";

import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import React, { ReactNode, useState } from "react";
import { Theme } from "@mui/system";
import { IRotateChildren, RotateComponent } from "../../common/RotateComponent";
import imgBlackarrow from "assets/SVG/arrow_icon.svg";
import { globelStyle } from "../../common/CommonStyleSheetVariable";

interface IExpandableRow {
  children: JSX.Element | JSX.Element[] | ReactNode;
  expandComponent?: JSX.Element | ReactNode | JSX.Element[];
  otherProps?: SxProps<Theme>;
  columnSpan: number;
  expandContractTextChange?: {
    ExpandedIconText: string;
    contractedIconText: string;
  };
}

export const ExpandableTableRow = (props: IExpandableRow) => {
  const {
    children,
    expandComponent,
    otherProps = {},
    columnSpan,
    expandContractTextChange,
  } = props;
  const [isExpanded, setIsExpanded] = useState(false);

  const renderExpandContractText = () => {
    if (expandContractTextChange) {
      if (isExpanded) {
        return expandContractTextChange.ExpandedIconText || "show";
      } else {
        return expandContractTextChange.contractedIconText || "hide";
      }
    }
  };

  return (
    <>
      {expandComponent && (
        <>
          <TableRow
            sx={{
              // backgroundColor: isExpanded
              //   ? `${globelStyle.tablehovercolor}`
              //   : '',

              "&:hover": {
                // border: '1px solid #00FF00',
                // color: 'gray',
                backgroundColor: `${globelStyle.tablehovercolor}`,
                // backgroundColor: `red`,
              },
              // border: 'none',
              // ...otherProps,
            }}
          >
            {children}

            {!expandContractTextChange && (
              <TableCell padding="checkbox">
                <IconButton onClick={() => setIsExpanded(!isExpanded)}>
                  {isExpanded ? (
                    // <RotateComponent rotate={IRotateChildren.RIGHT}>
                    // <RotateComponent rotate={IRotateChildren.RIGHT}>
                    //   <ArrowComponent isWhiteColor />
                    // </RotateComponent>
                    <RotateComponent rotate={IRotateChildren.DEFAULT}>
                      <img
                        style={{
                          width: "100%",
                          height: "100%",
                          // backgroundClip: '',
                        }}
                        src={imgBlackarrow}
                        alt="imgBlackarrow"
                      ></img>
                      {/* // <ArrowComponent isWhiteColor /> */}
                    </RotateComponent>
                  ) : (
                    <RotateComponent rotate={IRotateChildren.DOWN}>
                      <img
                        style={{
                          width: "100%",
                          height: "100%",
                          // backgroundClip: '',
                        }}
                        src={imgBlackarrow}
                        alt="imgBlackarrow"
                      ></img>
                      {/* // <ArrowComponent isWhiteColor /> */}
                    </RotateComponent>
                  )}
                </IconButton>
              </TableCell>
            )}

            {expandContractTextChange && (
              <TableCell padding="checkbox">
                <Typography
                  variant="inherit"
                  sx={{
                    color: "#618cfe",
                    textDecoration: "underline",
                    textAlign: "center",
                    cursor: "pointer",
                    paddingRight: "10px",
                  }}
                  onClick={() => {
                    setIsExpanded(!isExpanded);
                    // console.log('pressed')
                  }}
                >
                  {renderExpandContractText()}
                </Typography>
              </TableCell>
            )}
          </TableRow>
          {isExpanded && (
            <TableRow
              sx={{
                backgroundColor: isExpanded ? "#F5F6F8" : "",
              }}
              // background: #F5F6F8;
            >
              <TableCell colSpan={columnSpan + 1}>{expandComponent}</TableCell>
            </TableRow>
          )}
        </>
      )}
    </>
  );
};
