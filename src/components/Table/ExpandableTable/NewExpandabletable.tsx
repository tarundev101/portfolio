/* eslint-disable @typescript-eslint/ban-types */
import {
  Box,
  SxProps,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TablePagination,
  TableRow,
  Theme,
  Typography,
} from "@mui/material";
import { makeStyles } from "@mui/styles";

import React, { ReactNode, useEffect, useState } from "react";

import { ExpandableTableRow } from "./ExpandableTableRow";
import { isEmpty } from "utilities/validations/validationUtilities";
import {
  globelStyle,
  oneFontFamily,
} from "../../common/CommonStyleSheetVariable";
export interface IExpandableColumn {
  header: string | ReactNode;
  fieldName: string;
  align?: string;
  rendercell?: (rowData: any) => any;
}
export interface IExpandableTable<T> {
  row: T[];
  column: IExpandableColumn[];
  renderComponent?: (data: any) => React.ReactNode | JSX.Element;
  ComponenttoRender?: React.ReactNode | JSX.Element;
  Styles?: SxProps<Theme>;
  RowStyles?: SxProps<Theme>;
  expandContractTextChange?: {
    ExpandedIconText: string;
    contractedIconText: string;
  };
  initialRowPerPageOption?: number;
  rowsPerPageOptions?: number[];
}

export const NewExpandabletable = <T extends {}>(
  props: IExpandableTable<T>
) => {
  const {
    column,
    row,
    initialRowPerPageOption,
    rowsPerPageOptions,
    renderComponent,
    ComponenttoRender,
    Styles = {},
    RowStyles,
    expandContractTextChange = undefined,
  } = props;
  const [rowsPerPage, setRowsPerPage] = React.useState(
    initialRowPerPageOption ? initialRowPerPageOption : 5
  );
  const [page, setPage] = React.useState(0);
  const [RowData, setRowData] = useState<any[]>([]);
  const useStyles = makeStyles((theme) => ({
    noBorder: {
      "&:last-child td, &:last-child th": {
        border: "none",
      },
    },
  }));
  const classes = useStyles();
  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };
  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  useEffect(() => {
    const trimedRows = row.slice(
      page * rowsPerPage,
      page * rowsPerPage + rowsPerPage
    );
    setRowData(trimedRows);
  }, [page, row, rowsPerPage]);

  const renderColumns = () => {
    return column.map((singleColumn, keyNumber) => (
      <TableCell
        key={`expandleRow_${keyNumber}`}
        align={"left"}
        sx={{
          color: "#191919",
          fontWeight: "500",
          fontSize: "14px",

          fontFamily: oneFontFamily.regular,
        }}
        variant="head"
      >
        <Typography variant="inherit">{singleColumn.header}</Typography>
      </TableCell>
    ));
  };

  const renderRow = () => {
    const modifiedColumn = isEmpty(expandContractTextChange)
      ? [...column]
      : [...column].slice(0, -1);
    // console.log(modifiedColumn, 'modifiedColumn')

    return RowData.map((item: any, rowno) => {
      return (
        <ExpandableTableRow
          key={`expandleRow_${rowno}`}
          expandComponent={renderComponent && renderComponent(item)}
          columnSpan={column.length}
          expandContractTextChange={expandContractTextChange}

          // otherProps={RowStyles? {...RowStyles} }
        >
          {modifiedColumn.map((singlecolumn, cellno) => {
            return (
              <TableCell
                align="left"
                key={`${cellno}_${singlecolumn.header}`}
                className={classes.noBorder}
              >
                {singlecolumn?.rendercell
                  ? singlecolumn.rendercell(item)
                  : item[singlecolumn.fieldName]
                    ? item[singlecolumn.fieldName]
                    : "-"}
              </TableCell>
            );
          })}
        </ExpandableTableRow>
      );
    });
  };
  const tableStyles = {
    // borderRadius: '10px', // Set your desired border radius here
  };
  return (
    <Box
      sx={{
        border: `1px solid  ${globelStyle.tablehovercolor} `,
        ...Styles,
      }}
    >
      <Table style={tableStyles}>
        <TableHead sx={{ borderRadius: "10px" }}>
          <TableRow
            // style={{ border: '20px' }}
            sx={{
              color: "#919eab",
              borderRadius: "10px",
              backgroundColor: `${`${globelStyle.genericColumnheader}`}`,
              "& .MuiTableRow-head": {
                borderRadius: "10px",
              },
              "& .MuiTableRow-row:nth-child(even)": {
                backgroundColor: globelStyle.alternaterowcolor,
              },
            }}
          >
            {renderColumns()}

            {isEmpty(expandContractTextChange) && (
              <TableCell padding="checkbox" variant="head" />
            )}
          </TableRow>
        </TableHead>
        <TableBody>{renderRow()}</TableBody>
      </Table>
      {/* </TableContainer> */}
      {!isEmpty(row) && (
        <TablePagination
          sx={{ backgroundColor: "#f8f8f8" }}
          rowsPerPageOptions={
            rowsPerPageOptions ? rowsPerPageOptions : [5, 10, 25]
          }
          component="div"
          count={row.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      )}
    </Box>
  );
};
