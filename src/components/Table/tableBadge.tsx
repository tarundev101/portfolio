import { OneText, OneTextProps } from "components/common/text/OneText";
import React, { ReactNode } from "react";
export interface Badgeprops {
  children?: ReactNode;
  type?: "orange" | "red" | "blue" | "green" | null | undefined;
  badgeTextStyle?: React.CSSProperties;
  colorMappingData: colorMappingDataType;
  value: string;
  size?: OneTextProps["size"];
}
export interface colorMappingDataType {
  orange?: string;
  blue?: string;
  red?: string;
  green?: string;
  yellow?: string;
}
export const tableBadgecolor: tableBadgecolorType = {
  orange: "#F08F64",
  blue: " #4A7FF7 ",
  red: "#F06464",
  green: " #18AA1B",
  yellow: "#FFD700",
};
export interface tableBadgecolorType {
  orange?: string;
  blue?: string;
  red?: string;
  green?: string;
  yellow?: string;
}
export const tableBadgeBackgroundColor = {
  orange: "#FEF4F0",
  blue: "#EDF2FE",
  red: "#FEF0F0  ",
  green: "#E8F7E8",
  yellow: "#FFD700",
};
export const TableBadge = (props: Badgeprops) => {
  const {
    children,
    type,
    value,
    size = "l",
    badgeTextStyle,
    colorMappingData,
  } = props;
  const dataKey = Object.entries(colorMappingData);

  let color = tableBadgecolor.blue;
  for (let i = 0; i < dataKey.length; i++) {
    if (value.toLowerCase().match(dataKey[i][1].toLowerCase())) {
      color = tableBadgecolor[dataKey[i][0] as keyof tableBadgecolorType];
    }
  }
  return (
    <StringContainer
      size={size}
      value={value}
      color={`${color}`}
      badgeTextStyle={badgeTextStyle ? badgeTextStyle : undefined}
    />
  );
};
const StringContainer = ({
  value,
  color,
  badgeTextStyle,
  size,
}: {
  size: Badgeprops["size"];
  value: string;
  color: string;
  badgeTextStyle: React.CSSProperties | undefined;
}) => {
  return (
    <OneText
      family="medium"
      size={size}
      textStyle={{
        color: color,
        textAlign: "center",
        display: "inline-block",
        ...badgeTextStyle,
      }}
    >
      {value}
    </OneText>
  );
};
