import React, { useContext } from "react";
import { IspopepenContext } from "./context/HideContext";

export const useHide = () => {
  const { isPopOpen } = useContext(IspopepenContext) || {};
  return { isPopOpen };
};
