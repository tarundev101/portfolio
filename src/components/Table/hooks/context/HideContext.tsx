import React, { createContext, useState } from 'react'
export const IspopepenContext = createContext<{
  isPopOpen: number
  setIspopupopen: React.Dispatch<React.SetStateAction<number>>
} | null>(null)
interface HideProps {
  children: React.ReactNode
}
const HideContext = ({ children }: HideProps) => {
  const [open, setopne] = useState(0)

  return (
    <IspopepenContext.Provider
      value={{ isPopOpen: open, setIspopupopen: setopne }}
    >
      <div
        onClick={() => {
          setopne((num) => num + 1)
        }}
      >
        {children}
      </div>
    </IspopepenContext.Provider>
  )
}

export default HideContext
