/* eslint-disable react/jsx-no-undef */
import {
  TableRow,
  TableCell,
  IconButton,
  Collapse,
  SxProps,
  Typography,
} from "@mui/material";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";
import KeyboardArrowDownIcon from "@mui/icons-material/KeyboardArrowDown";
import React, { ReactNode } from "react";
import { Theme } from "@mui/system";
import { globelStyle } from "components/common/CommonStyleSheetVariable";
interface IExpandableRow {
  children: JSX.Element | JSX.Element[] | ReactNode;
  expandComponent?: JSX.Element | ReactNode | JSX.Element[];
  otherProps?: SxProps<Theme>;
  SingleRowHeight: number | string;
  columnSpan: number;
  expandContractTextChange?: {
    ExpandedIconText: string;
    contractedIconText: string;
  };
}

export const StickyExpandableTableRow = (props: IExpandableRow) => {
  const {
    children,
    expandComponent,
    otherProps = {},
    SingleRowHeight,
    columnSpan,
    expandContractTextChange,
  } = props;
  const [isExpanded, setIsExpanded] = React.useState(false);

  const renderExpandContractText = () => {
    if (expandContractTextChange) {
      if (isExpanded) {
        return expandContractTextChange.ExpandedIconText || "show";
      } else {
        return expandContractTextChange.contractedIconText || "hide";
      }
    }
  };

  return (
    <>
      {expandComponent && (
        <>
          <TableRow
            sx={{
              backgroundColor: isExpanded
                ? `${globelStyle.tablehovercolor}`
                : "",

              "&:hover": {
                backgroundColor: `${globelStyle.tablehovercolor}`,
              },
              height: SingleRowHeight ? SingleRowHeight : "",
              ...otherProps,
            }}
          >
            {children}

            {!expandContractTextChange && (
              <TableCell padding="checkbox">
                <IconButton onClick={() => setIsExpanded(!isExpanded)}>
                  {isExpanded ? (
                    <KeyboardArrowUpIcon />
                  ) : (
                    <KeyboardArrowDownIcon />
                  )}
                </IconButton>
              </TableCell>
            )}

            {expandContractTextChange && (
              <TableCell padding="checkbox">
                <Typography
                  variant="inherit"
                  sx={{
                    color: "#618cfe",
                    textDecoration: "underline",
                    textAlign: "center",
                    cursor: "pointer",
                    paddingRight: "10px",
                  }}
                  onClick={() => {
                    setIsExpanded(!isExpanded);
                    // console.log('pressed')
                  }}
                >
                  {renderExpandContractText()}
                </Typography>
              </TableCell>
            )}
          </TableRow>
          {isExpanded && (
            <TableRow
              sx={{
                backgroundColor: isExpanded ? "#F5F6F8" : "",
              }}
            >
              <TableCell colSpan={columnSpan + 1}>{expandComponent}</TableCell>
            </TableRow>
          )}
        </>
      )}
    </>
  );
};
