/* eslint-disable no-empty */
import { useContext, useEffect, useMemo, useState } from "react";
import { Context as AppContext } from "global/reducers/AppContextReducer";
import { usePaginationFooter } from "./PaginationNumberFooter";

interface usePaginationControllerType<T> {
  numberOfItemShowing: number;
  data: T[];
}
function usePaginationController<T>({
  data,
  numberOfItemShowing,
}: usePaginationControllerType<T>) {
  // {  data,
  //     numberOfItemShowing}:usePaginationControllerType<T>
  // const{data,numberOfItemShowing}=  props
  const { renderComponent, selectedPageNumber } = usePaginationFooter({
    totalNumberOfPages: Math.ceil(data.length / numberOfItemShowing),
  });
  const [dataForUi, setDataForUi] = useState<T[]>([]);
  const HandleDataOnPageSelect = () => {
    const modifiedData = data;

    const initialIndex = (selectedPageNumber - 1) * numberOfItemShowing;

    if (data.length > numberOfItemShowing + initialIndex) {
      const trimedmodifiedData = modifiedData.slice(
        initialIndex,
        initialIndex + numberOfItemShowing
      );

      setDataForUi(trimedmodifiedData);
    } else {
      const trimedmodifiedData = modifiedData.slice(
        initialIndex,
        modifiedData.length
      );
      setDataForUi(trimedmodifiedData);
    }
  };
  useEffect(() => {
    HandleDataOnPageSelect();
  }, [selectedPageNumber, data]);
  return {
    dataForUi,
    footerSection: renderComponent,
  } as const;
}

export default usePaginationController;
