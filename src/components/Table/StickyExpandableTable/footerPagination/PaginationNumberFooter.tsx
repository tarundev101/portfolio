import React, { useEffect, useMemo, useState } from "react";
import { handleShowingNumber } from "./Utils/paginationSupportingFunction";
import {
  IRotateChildren,
  RotateComponent,
} from "components/common/RotateComponent";
import ArrowComponent from "components/common/style/ArrowComponent";
import { OneText } from "components/common/sidenav/OneText";
export interface IusePaginationNumberProps {
  totalNumberOfPages: number;
}
export const usePaginationFooter = (props: IusePaginationNumberProps) => {
  const { totalNumberOfPages } = props;
  const [selectedPageNumber, setSelectedPageNumber] = useState<number>(1);
  let mappingNumber = [1];
  const arrowSize = "26px";
  if (totalNumberOfPages == 2) {
    mappingNumber = [1, 2];
  } else if (totalNumberOfPages > 2) {
    mappingNumber = [0, 0, 0];
  }

  useEffect(() => {
    setSelectedPageNumber(1);
  }, [totalNumberOfPages]);
  const handleIncrementNumber = () => {
    if (selectedPageNumber + 1 <= totalNumberOfPages) {
      setSelectedPageNumber(selectedPageNumber + 1);
    }
  };
  const handleDecrementNumber = () => {
    if (selectedPageNumber - 1 > 0) {
      setSelectedPageNumber(selectedPageNumber - 1);
    }
  };

  const renderPaginationComponent = useMemo(() => {
    return (
      <div style={{ display: "flex", position: "relative" }}>
        <div
          style={{
            width: "26px",
            marginRight: "17px",
            cursor: "pointer",
          }}
          onClick={() => handleDecrementNumber()}
        >
          <RotateComponent rotate={IRotateChildren.DOWN}>
            <ArrowComponent
              imageStyleProp={{ marginTop: "-2px" }}
              isWhiteColor
              width={arrowSize}
              height={arrowSize}
            />
          </RotateComponent>
        </div>
        <div style={{ marginTop: "0px", display: "flex" }}>
          {handleShowingNumber(selectedPageNumber, totalNumberOfPages).map(
            (item) => {
              return (
                <OneText
                  color={item == selectedPageNumber ? "blue" : "textGrey"}
                  textStyle={{ marginRight: "17px" }}
                >
                  {item}
                </OneText>
              );
            }
          )}
        </div>
        <div
          style={{ width: "26px", cursor: "pointer", marginTop: "-4px" }}
          onClick={() => handleIncrementNumber()}
        >
          <RotateComponent rotate={IRotateChildren.DEFAULT}>
            <ArrowComponent isWhiteColor width={arrowSize} height={arrowSize} />
          </RotateComponent>
        </div>
      </div>
    );
  }, [selectedPageNumber, mappingNumber, totalNumberOfPages]);

  return {
    renderComponent: renderPaginationComponent,
    selectedPageNumber,
  } as const;
};
