export const handleShowingNumber = (
  selectedPageNumber: number,
  totalNumberOfPages: number
) => {
  const mappingNumber: number[] = []
  let selectedPageNum = selectedPageNumber
  if (totalNumberOfPages <= 0 || selectedPageNum <= 0) {
    return mappingNumber
  }

  // Ensure selectedPageNum is within valid range
  selectedPageNum = Math.min(Math.max(selectedPageNum, 1), totalNumberOfPages)

  // Always include the current selected page
  mappingNumber.push(selectedPageNum)

  // Include the previous and next pages if available
  if (selectedPageNum > 1) {
    mappingNumber.unshift(selectedPageNum - 1)
  }

  if (selectedPageNum < totalNumberOfPages) {
    mappingNumber.push(selectedPageNum + 1)
  }

  return mappingNumber
}

// For Any Bug its backup
// start
// export const handleShowingNumber = (
//   selectedPageNumber: number,
//   totalNumberOfPages: number
// ) => {
//   // console.log(selectedPageNumber)
//   let mappingNumber: number[] = []
//   if (
//     selectedPageNumber + 1 <= totalNumberOfPages &&
//     selectedPageNumber - 1 > 0
//   ) {
//     mappingNumber = [
//       selectedPageNumber - 1,
//       selectedPageNumber,
//       selectedPageNumber + 1,
//     ]
//   }
//   if (selectedPageNumber == 1 && selectedPageNumber + 2 <= totalNumberOfPages) {
//     mappingNumber = [
//       selectedPageNumber,
//       selectedPageNumber + 1,
//       selectedPageNumber + 2,
//     ]
//   }
//   if (selectedPageNumber == totalNumberOfPages && selectedPageNumber - 2 > 0) {
//     mappingNumber = [
//       selectedPageNumber - 2,
//       selectedPageNumber - 1,
//       selectedPageNumber,
//     ]
//   }
//   return mappingNumber
// }
// end
