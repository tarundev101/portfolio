/* eslint-disable @typescript-eslint/ban-types */
import {
  Box,
  Paper,
  Stack,
  SxProps,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TablePagination,
  TableRow,
  Theme,
  Typography,
} from "@mui/material";
import { makeStyles } from "@mui/styles";
import ExpandMoreIcon from "@mui/icons-material/ExpandMore";
import { tableCellClasses } from "@mui/material/TableCell";
// import { globelStyle } from "components/common/genericFunction/CommonStyleSheetVariable";
// import { globelStyle } from "components/common/CommonStyleSheetVariable";

import React, { ReactNode, useCallback, useEffect, useState } from "react";

import { isEmpty } from "utilities/validations/validationUtilities";
import { StickyExpandableTableRow } from "./StickyExpandableTableRow";
import TableFooterPagination from "./StickyFooterPagination";
export interface IExpandableColumn {
  header: string | ReactNode;
  fieldName: string;
  align?: string;
  rendercell?: (rowData: any) => any;
}
export interface IExpandableTable<T> {
  row: T[];
  column: IExpandableColumn[];
  rowPerPage?: number;
  renderComponent?: (data: any) => React.ReactNode | JSX.Element;
  ComponenttoRender?: React.ReactNode | JSX.Element;
  Styles?: SxProps<Theme>;
  RowStyles?: SxProps<Theme>;
  expandContractTextChange?: {
    ExpandedIconText: string;
    contractedIconText: string;
  };
  maxHeight: number;
  hideFooter?: boolean;
}

export const StickyExpandabletable = <T extends {}>(
  props: IExpandableTable<T>
) => {
  const {
    column,
    rowPerPage,
    row,
    renderComponent,
    ComponenttoRender,
    Styles = {},
    RowStyles = {},
    maxHeight,
    hideFooter = false,
    expandContractTextChange = undefined,
  } = props;
  const [rowsPerPage, setRowsPerPage] = React.useState(5);
  const [page, setPage] = React.useState(0);
  const [NumberOfItemDisplay, setNumberOfItemDisplay] = React.useState([
    0,
    rowsPerPage,
  ]);

  const [RowData, setRowData] = useState<any[]>([]);
  const useStyles = makeStyles((theme) => ({
    noBorder: {
      "&:last-child td, &:last-child th": {
        border: "none",
      },
    },
  }));

  const classes = useStyles();
  const tableStyle = {
    header: {
      color: "#191919",
      fontWeight: "600",
      fontSize: "14px",
      fontFamily: "Inter semiBold",
      backgroundColor: `#EDF2FE`,
    },
    scrollbar: {
      "&::-webkit-scrollbar": {
        width: "0.4em",
      },
      "&::-webkit-scrollbar-track": {
        "-webkit-box-shadow": "inset 0 0 6px rgba(0,0,0,0.00)",
      },
      "&::-webkit-scrollbar-thumb": {
        backgroundColor: "red",
        outline: "1px solid slategrey",
      },
    },
  };

  // const handleChangePage = (event: unknown, newPage: number) => {
  //   setPage(newPage)
  // }
  const handleTableChangePage = (newPage: number) => {
    setPage(newPage);
  };
  // const handleChangeRowsPerPage = (
  //   event: React.ChangeEvent<HTMLInputElement>
  // ) => {
  //   setRowsPerPage(parseInt(event.target.value, 10))
  //   setPage(0)
  // }
  useEffect(() => {
    if (rowPerPage) {
      setRowsPerPage(rowPerPage);
    }
  }, [rowPerPage]);
  const handleCheckForRow = () => {
    // for setting how much item to show on specific that page
    // row;
    if (row.length > page * rowsPerPage + rowsPerPage) {
      return page * rowsPerPage + rowsPerPage;
    } else {
      return row.length;
    }
  };
  useEffect(() => {
    const trimedRows = row.slice(
      page * rowsPerPage,
      page * rowsPerPage + rowsPerPage
    );
    setNumberOfItemDisplay([page * rowsPerPage + 1, handleCheckForRow()]);
    setRowData(trimedRows);
  }, [page, row, rowsPerPage]);

  const renderColumns = () => {
    return column.map((singleColumn, keyNumber) => (
      <TableCell
        key={`expandleRow_${keyNumber}`}
        align={"left"}
        sx={tableStyle.header}
      >
        <Typography variant="inherit">{singleColumn.header}</Typography>
      </TableCell>
    ));
  };

  const renderRow = () => {
    const modifiedColumn = isEmpty(expandContractTextChange)
      ? [...column]
      : [...column].slice(0, -1);
    return RowData.map((item: any, rowno) => {
      return (
        <StickyExpandableTableRow
          key={`expandleRow_${rowno}`}
          expandComponent={renderComponent && renderComponent(item)}
          columnSpan={column.length}
          expandContractTextChange={expandContractTextChange}
          SingleRowHeight={"74px"}
          // otherProps={Styles}
        >
          {modifiedColumn.map((singlecolumn, cellno) => {
            return (
              <TableCell
                align="left"
                key={`${cellno}_${singlecolumn.header}`}
                className={classes.noBorder}
              >
                {singlecolumn?.rendercell
                  ? singlecolumn.rendercell(item)
                  : item[singlecolumn.fieldName]
                    ? item[singlecolumn.fieldName]
                    : "-"}
              </TableCell>
            );
          })}
        </StickyExpandableTableRow>
      );
    });
  };

  return (
    <Paper
      sx={{
        width: "100%",
        // overflow: 'hidden',
        boxShadow: "none",
        height: maxHeight + 74,
      }}
    >
      <TableContainer
        sx={{
          maxHeight: maxHeight,
          borderRadius: "15px",
          border: `1px solid  #DEE4E8 `,
          position: "relative",
          ...Styles,
        }}
      >
        <Table stickyHeader>
          <TableHead
            sx={{
              borderRadius: "20px",
            }}
          >
            <TableRow>
              {renderColumns()}

              {isEmpty(expandContractTextChange) && (
                <TableCell
                  padding="checkbox"
                  variant="head"
                  sx={tableStyle.header}
                />
              )}
            </TableRow>
          </TableHead>
          <TableBody sx={{}}>{renderRow()}</TableBody>
        </Table>
        <div
          style={{
            position: "sticky",
            bottom: 0,
            width: "100%",
          }}
        >
          {!isEmpty(row) && !hideFooter && (
            <TableFooterPagination
              totalNumberOfPages={Math.ceil(row.length / rowsPerPage)}
              NumberOfItemDisplay={NumberOfItemDisplay}
              totalEntries={row.length}
              onPageChange={handleTableChangePage}
            />
          )}
        </div>
      </TableContainer>
      {/* {!isEmpty(row) && (
        <TablePagination
          sx={{ backgroundColor: '#f8f8f8' }}
          rowsPerPageOptions={[]}
          component="div"
          count={row.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      )} */}
    </Paper>
  );
};
