/* eslint-disable @typescript-eslint/no-non-null-assertion */
import { Color } from "components/common/CommonStyleSheetVariable";
// import { oneColor } from 'components/common/genericFunction/CommonStyleSheetVariable'
import React, { useEffect } from "react";
import { usePaginationFooter } from "./footerPagination/PaginationNumberFooter";

interface tableFooterProps {
  totalNumberOfPages: number;
  NumberOfItemDisplay: number[];
  totalEntries: number;
  onPageChange: (newPage: number) => void;
}
function TableFooterPagination(props: tableFooterProps) {
  const {
    totalNumberOfPages,
    NumberOfItemDisplay,
    totalEntries,
    onPageChange,
  } = props;
  const { renderComponent, selectedPageNumber } = usePaginationFooter({
    totalNumberOfPages: totalNumberOfPages,
  });
  useEffect(() => {
    onPageChange(selectedPageNumber - 1);
  }, [selectedPageNumber]);
  return (
    <div
      style={{
        height: "74px",
        position: "relative",
        backgroundColor: Color.p_gull_grey,
      }}
    >
      {/* <div
        style={{
          position: 'absolute',
          left: 16,
          right: '16px',
          bottom: '27px',
        }}
      >
        Showing results {NumberOfItemDisplay[0]} to {NumberOfItemDisplay[1]} of
        {` ${totalEntries}`} Enteries
      </div> */}
      <div
        style={{
          position: "absolute",
          right: 16,
          bottom: "22px",
        }}
      >
        {renderComponent}
      </div>
    </div>
  );
}

export default TableFooterPagination;
