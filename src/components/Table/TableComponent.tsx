import React from "react";
import True from "../../assets/SVG/True.svg";
import { Badgeprops, OneBadge } from "components/common/text/Badge";
import { OneText, OneTextProps } from "components/common/text/OneText";

export interface componentProps {
  label?: string;
  sublabel?: string;
  leftIconUrl?: string;
  rightIconUrl?: string;
  labelProps?: OneTextProps;
  sublabelProps?: OneTextProps;
  iconStyle?: React.CSSProperties;
  chips?: string;
  chipDivStyle?: React.CSSProperties;
  chipsProps?: Badgeprops;
  showChipIcon?: boolean;
  containerStyle?: React.CSSProperties;
}
const commonImgStyle = {
  width: "25px",
  height: "25px",
  borderRadius: "50%",
  margin: "0px 10px",
};

export const TableComponent = (props: componentProps) => {
  const {
    label,
    sublabel,
    leftIconUrl,
    rightIconUrl,
    containerStyle,
    labelProps = { family: "semiBold" },
    sublabelProps = {
      family: "regular",
      color: "darkGrey",
      lineHeight: "30px",
      textStyle: { textAlign: "left" },
    },
    iconStyle = {},
    chips,
    chipDivStyle,
    chipsProps,
    showChipIcon,
  } = props;
  return (
    <div style={{ display: "flex", alignItems: "center", ...containerStyle }}>
      {leftIconUrl && (
        <img
          src={leftIconUrl}
          // alt="icon"
          style={{ ...commonImgStyle, ...iconStyle }}
        />
      )}
      <div>
        <OneText {...labelProps}>{label}</OneText>
        <OneText {...sublabelProps}>{sublabel}</OneText>
      </div>
      {chips && (
        <div
          style={{
            display: "flex",
            alignItems: "center",
            // padding: '5px 10px',
            // backgroundColor: ' #EDF2FE',
            borderRadius: "15px",
            marginLeft: "20px",
            ...chipDivStyle,
          }}
        >
          {/* <OneText family="semiBold" color="blue" {...chipsProps}>
            {chips}
          </OneText> */}
          <OneBadge
            type={"blue"}
            badgeStyle={{
              fontFamily: "Inter SemiBold",
              // padding: '0px',
              // textAlign: 'left',
              // marginLeft: '0px',
            }}
            {...chipsProps}
          >
            {chips}
          </OneBadge>
          {showChipIcon && (
            <img
              src={True}
              width={"15px"}
              height="15px"
              style={{ margin: "0px 5px" }}
            />
          )}
        </div>
      )}
      {rightIconUrl && (
        <img
          src={rightIconUrl}
          // alt="icon"
          style={{ ...commonImgStyle, ...iconStyle }}
        />
      )}
    </div>
  );
};
