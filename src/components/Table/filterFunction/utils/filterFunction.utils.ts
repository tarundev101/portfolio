export const checkForEmptyOptions = (data: {
  [key: string]: string[];
}): boolean => {
  const Keys = Object.keys(data);
  let isEmpty = true;
  Keys.forEach((key) => {
    if (data[key].length > 0) {
      isEmpty = false;
      return;
    }
  });
  return isEmpty;
};

export function getFilteredData<T extends { [key: string]: any }>(
  data: T[],
  filteroptions: {
    [key: string]: string[];
  }
): T[] {
  const filteredData: T[] = [];
  data.forEach((column) => {
    const keys = Object.keys(filteroptions);
    let toAdd = true;
    for (const key of keys) {
      const optionarr = filteroptions[key];
      if (optionarr.length > 0) {
        toAdd = optionarr.includes(`${column[key]}`);
        if (!toAdd) {
          break;
        }
      }
    }
    if (toAdd) {
      filteredData.push(column);
    }
  });
  return filteredData;
}
export const getOptionsofkey = (key: string, data: any[]): string[] => {
  const option: { [key: string]: number } = {};
  data.map((item) => {
    if (item[key]) {
      option[item[key]] = 1;
    }
  });
  const result = Object.keys(option);
  return result;
};
