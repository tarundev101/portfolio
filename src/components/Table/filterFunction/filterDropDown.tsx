import { useEffect, useRef, useState } from "react";
import { filteroptionsProps } from "./MyFilter";
import Down from "assets/SVG/CircleArrowDown.svg";
import Up from "assets/SVG/CircleArrowUp.svg";
import Check from "assets/SVG/CheckBox.svg";
import Checked from "assets/SVG/Checked.svg";
import "./filter.scss";
import { useHide } from "../hooks/useHide";
import { OneText } from "components/common/text/OneText";
import { globelStyle } from "components/common/CommonStyleSheetVariable";
import { useOverLayScreen } from "components/common/OverLayScreenClose/OverlayScreenClose.screen";
interface filterboxprops {
  option: filteroptionsProps;
  setcheckeyvalue: (checkedv: {
    ischeck: boolean;
    key: string;
    value: string;
    label: string;
    index: number;
    visibleValue: string;
    selectSingleOption: boolean;
  }) => void;
  checkarray: {
    [key: string]: { [key: number]: boolean };
  };
  selectSingleOption?: boolean;
  checkedvalue: string[];
  width?: string;
}
export const FilterBox = (props: filterboxprops) => {
  const {
    option,
    setcheckeyvalue,
    checkarray,
    checkedvalue,
    selectSingleOption = false,
    width = "180px",
  } = props;
  const { isPopOpen } = useHide();
  const [open, setopen] = useState(false);
  useEffect(() => {
    setopen(false);
  }, [isPopOpen]);

  const wrapperRef = useRef<HTMLDivElement>(null);
  useOverLayScreen(wrapperRef, () => {
    setopen(false);
  });
  return (
    <div ref={wrapperRef} style={{ width: width, ...BoxStyle.box }}>
      {/* {checkedvalue.length > 0 && (
        <OneText
          textStyle={{ position: "absolute", top: "0px" }}
          family="medium"
        >
          {option.label}
        </OneText>
      )} */}

      <div
        style={BoxStyle.topcontainer}
        onClick={(e) => {
          e.stopPropagation();
          setopen(!open);
        }}
      >
        <div style={BoxStyle.flex}>
          {checkedvalue?.length > 0 && (
            <div
              style={{
                display: "flex",
                overflowX: "scroll",
              }}
            >
              {checkedvalue?.map((item, index) => {
                return (
                  <OneText
                    family="regular"
                    size="s"
                    textStyle={{ marginRight: "8px", whiteSpace: "nowrap" }}
                  >
                    {item}
                    {index !== checkedvalue.length - 1 ? "," : ""}
                  </OneText>
                );
              })}
            </div>
          )}
          {checkedvalue?.length <= 0 && (
            <OneText family="regular" size="s" color="lightGrey">
              {option.label}
            </OneText>
          )}

          <img src={open ? Up : Down} alt="icon" />
        </div>
      </div>
      <div style={BoxStyle.topfiltCont}>
        {open && (
          <div style={{ width: width, ...BoxStyle.filterbox }}>
            {option.keys.map((filteroption, filteroptionidx) => {
              return (
                <div key={filteroptionidx}>
                  {filteroption.visibleValues.map(
                    (visibleValue, visibleIndex) => {
                      return (
                        <div
                          key={visibleIndex}
                          onClick={(e) => {
                            e.stopPropagation();
                            setcheckeyvalue({
                              ischeck:
                                checkarray?.[filteroption.key]?.[
                                  visibleIndex
                                ] == true
                                  ? false
                                  : true,
                              key: filteroption.key,
                              value: `${filteroption.optionValues[visibleIndex]}`,
                              visibleValue:
                                filteroption.visibleValues[visibleIndex],
                              index: visibleIndex,
                              label: option.label,
                              selectSingleOption: selectSingleOption,
                            });
                          }}
                          style={{ display: "flex", alignItems: "center" }}
                        >
                          <img
                            src={
                              checkarray?.[filteroption.key]?.[visibleIndex]
                                ? Checked
                                : Check
                            }
                            width={12}
                            height={12}
                            style={{ marginRight: "8px" }}
                          />
                          <OneText size="s" family="regular">
                            {visibleValue}
                          </OneText>
                        </div>
                      );
                    }
                  )}
                </div>
              );
            })}
          </div>
        )}
      </div>
    </div>
  );
};
const BoxStyle: { [key: string]: React.CSSProperties } = {
  box: {
    margin: "20px",
  },
  topcontainer: {
    backgroundColor: globelStyle.backgroundColor,
    padding: "6px 14px",
    borderRadius: "10px",
    width: "100%",
    position: "relative",
    border: globelStyle.genricBorder,
  },
  flex: {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    width: "100%",
  },
  topfiltCont: {
    width: "100%",
  },

  filterbox: {
    border: "1px solid #ACC4FB",
    padding: "16px 12px",
    borderRadius: "10px",
    backgroundColor: globelStyle.backgroundColor,
    marginTop: "10px",
    position: "absolute",
    zIndex: 1,
  },
};
