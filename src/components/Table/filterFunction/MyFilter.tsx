import React, { useCallback, useEffect, useMemo, useState } from "react";

import { FilterBox } from "./filterDropDown";
import {
  checkForEmptyOptions,
  getFilteredData,
} from "./utils/filterFunction.utils";
import { OneButton } from "components/common/button/OneButton";
export enum FilterType {
  droppdown = "droppdown",
  box = "button",
}
export interface filteroptionsProps {
  label: string;
  keys: {
    key: string;
    visibleValues: string[];
    optionValues: string[] | boolean[];
  }[];
}
export interface usefilterProps {
  globaldata: { [key: string]: any }[];
  filteroptions: filteroptionsProps[];
  type?: FilterType;
}
export const useFilter = (
  globaldata: { [key: string]: any }[] | null,
  filteroptions: filteroptionsProps[],
  type = FilterType.droppdown,
  initialValue?: {
    [key: string]: string[];
  } | null,
  selectSingleOption?: boolean
) => {
  const [filteredData, setFilteredData] = useState<
    { [key: string]: any }[] | []
  >([]);
  const initialSelectedValue = useMemo(() => initialValue, [initialValue]);
  const [selectedfilteroptions, setfilteredoptions] = useState<{
    [key: string]: string[];
  } | null>(null);
  const { checkarray, checkedvalue, handleSelectedValue } = useDrowpDownFilter(
    globaldata,
    selectedfilteroptions,
    setfilteredoptions
  );
  useEffect(() => {
    if (initialSelectedValue !== null) {
      setfilteredoptions({
        ...initialSelectedValue,
      });
    }
  }, [initialSelectedValue]);
  useEffect(() => {
    // caution bug zone

    if (globaldata && selectedfilteroptions) {
      if (globaldata.length <= 0) {
        setFilteredData([]);
      }
      if (
        checkForEmptyOptions(selectedfilteroptions) &&
        globaldata.length > 0
      ) {
        setFilteredData(globaldata);
      } else if (globaldata && globaldata.length > 0) {
        const filtered = getFilteredData(globaldata, selectedfilteroptions);
        setFilteredData(filtered);
      }
    }
  }, [selectedfilteroptions, globaldata]);

  const renderDropDownComponent = useMemo(() => {
    if (type !== FilterType.droppdown) {
      return null;
    }
    return (
      <div style={{ display: "flex", zIndex: 1 }}>
        {filteroptions
          ? filteroptions.map((option, index) => {
              return (
                <FilterBox
                  key={index}
                  option={option}
                  setcheckeyvalue={handleSelectedValue}
                  checkarray={checkarray[option?.label]}
                  checkedvalue={checkedvalue?.[option?.label] || []}
                  selectSingleOption={selectSingleOption}
                />
              );
            })
          : null}
      </div>
    );
  }, [filteroptions, type, checkarray, checkedvalue]);
  const renderBoxComponent = useMemo(() => {
    return (
      <div style={{ display: "flex", zIndex: 1 }}>
        {filteroptions
          ? filteroptions.map((filterOptionList) => {
              return filterOptionList.keys.map((datafilterkey) => {
                //
                return datafilterkey.visibleValues.map((singleVisiblevalue) => {
                  return (
                    <OneButton
                      title={singleVisiblevalue}
                      onClick={() => {
                        setfilteredoptions({
                          [datafilterkey.key]: [singleVisiblevalue], // set single filter option
                        });
                      }}
                      variant={
                        selectedfilteroptions?.[datafilterkey.key]?.[0] ==
                        singleVisiblevalue
                          ? "filled"
                          : "outlined"
                      }
                    />
                  );
                });
              });
            })
          : null}
      </div>
    );
  }, [filteroptions, selectedfilteroptions]);

  return {
    renderComponent:
      type == FilterType.droppdown
        ? renderDropDownComponent
        : renderBoxComponent,
    filteredData: filteredData,
  };
};
const useDrowpDownFilter = (
  globaldata: { [key: string]: any }[] | null | undefined,
  selectedfilteroptions: {
    [key: string]: string[];
  } | null,
  setfilteredoptions: React.Dispatch<
    React.SetStateAction<{
      [key: string]: string[];
    } | null>
  >
) => {
  const [checkarray, setcheckedarray] = useState<{
    [key: string]: {
      [key: string]: { [key: number]: boolean };
    };
  }>({});
  const [checkedvalue, setcheckedvalue] = useState<{
    [key: string]: string[];
  } | null>(null);
  useEffect(() => {
    //refresh
    handleRefresh();
  }, [globaldata]);
  const handleRefresh = () => {
    if (globaldata && globaldata?.length > 0) {
      setcheckedarray({});
      setcheckedvalue(null);
      setfilteredoptions({});
    }
  };
  const handleSelectedValue = (selectedValue: {
    ischeck: boolean;
    key: string;
    value: string;
    label: string;
    index: number;
    visibleValue: string;
    selectSingleOption: boolean;
  }) => {
    if (selectedValue.selectSingleOption == true) {
      const carbon = { ...checkarray[selectedValue.label] };

      carbon[selectedValue.key]
        ? (carbon[selectedValue.key][selectedValue.index] =
            selectedValue.ischeck)
        : (carbon[selectedValue.key] = {
            [selectedValue.index]: selectedValue.ischeck,
          });

      const newCarbon = { ...carbon };
      const reqObject = { ...newCarbon[selectedValue.key] };
      for (const key in reqObject) {
        reqObject[key] = false;
      }
      reqObject[selectedValue.index] = true;
      newCarbon[selectedValue.key] = reqObject;
      if (selectedValue?.ischeck) {
        setcheckedarray({ [selectedValue.label]: { ...newCarbon } });
      } else {
        setcheckedarray({}); // for unclick on same selected option
      }
    } else {
      const carbon = { ...checkarray[selectedValue.label] };
      carbon[selectedValue.key]
        ? (carbon[selectedValue.key][selectedValue.index] =
            selectedValue.ischeck)
        : (carbon[selectedValue.key] = {
            [selectedValue.index]: selectedValue.ischeck,
          });
      setcheckedarray({ ...checkarray, [selectedValue.label]: { ...carbon } });
    }

    //to filter data
    let options = selectedfilteroptions
      ? selectedfilteroptions[selectedValue?.key]
        ? [...selectedfilteroptions[selectedValue?.key]]
        : []
      : [];

    if (selectedValue?.ischeck) {
      options.push(selectedValue?.value);
    } else {
      options = options.filter((item) => item !== selectedValue?.value);
    }

    if (selectedValue.selectSingleOption == true) {
      if (selectedValue?.ischeck) {
        setfilteredoptions({
          // ...selectedfilteroptions,
          [selectedValue?.key]: [selectedValue?.value],
        });
      } else {
        setfilteredoptions({}); // for unclick on same selected option
      }
    } else {
      setfilteredoptions({
        ...selectedfilteroptions,
        [selectedValue?.key]: Array.from(new Set(options)),
      });
    }
    //to render selected options
    let checkcopyarray = checkedvalue?.[selectedValue.label] || [];
    if (selectedValue.ischeck) {
      checkcopyarray.push(selectedValue.visibleValue);
    } else {
      checkcopyarray = checkcopyarray?.filter(
        (item) => item !== selectedValue.visibleValue
      );
    }
    if (selectedValue.selectSingleOption == true) {
      if (selectedValue?.ischeck) {
        setcheckedvalue({
          [selectedValue.label]: [selectedValue.visibleValue],
        });
      } else {
        setcheckedvalue(null); // for unclick on same selected option
      }
      return;
    }
    setcheckedvalue({
      ...checkedvalue,
      [selectedValue.label]: Array.from(new Set(checkcopyarray)),
    });
  };
  return {
    setcheckedvalue,
    setcheckedarray,
    checkarray,
    checkedvalue,
    handleSelectedValue,
  };
};
