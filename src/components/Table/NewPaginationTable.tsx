import {
  DataGrid,
  GridFilterModel,
  GridRowIdGetter,
  DataGridProps,
  GridLinkOperator,
  // GridLinkOperator,
} from "@mui/x-data-grid";
import Box from "@mui/material/Box";
import React, { ReactNode, useCallback, useEffect, useState } from "react";
import {
  Alert,
  CircularProgress,
  Divider,
  Paper,
  Typography,
} from "@mui/material";
import { isEmpty } from "utilities/validations/validationUtilities";
import { globelStyle } from "components/common/CommonStyleSheetVariable";

interface PaginationTableProps {
  filterDefination?: GridFilterModel;
  children?: ReactNode;
  tableStyles?: React.CSSProperties;
  title?: string;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  specifyId?: GridRowIdGetter<any>;
  width?: number;
  titleStyles?: React.CSSProperties;
  cardStyles?: React.CSSProperties;
  minWidth?: number | string;
  maxWidth?: number | string;
  tableMinWidth?: number | string;
  noDataErrorMessage?: string;
  isLoading?: boolean;
  error?: string;
  rowsPerPageOptions?: number[];
  elevation?: number;
  hideFooter?: boolean;
  isLeftnotrounded?: boolean;
  isRightnotrounded?: boolean;
  noOfrow?: number;
}

export const NewPaginationTable = (
  props: PaginationTableProps & DataGridProps
) => {
  const {
    filterDefination = undefined,
    children,
    tableStyles,
    title,
    specifyId,
    width,
    titleStyles = {},
    cardStyles = {},
    minWidth,
    maxWidth = "90%",
    tableMinWidth,
    noDataErrorMessage = "No Data Available",
    isLoading = false,
    error,
    rowsPerPageOptions = [5, 10, 15],
    elevation = 2,
    filterModel,
    hideFooter = false,
    noOfrow,
  } = props;

  const [pageSize, setPageSize] = useState<number>(noOfrow || 10);
  useEffect(() => {
    if (noOfrow) setPageSize(noOfrow);
  }, [noOfrow]);
  const renderTable = useCallback(() => {
    if (isLoading) {
      return (
        <Box
          sx={{
            p: 2,
            justifyContent: "center",
            alignItems: "center",
            display: "flex",
          }}
        >
          <CircularProgress color="inherit" />
        </Box>
      );
    }
    if (error != null) {
      return (
        <Box sx={{ p: 2 }}>
          <Alert sx={{ borderRadius: 6 }} severity="error">
            {error}
          </Alert>
        </Box>
      );
    }
    if (isEmpty(props.rows)) {
      return (
        <Box sx={{ p: 2 }}>
          <Alert sx={{ borderRadius: 6 }} severity="error">
            {noDataErrorMessage}
          </Alert>
        </Box>
      );
    }
    return (
      <Box
        sx={[
          {
            width: width || "auto",
            minWidth,
            overflowX: "auto",
          },
        ]}
      >
        <DataGrid
          {...props}
          disableColumnFilter
          initialState={{
            filter: {
              filterModel: {
                items: filterModel?.items || [],
              },
            },
          }}
          componentsProps={{
            filterPanel: { linkOperators: [GridLinkOperator.And] },
          }}
          pageSize={pageSize}
          onPageSizeChange={(pageNumber: number) => setPageSize(pageNumber)}
          rowsPerPageOptions={rowsPerPageOptions}
          getRowId={specifyId}
          style={{
            border: "none",
            minWidth: tableMinWidth,
            borderRadius: "10px",
          }}
          sx={{
            whiteSpace: "normal",
            border: `1px solid ${globelStyle.genricBorder}`,
            "& .no-focus-outline:focus": {
              outline: "none",
            },
            "& .MuiDataGrid-columnHeaders": {
              backgroundColor: `${globelStyle.genericColumnheader}`,
              fontSize: "14px",
              color: "black",
              fontFamily: "Inter SemiBold",
            },
            "& .MuiDataGrid-columnHeaders, & .MuiDataGrid-cell": {
              "&:focus, & .Mui-focusVisible": {
                outline: "none",
              },
            },

            "& .MuiDataGrid-columnSeparator--sideRight": {
              visibility: "hidden",
            },
            "& .MuiDataGrid-footerContainer": {},
            "& .MuiDataGrid-row:nth-child(even)": {
              backgroundColor: globelStyle.alternaterowcolor,
            },
            "& .MuiDataGrid-row:hover": {
              backgroundColor: globelStyle.tablehovercolor,
            },
            "& .MuiDataGrid-cell": {
              border: "none",
            },
            "& .MuiDataGrid-cell:focus": {
              outline: "none",
            },
          }}
          disableColumnMenu
          disableSelectionOnClick={true}
          autoHeight
          hideFooter={hideFooter}
        />
      </Box>
    );
  }, [
    error,
    isLoading,
    minWidth,
    noDataErrorMessage,
    pageSize,
    props,
    rowsPerPageOptions,
    specifyId,
    tableMinWidth,
    width,
  ]);

  return (
    <Box
      style={{
        padding: 0,
        margin: 0,
      }}
    >
      {title != null && (
        <>
          <Box
            sx={{
              pl: 2,
              pt: 2,
              mb: 2,
            }}
          >
            <Typography sx={titleStyles} variant="h6" component="div">
              {title}
            </Typography>
          </Box>
          <Divider />
        </>
      )}
      {children}
      <div
        style={{
          zIndex: 1,
          padding: 0.5,
          borderRadius: "9px",
          border: `1px solid  ${globelStyle.tablehovercolor} `,
          ...tableStyles,
        }}
      >
        {renderTable()}
      </div>
    </Box>
  );
};
