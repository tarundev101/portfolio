import { customDateFormatter, getSubtractDate } from "./dateUtils/dateUtils";

export const getFormattedDate = (date: string | number) => {
  // return formattedDate
};
export const getPresentDate = () => {
  return customDateFormatter(new Date(), "yyyymmdd");
};
export const getPreviousDate = (
  month: number,
  timestamp: string | number | Date,
  day = 0
) => {
  const dates = getSubtractDate(month, timestamp, day);

  return dates;
};

export const getFormattedDateForTable = (date: any) => {
  return customDateFormatter(date, "");
};
export const getDateForSiHub = (date: string | number) => {
  return customDateFormatter(date, "AmAndPm");
};

export const oneDateFormatter = (date?: string | number, format = "") => {
  if (date) return customDateFormatter(date, format);
};
export const getYear = (timestamp: string | number | Date) => {
  return new Date(timestamp).getFullYear();
};
export const getMonth = (timestamp: string | number | Date) => {
  return (new Date(timestamp).getMonth() + 1).toString().padStart(2, "0");
};

export const oneDateFormatterForTimeStamp = (
  date?: string | number,
  format = ""
) => {
  if (date) {
    const newDate = Math.ceil(Number(date)) * 1000;
    return customDateFormatter(newDate, format);
  }
};
