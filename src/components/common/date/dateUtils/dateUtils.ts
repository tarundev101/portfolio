// function dateToTimestamp(date: string) {
//     return date.getTime();
//   }
// interface
export enum dateformate {
  'ampm' = 'AmAndPm',
  yyyymmdd = 'yyyymmdd',
  ampmhour = 'ampm',
}
export const getTimeStamp = (dateStr: string | number | Date) => {
  // for getting timestamp from any date format
  // const timestampString = "1634362722000"; // Replace this with your timestamp string
  const time = Number(dateStr) // Parse the string to an integer
  // if()

  let timestamp: string | number = ''
  if (!isNaN(time)) {
    // const date = new Date(time)
    timestamp = new Date(time).getTime()
  } else {
    timestamp = new Date(dateStr).getTime()
  }
  return timestamp
}
export function convert24HourTo12Hour(time24: string) {
  if (time24 == '') {
    return '-'
  }
  const [hour, minute] = time24.split(':')
  const hourInt = parseInt(hour)

  if (hourInt === 0) {
    return `12:${minute} AM`
  } else if (hourInt < 12) {
    return `${hour.padStart(2, '0')}:${minute.padStart(2, '0')} AM`
  } else if (hourInt === 12) {
    return `12:${minute} PM`
  } else {
    const hour12 = String(hourInt - 12)
    return `${hour12.padStart(2, '0')}:${minute.padStart(2, '0')} PM`
  }
}

// Example usage: Convert 24-hour time to 12-hour time
// const time24 = '14:30' // Replace this with your 24-hour time
// const time12 = convert24HourTo12Hour(time24)

// function getAMPMFromTimestamp(hourss: string) {
//   // const date = new Date(timestamp);
//   const hours = Number(hourss)

//   if (hours >= 0 && hours < 12) {
//     return 'AM'
//   } else {
//     return 'PM'
//   }
// }
// Example usage: Convert the current date to a timestamp
// const dateStr = '2022-11-25 18:28:59.677'
// const timestamp = getTimeStamp(dateStr)

export function customDateFormatter(
  timestamp: number | string | Date,
  format: string
) {
  const months = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec',
  ]

  const initialTimeStamp = getTimeStamp(timestamp)
  const date = new Date(initialTimeStamp)
  const isValid = date.toString() !== 'Invalid Date'
  if (!isValid) {
    return ' -'
  }
  const year = date.getFullYear()
  const month = months[date.getMonth()]
  const nummonth = String(date.getMonth() + 1).padStart(2, '0')
  const day = String(date.getDate()).padStart(2, '0')
  const hours = String(date.getHours()).padStart(2, '0')
  const minutes = String(date.getMinutes()).padStart(2, '0')
  // const seconds = String(date.getSeconds()).padStart(2, '0')
  const amprpm = convert24HourTo12Hour(`${hours}:${minutes}`)
  const formattedDate = `${day} ${month} ${year} ${amprpm}`
  const defaultFormat = `${day} ${month} ${year}`
  const yyyymmdd = `${year}-${nummonth}-${day}`
  const ddmm_am_pa = `${day} ${month}  ${amprpm}`

  switch (format) {
    case 'AmAndPm':
      return formattedDate
    case 'yyyymmdd':
      return yyyymmdd
    case 'ddmm_am_pa':
      return ddmm_am_pa
    case 'ampm':
      return amprpm
    case 'monthInLetter':
      return month
    case 'year':
      return year
    case 'day':
      return day
    default:
      return defaultFormat

    // break;
  }
}

export const getSubtractDate = (
  month: number,
  timestamp: string | number | Date,
  day = 0
) => {
  const timestamps = getTimeStamp(timestamp)
  const date = new Date(timestamps)

  const months = date.getMonth() - month
  const start = new Date(date.getFullYear(), months, date.getUTCDate() - day)
  // const end = new Date(
  //   date.getFullYear(),
  //   date.getMonth(),
  //   date.getUTCDate() - 1
  // )
  return customDateFormatter(start, 'yyyymmdd')
}

export const sliceDateTime = (start: number, str: string) => {
  return `${str.slice(0, 2)}:${str.slice(2)}`
}

export function convertDateFromArry(data: string[] | number[]) {
  if (data && data.length != 0) {
    return Date.parse(`${data[1]}/${data[2]}/${data[0]}`)
  }
}
export const Months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
]
export function calculateMonthsBetween(startdate: Date, enddate: Date) {
  const monthDiff =
    (enddate.getFullYear() - startdate.getFullYear()) * 12 +
    (enddate.getMonth() - startdate.getMonth())
  return monthDiff < 0 ? 0 : monthDiff
}

export const changeDateMonth=(date: string)=>{

 const month = date.slice(
        0,
        2
      )
 const Date = date.slice(
        3,
        5
      )
 const year = date.slice(
        6,
        10
      )
const modifiedDate =`${Date}-${month}-${year}`
return modifiedDate

}
