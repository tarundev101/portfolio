import React, { useState, useContext, useEffect } from "react";
import { Navbox } from "./Navbox";
import OnecardX from "assets/SVG/Billing.svg";
import { Context as AppContext } from "global/reducers/AppContextReducer";
import { OneText } from "./OneText";
import { useLocation, useNavigate } from "react-router-dom";
import { base_prefix } from "config/common.config";
import { MainSideNav, NavSideBoxProps } from "./SideNavTitle";
import CustomDivider from "../text/Divider";
import useNavigateForNestedTabController from "modules/navigation/views/NavigationForNestedTab.controller";
import { UrlArray } from "modules/navigation/utils/UrlArray.util";
export const NavComponent = () => {
  const [passedTitle, setPassedTitle] = useState("");
  const [previousSelected, setPreviousSelected] = useState<number>(-1);
  const { NavigateForNestedTab } = useNavigateForNestedTabController();

  const [modMainSideNavArr, setModMainSideNavArr] =
    useState<NavSideBoxProps[]>(MainSideNav);
  const handleModiySideNavArray = (index: number) => {
    const modifyMainSideNav = MainSideNav;
  };
  useEffect(() => {
    handleClickModiySideNavArray(0, false, -1);
  }, []);
  const handleClickModiySideNavArray = (
    index: number,
    isNested: boolean,
    linkDataIndex: number
  ) => {
    const modifyMainSideNav = MainSideNav;
    for (let i = 0; i < modifyMainSideNav.length; i++) {
      if (i == index && !modifyMainSideNav[i].linkData) {
        modifyMainSideNav[i].isSelected = true;
        setPreviousSelected(i);
      } else {
        modifyMainSideNav[i].isSelected = false;
      }

      if (
        isNested === false &&
        modifyMainSideNav[i].linkData &&
        previousSelected != index
      ) {
        for (let j = 0; j < (modifyMainSideNav[i].linkData?.length ?? 0); j++) {
          const link = modifyMainSideNav[i].linkData![j];
          modifyMainSideNav[i].linkData![j].isSelected = false;
        }
        if (i == index) {
          if (linkDataIndex == -1 && modifyMainSideNav[i].linkData) {
            modifyMainSideNav[i].isInitialOpen =
              !modifyMainSideNav[i].isInitialOpen;
          }

          modifyMainSideNav[i].isSelected = false;
          // modifyMainSideNav[i].showExpand = modifyMainSideNav[i].showExpand
        }
      }

      if (isNested === true && modifyMainSideNav[i].linkData) {
        for (let j = 0; j < (modifyMainSideNav[i].linkData?.length ?? 0); j++) {
          const link = modifyMainSideNav[i].linkData![j];

          modifyMainSideNav[i].linkData![j].isSelected = false;
        }
      }
    }

    if (isNested == true) {
      modifyMainSideNav[index].isSelected = true;
      modifyMainSideNav[index].isInitialOpen = true;
      modifyMainSideNav[index].showExpand = true;
      setPreviousSelected(-1);

      if (
        linkDataIndex != -1 &&
        modifyMainSideNav[index] &&
        modifyMainSideNav[index].linkData &&
        modifyMainSideNav[index].linkData![linkDataIndex]
      ) {
        modifyMainSideNav[index].linkData![linkDataIndex].isSelected = true;
      }
    } else if (
      isNested == false &&
      modifyMainSideNav[index].linkData &&
      previousSelected !== -1
    ) {
      modifyMainSideNav[index].isSelected = false;
      modifyMainSideNav[index].showExpand = false;
      modifyMainSideNav[previousSelected].isSelected = true;
    }

    setModMainSideNavArr([...modifyMainSideNav]);
  };
  useEffect(() => {
    const array = pathname.split(base_prefix);
    const result = array.pop();
    const modifyMainSideNav = modMainSideNavArr;

    for (let i = 0; i < modifyMainSideNav.length; i++) {
      if (modifyMainSideNav[i].linkData) {
        if (
          modifyMainSideNav[i] &&
          modifyMainSideNav[i].linkData &&
          modifyMainSideNav[i].linkData !== undefined
        ) {
          for (
            let j = 0;
            j < (modifyMainSideNav[i].linkData?.length ?? 0);
            j++
          ) {
            if (
              result?.includes(modifyMainSideNav[i].linkData![j].linkto) &&
              modifyMainSideNav[i].linkData![j].isSelected == false
            ) {
              handleClickModiySideNavArray(i, true, j);
              return;
            }
          }
        }
      }
      if (
        modifyMainSideNav[i].isSelected == false &&
        modifyMainSideNav[i].redirectTo &&
        result?.includes(`${modifyMainSideNav[i].redirectTo}`)
      ) {
        handleClickModiySideNavArray(i, false, -1);
        return;
      }
    }
  }, [window.location.href]);

  const { state: AppState } = useContext(AppContext);
  const firstName = `Tarun`;
  const lastName = `dev`;
  const customerNumber = `9914850796`;
  const navigate = useNavigate();
  const location = useLocation();
  const { pathname } = location;

  const handleImageClick = () => {
    navigate(`${base_prefix}`);
  };
  return (
    <div
      style={{
        width: "85%",
        margin: "auto",
        height: "100vh",
        position: "sticky",
        top: 0,
        left: 0,
        bottom: 0,
      }}
    >
      <img
        src={OnecardX}
        onClick={() => handleImageClick()}
        style={{
          marginBottom: "24px",
          marginLeft: "16px",
          cursor: "pointer",
          marginTop: "30px",
        }}
      />
      <OneText
        family="semiBold"
        size="m"
        color="blue"
        textStyle={{
          marginLeft: "16px",
        }}
      >
        {customerNumber}
      </OneText>
      <div
        onClick={() => NavigateForNestedTab(`${base_prefix}`)}
        style={{ display: "flex", margin: "16px 16px 24px 16px" }}
      >
        <OneText family="medium" size="m">{`${firstName || "-"} ${
          lastName || "-"
        }`}</OneText>
      </div>
      <CustomDivider containerWidth={"100%"} />
      <div style={{ marginBottom: "50px" }} />
      {modMainSideNavArr.map((navbox, index) => {
        return (
          <Navbox
            key={navbox.LabelTitle}
            title={navbox.title}
            LabelTitle={navbox.LabelTitle}
            passedTitle={navbox.showExpand}
            BoxClickAction={setPassedTitle}
            redirectTo={navbox.redirectTo}
            linkData={navbox.linkData}
            index={index}
            isSelected={navbox.isSelected}
            isInitialOpen={navbox.isInitialOpen}
            handleModiySideNavArray={handleModiySideNavArray}
            handleClickModiySideNavArray={handleClickModiySideNavArray}
          />
        );
      })}
    </div>
  );
};
