import { Box, Stack } from "@mui/material";
import React, { useState, useEffect, useCallback, useContext } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import { OneText } from "./OneText";
import { base_prefix } from "config/common.config";
import { Context as AppContext } from "global/reducers/AppContextReducer";
import { NavListProps, linkArray } from "./model/Nav.model";
import { oneColor } from "../CommonStyleSheetVariable";

const navColor = "#EBF8FF";
const navPadding = 4;

export const NavList = ({
  linkData = [],
  showExpand,
  BoxClickAction,
  LabelTitle,
  handleImageClick,
}: NavListProps) => {
  const [filteredLinkData, setFilteredLinkData] =
    useState<linkArray[]>(linkData);
  const [idx, setIdx] = useState(-1);
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const { state: AppState } = useContext(AppContext);

  useEffect(() => {
    if (idx !== -1 && showExpand == false) {
      setIdx(-1);
    }
  }, [showExpand]);

  const SetIndex = useCallback(() => {
    const array = pathname.split(base_prefix);
    const result = array.pop()?.split("/");
    const pathto = result?.[0] + "/" + result?.[1];
    const secondpathIndex = linkData.findIndex((ele) => ele.linkto == pathto);
    if (secondpathIndex > 0) {
      setIdx(secondpathIndex);
      ToNavigate(secondpathIndex);
    }
  }, [filteredLinkData, pathname]);

  const ToNavigate = useCallback(
    (id: number) => {
      const url = filteredLinkData[id]?.linkto;
      if (url) {
        navigate(`${base_prefix}${url}`);
      }
    },
    [base_prefix]
  );

  const handleClickLink = useCallback((index: number) => {
    setIdx(index);
    ToNavigate(index);

    if (!showExpand) {
      BoxClickAction(LabelTitle);
    }
    handleImageClick(index);
  }, []);

  return (
    <>
      {filteredLinkData &&
        filteredLinkData.map((item, index) => (
          <Box key={index}>
            <Stack direction={"row"}>
              <Box
                sx={{
                  width: 3,
                  backgroundColor:
                    idx == -1 ? oneColor.button_background_selected : navColor,
                  flexFlow: "column",
                  borderTopLeftRadius: index == 0 ? 4 : 0,
                  borderTopRightRadius: index == 0 ? 4 : 0,
                  borderBottomLeftRadius: index == linkData.length - 1 ? 4 : 0,
                  borderBottomRightRadius: index == linkData.length - 1 ? 4 : 0,
                }}
              />
              <Stack direction={"row"} alignItems={"center"}>
                <Box
                  sx={{
                    width: 5,
                    height: 4,
                    backgroundColor:
                      // idx == index &&
                      item.isSelected == true ? "#4A7FF7" : navColor,
                    borderTopRightRadius: 10,
                    borderBottomRightRadius: 10,
                  }}
                />
                <div
                  onClick={() => {
                    handleClickLink(index);
                  }}
                  style={{ padding: "10px 0px" }}
                >
                  <OneText
                    family={item.isSelected == true ? "medium" : "regular"}
                    color={item.isSelected == true ? "blue" : "lightGrey"}
                    size="m"
                    textStyle={{
                      marginLeft: 3 * navPadding,
                    }}
                  >
                    {item.linktitle}
                  </OneText>
                </div>
              </Stack>
            </Stack>
          </Box>
        ))}
    </>
  );
};
