import React from 'react'
import { Card, Grid, CardContent } from '@mui/material'

type nodeType = {
  rele: React.ReactNode
}

type typeProps = {
  data?: nodeType[]
  colmn?: number
  title?: React.ReactNode
  boxMargin?: number | string
  boxStyle?: React.CSSProperties
  contentStyle?: React.CSSProperties
  variant?: 'elevation' | 'outlined'
  spacing?: number | string
}

export const OneCustomerBox = ({
  data = [],
  colmn,
  boxMargin,
  boxStyle,
  variant = 'outlined',
  title,
  spacing,
  contentStyle,
}: typeProps) => {
  const Content = () => (
    <>
      {title ? title : null}
      <CardContent sx={{ ...contentStyle }}>
        <Grid container>
          {data.map((item: { ['rele']: React.ReactNode }, index: number) => {
            return (
              <Grid key={index} xs={colmn ? 12 / colmn : 12} spacing={spacing}>
                {item.rele}
              </Grid>
            )
          })}
        </Grid>
      </CardContent>
    </>
  )
  return (
    <Card
      variant={variant}
      sx={{
        margin: boxMargin ? boxMargin : 0,
        width: '100%',
        ...boxStyle,
      }}
    >
      {Content()}
    </Card>
  )
}
