import { Grid } from '@mui/material'
import React from 'react'
import { OneText } from './OneText'

const fontBaseStyles = {
  // margin: '27px 154px 16px 5px',
  fontFamily: 'Inter Regular',
  fontStretch: 'normal',
  fontStyle: 'normal',
  letterSpacing: 'normal',
  width: '100%',
}

const textLabelStyles = {
  ...fontBaseStyles,
  fontSize: '16px',
  fontWeight: 'normal',
  color: '#707070',
  lineHeight: 1.29,
  margin: '5px 20px 5px 20px',
}

const textValueStyles = {
  ...fontBaseStyles,
  fontFamily: 'Inter SemiBold',
  // fontSize: '18px',
  fontWeight: '600',
  fontSize: '16px',
  lineHeight: 1.13,
  margin: '8px 20px 5px 20px',
  color: '#191919',
}

export const KeyValue = (
  first: string,
  sec: string,
  labelAreaWidth1 = 2.5,
  labelAreaWidth2 = 9
) => (
  <Grid container>
    <Grid item xs={labelAreaWidth1}>
      <OneText textStyle={textLabelStyles}>{first}</OneText>
    </Grid>
    <Grid item xs={labelAreaWidth2}>
      <OneText textStyle={textValueStyles}>{sec}</OneText>
    </Grid>
  </Grid>
)
