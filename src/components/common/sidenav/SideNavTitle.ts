import { linkArray } from "./model/Nav.model";
export interface NavSideBoxProps {
  title: string;
  LabelTitle: string;
  passedTitle?: string;
  redirectTo?: string;
  linkData?: linkArray[];
  isInitialOpen?: boolean;
  isSelected: boolean;
  showExpand: boolean;
}
//insure labeltitle be same as given route in app navigation

const onecardList: linkArray[] = [
  {
    linktitle: "Card Details",
    linkto: "one-card/card-details",
    isSelected: false,
  },
  {
    linktitle: "Bills & Transactions",
    linkto: "one-card/bills-transactions",
    isSelected: false,
  },
  {
    linktitle: "Features",
    linkto: "one-card/features-section",
    isSelected: false,
  },
  { linktitle: "Offers", linkto: "one-card/offers", isSelected: false },
  { linktitle: "FD", linkto: "one-card/fd-section", isSelected: false },
];
export const MainSideNav: NavSideBoxProps[] = [
  {
    title: "Table Collection",
    LabelTitle: "dashboard",
    redirectTo: "dashboard",
    isSelected: true,
    showExpand: false,
  },
  {
    title: "tab",
    LabelTitle: "tab",
    redirectTo: "tab",
    isSelected: true,
    showExpand: false,
  },
];
