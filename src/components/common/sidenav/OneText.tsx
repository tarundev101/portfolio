import React from "react";
import { fontFamily, globelStyle, oneColor } from "../CommonStyleSheetVariable";

export interface OneTextProps {
  children?: React.ReactNode;
  textStyle?: React.CSSProperties;
  size?: "m" | "s" | "l" | "xl" | "xxl" | "xxxl";
  family?: keyof fontFamily;
  color?:
    | "black"
    | "darkGrey"
    | "lightGrey"
    | "blue"
    | "green"
    | "darkBlack"
    | "textGrey"
    | "red"
    | "white";

  lineHeight?: number | string;
}

export const OneText = ({
  children,
  size,
  textStyle,
  family,
  color,
  lineHeight,
}: OneTextProps) => {
  const FontSize = {
    l: "16px",
    m: "14px",
    s: "12px",
    xl: "20px",
    xxl: "40px",

    xxxl: "60px",
  };
  const FontColour = {
    white: oneColor.text,
    black: "#000000",
    darkGrey: "#707070",
    lightGrey: "#96a8b4",
    blue: "#4a7ff7",
    green: "#18aa1b",
    darkBlack: "#191919",
    textGrey: "#909090",
    red: "red",
  };
  return (
    <div
      style={{
        fontSize: size ? FontSize[size] : FontSize["m"],
        fontFamily: family
          ? globelStyle.fontFamily[family]
          : globelStyle.fontFamily["medium"],
        color: color ? FontColour[color] : FontColour["black"],
        lineHeight: lineHeight ? `${lineHeight}px` : "",
        padding: 0,
        cursor: "default",
        ...textStyle,
      }}
    >
      {children}
    </div>
  );
};
