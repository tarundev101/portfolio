import React from "react";
import { Stack } from "@mui/material";
import { globelStyle } from "components/common/CommonStyleSheetVariable";

type propsType = {
  children: React.ReactNode;
  textStyle?: React.CSSProperties;
  dotStyle?: React.CSSProperties;
  gap?: number;
  mt?: number;
  fullWidth?: boolean;
};

// used to add header we can change style of box and dot
export const DotHeader = ({
  children,
  textStyle,
  dotStyle,
  gap,
  mt,
  fullWidth,
}: propsType) => {
  return (
    <div
      style={{
        display: fullWidth ? "block" : "inline-block",
      }}
    >
      <Stack
        direction={"row"}
        spacing={gap ? gap : 1}
        alignItems={"center"}
        sx={{ marginTop: mt ? mt : globelStyle.dotheaderTopMargin }}
      >
        <div
          style={{
            height: 10,
            backgroundColor: "#96a8b4",
            width: 4,
            borderTopRightRadius: 4,
            borderBottomRightRadius: 4,
            marginLeft: -1,
            ...dotStyle,
          }}
        />
        <p
          style={{
            fontFamily: "Inter Regular",
            color: "#96a8b4",
            fontSize: "14px",
            ...textStyle,
          }}
        >
          {children}
        </p>
      </Stack>
    </div>
  );
};
