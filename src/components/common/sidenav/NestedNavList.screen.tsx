/* eslint-disable @typescript-eslint/no-non-null-assertion */
import React from 'react'
import { NavSideBoxProps } from './SideNavTitle'

interface NestedNavListProps {
  MainSideNav: NavSideBoxProps[]
}
function NestedNavList({ MainSideNav }: NestedNavListProps) {
  return <></>
}

export default NestedNavList
