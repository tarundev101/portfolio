export type NavboxProps = {
  index: number
  title: string
  linkData?: linkArray[]
  mainBoxStyle?: React.CSSProperties
  textStyle?: React.CSSProperties
  passedTitle: boolean
  LabelTitle: string
  BoxClickAction: React.Dispatch<React.SetStateAction<string>>
  redirectTo?: string
  isInitialOpen?: boolean
  handleModiySideNavArray: (index: number) => void
  handleClickModiySideNavArray: (
    index: number,
    isNested: boolean,
    linkDataIndex: number
  ) => void
  isSelected: boolean
}
export type linkArray = {
  linktitle: string
  linkto: string
  isSelected: boolean
}

export type NavListProps = {
  linkData?: linkArray[]
  showExpand: boolean
  // passedTitle: string | undefined
  BoxClickAction: NavboxProps['BoxClickAction']
  LabelTitle: string
  handleImageClick: (linkDataIdex: number) => void
  isClickedOutsideOfNested: boolean
}
