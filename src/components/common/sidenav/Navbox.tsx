import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { OneText } from "./OneText";
import { NavList } from "./Navlist";
import CircleUp from "../../../assets/SVG/CircleArrowUp.svg";
import CircleDown from "../../../assets/SVG/CircleArrowDown.svg";
import { base_prefix } from "config/common.config";
import { NavboxProps } from "./model/Nav.model";
import { Color, oneColor } from "../CommonStyleSheetVariable";
import { OneTextProps } from "../text/OneText";

const navPadding = 4;

export const Navbox = ({
  index,
  title,
  linkData,
  mainBoxStyle,
  passedTitle, //to distinguish it from other navbox
  LabelTitle, //unique title of the navbox
  textStyle,
  BoxClickAction,
  redirectTo,
  isInitialOpen = false,
  handleModiySideNavArray,
  handleClickModiySideNavArray,
  isSelected,
}: NavboxProps) => {
  const navigate = useNavigate();
  const showExpand = passedTitle;
  const [isOptionSelected, setisOptionSelected] = useState<boolean>(false);
  const [isClickedOutsideOfNested, setisClickedOutsideOfNested] =
    useState<boolean>(false);

  const handleImageClick = (linkDataIndex: number) => {
    if (isOptionSelected == false) {
      setisOptionSelected(true);
    }
    // handleClickModiySideNavArray(index, true, linkDataIndex)
  };
  const handleLeftElementClick = ({
    index,
    redirectTo,
  }: {
    index: number;
    redirectTo?: string;
  }) => {
    if (redirectTo) {
      navigate(`${base_prefix}${redirectTo}`);
    }
    handleClickModiySideNavArray(index, false, -1);
    // console.log();
  };
  useEffect(() => {
    if (isInitialOpen == true) {
      BoxClickAction(LabelTitle);
    }
  }, [isInitialOpen]);
  const handleColor = () => {
    let backgroundcolor = "white";
    let color = "textGrey";

    if (
      (isOptionSelected == false && linkData && showExpand) ||
      isInitialOpen == true
    ) {
      color = "white";
      backgroundcolor = oneColor.button_background_selected;
    }
    if (isSelected == true) {
      color = "white";

      backgroundcolor = oneColor.button_background_selected;
    }
    return { backgroundcolor, color };
  };
  const LeftElement = () => (
    <div
      style={{
        display: "flex",
        justifyContent: "space-between",
        alignItems: "center",
        minWidth: "100%",
        backgroundColor: handleColor().backgroundcolor,
        borderRadius: 2 * navPadding,
        ...mainBoxStyle,
      }}
      onClick={() => {
        handleLeftElementClick({ index });
      }}
    >
      <div>
        <OneText
          family="semiBold"
          color={`${handleColor().color}` as OneTextProps["color"]}
          textStyle={{ ...textStyle, padding: 4 * navPadding }}
        >
          {title}
        </OneText>
      </div>
      <div style={{ marginRight: 4 * navPadding }}>{RightElement()}</div>
    </div>
  );
  const RightElement = () => {
    if (!linkData) {
      return null;
    }
    if (showExpand) {
      return <img src={CircleUp} width={15} />;
    } else {
      return <img src={CircleDown} width={15} />;
    }
  };

  return (
    <>
      {LeftElement()}
      {(showExpand || isInitialOpen == true) && (
        <div style={{ marginLeft: 20 }}>
          <NavList
            linkData={linkData}
            showExpand={showExpand}
            // passedTitle={passedTitle}
            BoxClickAction={BoxClickAction}
            LabelTitle={LabelTitle}
            handleImageClick={handleImageClick}
            isClickedOutsideOfNested={isClickedOutsideOfNested}
          />
        </div>
      )}
    </>
  );
};
