import React from "react";
import "./oneInputStyle.scss";
import { globelStyle, oneFontFamily } from "../CommonStyleSheetVariable";
export interface handleInputValType {
  value: string | number;
  index?: number;
}
export interface OneInputPropsType
  extends React.HTMLAttributes<HTMLDivElement> {
  children?: React.ReactNode;
  boxStyle?: React.CSSProperties;
  inputStyleProps?: React.CSSProperties;
  index?: number;
  background?: {
    backgroundPath: string;
    backgroundPosition: "left" | "right";
  };
  isSpecialCharAllowedForNumber?: boolean;
  isDotAllowedForNumber?: boolean;

  isFloatAllowedForNumber?: boolean;
  placeHolder?: string;
  type?: string;
  value: string | number | undefined;
  onInputChangeFn?: ({ value }: handleInputValType) => void;
  onChangeFunction?: React.Dispatch<React.SetStateAction<any>>;
  onEnterKeyPressFunction?: () => void;
  onClickFunction?: () => void;
  onKeyDownFunction?: (event: string) => void;
  autoFocus?: boolean;
  inputProps?: React.DetailedHTMLProps<
    React.InputHTMLAttributes<HTMLInputElement>,
    HTMLInputElement
  >;
}

export const OneInputText = ({
  index = -1, // index can be used if you wat to use input in array
  type,
  value,
  background,
  onInputChangeFn,
  onChangeFunction,
  onEnterKeyPressFunction,
  onKeyDownFunction,
  onClickFunction,
  placeHolder,
  inputStyleProps,
  autoFocus,
  inputProps,
  isSpecialCharAllowedForNumber = true,
  isDotAllowedForNumber = true,
}: OneInputPropsType) => {
  function handleInputChange(event: React.ChangeEvent<HTMLInputElement>) {
    if (index != -1 && onInputChangeFn) {
      if (type == "checkbox") {
        // onInputChangeFn(event.target.checked, index)
        // onInputChangeFn &&
        onInputChangeFn({ value: `${event.target.checked}`, index });

        return;
      }

      if (isSpecialCharAllowedForNumber == false) {
        // onInputChangeFn &&

        onInputChangeFn({
          value: `${handleSpecialCharacterForNumberType(event.target.value)}`,
          index,
        });

        return;
      } else {
        // onInputChangeFn &&
        onInputChangeFn({ value: `${event.target.value}`, index });
      }
    } else if (onInputChangeFn) {
      if (type == "checkbox") {
        onInputChangeFn({ value: `${event.target.checked}` });
        return;
      }
      onInputChangeFn({ value: `${event.target.value}` });
    }
    if (onChangeFunction) onChangeFunction(event.target.value);
  }
  const handleSpecialCharacterForNumberType = (value: string) => {
    let tempValue = "";
    let dotCount = 0;
    if (isDotAllowedForNumber == true) {
      console.log("nkjbksjbdisDotAllowedForNumber");

      for (let i = 0; i < value.length; i++) {
        if (
          Number.isInteger(parseInt(value[i])) == true &&
          value[i] !== "+" &&
          value[i] !== "e" &&
          value[i] !== "E" &&
          value[i] !== "."
        ) {
          tempValue += value[i];
        }
      }
    } else {
      for (let i = 0; i < value.length; i++) {
        if (
          Number.isInteger(parseInt(value[i])) == true &&
          value[i] !== "+" &&
          value[i] !== "e" &&
          value[i] !== "E"
        ) {
          if (dotCount < 1) {
            tempValue += value[i];
            dotCount += 1;
          } else {
            if (value[i] !== ".") tempValue += value[i];
          }
        }
      }
    }

    return tempValue;
  };

  const handleKeyPress = (event: any) => {
    if (event.key === "Enter") {
      if (onEnterKeyPressFunction) {
        onEnterKeyPressFunction();
      }
    }
    if (onKeyDownFunction) {
      onKeyDownFunction(event.key);
    }
  };
  return (
    <div>
      <input
        // disabled
        onChange={handleInputChange}
        onKeyDown={handleKeyPress}
        onClick={onClickFunction}
        placeholder={placeHolder ? placeHolder : ""}
        type={type ? type : "text"}
        value={value}
        style={{
          height: "40px",
          fontSize: "14px",
          borderRadius: "10px",
          fontFamily: oneFontFamily.regular,
          paddingLeft: "20px",
          background:
            background &&
            ` url(${background.backgroundPath}) no-repeat ${background.backgroundPosition}`,

          backgroundSize: " 20px",
          border: globelStyle.genricBorder,

          ...inputStyleProps,
        }}
        autoFocus={autoFocus}
        {...inputProps}
      />
    </div>
  );
};
