import { Skeleton, SkeletonProps } from "@mui/material";
import React, { ReactNode, useEffect, useState } from "react";
import ErrorSvg from "assets/SVG/Error.svg";
import EmptyState from "assets/SVG/EmptyState.svg";
import { OneText } from "./text/OneText";
import { Spin } from "antd";
import "./genricPopUpPage/GenricPopUpStyle.scss";
import { GennricPopUp } from "./genricPopUpPage/GenricPopUpPage";
// import LoadingSpinner from "./loading/Loading.screen";

interface loadingstateProps extends SkeletonProps {
  popup?: boolean;
  loading?: boolean;
  children?: ReactNode;
  error?: boolean;
  errortextStyle?: React.CSSProperties;
  NoDatatextStyle?: React.CSSProperties;
  containerImgStyle?: React.CSSProperties;
  errorMsg?: string | null;
  ErrorComponenet?: ReactNode;
  retryFunc?: () => void;
  retryHeight?: string;
  isDataEmpty?: boolean;
  zIndex?: number;
  statusCode?: number;
}
const ContainerWrapper = (props: loadingstateProps) => {
  const {
    loading,
    children,
    error,
    errortextStyle,
    NoDatatextStyle,
    errorMsg,
    ErrorComponenet,
    containerImgStyle,
    retryFunc,
    retryHeight = "",
    isDataEmpty,
    popup = false,
    zIndex = 10,
    statusCode,
    ...skeletonProps
  } = props;

  const [openPopUp, setzopenPopUp] = useState(false);
  const RetryScreen = (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        height: retryHeight,
      }}
    >
      <img alt="" src={ErrorSvg} style={{ ...containerImgStyle }} />
      <div onClick={retryFunc}>
        <OneText
          family="semiBold"
          color="blue"
          textStyle={{
            margin: "14px 0px 0px 0px",
            cursor: "pointer",
            ...errortextStyle,
          }}
        >
          Tap to Retry
        </OneText>
      </div>
    </div>
  );
  const unAuthorizeScreen = (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        height: retryHeight,
      }}
    >
      <img alt="unAuthorize" src={ErrorSvg} style={{ ...containerImgStyle }} />
      <div>
        <OneText
          family="semiBold"
          color="blue"
          textStyle={{
            margin: "14px 0px 0px 0px",
            cursor: "pointer",
            ...errortextStyle,
          }}
        >
          You do not have access for this resource please connect with admin
        </OneText>
      </div>
    </div>
  );
  const resourceNotFoundScreen = (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        height: retryHeight,
      }}
    >
      <img alt="unAuthorize" src={ErrorSvg} style={{ ...containerImgStyle }} />
      <div>
        <OneText
          family="semiBold"
          color="blue"
          textStyle={{
            margin: "14px 0px 0px 0px",
            cursor: "pointer",
            ...errortextStyle,
          }}
        >
          Resource does not exist
        </OneText>
      </div>
    </div>
  );
  const emptyDataScreen = (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        height: retryHeight,
      }}
    >
      <img alt="" src={EmptyState} style={{ ...containerImgStyle }} />
      <div>
        <OneText
          family="semiBold"
          color="blue"
          textStyle={{
            margin: "14px 0px 0px 0px",
            cursor: "pointer",
            ...NoDatatextStyle,
          }}
        >
          No data available
        </OneText>
      </div>
    </div>
  );
  useEffect(() => {
    if (popup == true) {
      if (error == true || isDataEmpty == true) {
        setzopenPopUp(true);
      }
    }
  }, [error, popup, isDataEmpty]);
  if (loading) {
    if (popup == true) {
      return (
        <div
          style={{
            zIndex: zIndex,
          }}
        >
          <div
            className="modal-overlay"
            style={{
              position: "fixed",
              top: "0",
              left: "0",
              bottom: "0",
              right: "0",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              background: "rgba(0, 0, 0, 0.3)",
              zIndex: zIndex || 100,
            }}
            onClick={() => {
              //   closeModal(false);
            }}
          >
            {/* <LoadingSpinner /> */}
            <Spin size="large"></Spin>
          </div>

          {/* <Skeleton
            animation="wave"
            sx={{ marginTop: "-40px" }}
            // style={{ marginTop: '-40px' }}
            {...skeletonProps}
          ></Skeleton> */}
        </div>
      );
    }
    return (
      <div>
        <Skeleton
          animation="wave"
          sx={{ marginTop: "-40px" }}
          // style={{ marginTop: '-40px' }}
          {...skeletonProps}
        ></Skeleton>
      </div>
    );
  }
  if (error) {
    if (popup == true) {
      return (
        <>
          <GennricPopUp
            open={openPopUp}
            closeModal={setzopenPopUp}
            containerWidth="380px"
            containerHeight={`315px`}
            zIndex={zIndex}
          >
            <div
              style={{
                width: skeletonProps.width ? skeletonProps.width : "95%",
                margin: "50px auto",
              }}
            >
              {RetryScreen}
            </div>
          </GennricPopUp>
        </>
      );
    }
    return (
      <>
        {ErrorComponenet || (
          <div
            style={{
              width: skeletonProps.width ? skeletonProps.width : "95%",
              margin: "0px auto",
            }}
          >
            {RetryScreen}
          </div>
        )}
      </>
    );
  } else if (isDataEmpty == true) {
    if (popup == true) {
      return (
        <>
          <GennricPopUp
            open={openPopUp}
            closeModal={setzopenPopUp}
            containerWidth="380px"
            containerHeight={`315px`}
            zIndex={zIndex}
          >
            <div
              style={{
                width: skeletonProps.width ? skeletonProps.width : "95%",
                margin: "50px auto",
              }}
            >
              {emptyDataScreen}
            </div>
          </GennricPopUp>
        </>
      );
    }
    return (
      <>
        {
          <div
            style={{
              width: skeletonProps.width ? skeletonProps.width : "95%",
              margin: "0px auto",
            }}
          >
            {emptyDataScreen}
          </div>
        }
      </>
    );
  }
  if (statusCode == 401) {
    if (popup == true) {
      return (
        <>
          <GennricPopUp
            open={openPopUp}
            closeModal={setzopenPopUp}
            containerWidth="380px"
            containerHeight={`315px`}
            zIndex={zIndex}
          >
            <div
              style={{
                width: skeletonProps.width ? skeletonProps.width : "95%",
                margin: "50px auto",
              }}
            >
              {unAuthorizeScreen}
            </div>
          </GennricPopUp>
        </>
      );
    }
    return (
      <>
        {
          <div
            style={{
              width: skeletonProps.width ? skeletonProps.width : "95%",
              margin: "0px auto",
            }}
          >
            {unAuthorizeScreen}
          </div>
        }
      </>
    );
  }
  if (statusCode == 403) {
    if (popup == true) {
      return (
        <>
          <GennricPopUp
            open={openPopUp}
            closeModal={setzopenPopUp}
            containerWidth="380px"
            containerHeight={`315px`}
            zIndex={zIndex}
          >
            <div
              style={{
                width: skeletonProps.width ? skeletonProps.width : "95%",
                margin: "50px auto",
              }}
            >
              {resourceNotFoundScreen}
            </div>
          </GennricPopUp>
        </>
      );
    }
    return (
      <>
        {
          <div
            style={{
              width: skeletonProps.width ? skeletonProps.width : "95%",
              margin: "0px auto",
            }}
          >
            {resourceNotFoundScreen}
          </div>
        }
      </>
    );
  }
  return <>{children}</>;
};

export default ContainerWrapper;
