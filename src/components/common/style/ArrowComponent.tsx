import React from "react";
import imgBlackarrow from "assets/SVG/arrow_icon.svg";
import imgWhitearrow from "assets/SVG/arrow_icon_white.svg";
import { IRotateChildren, RotateComponent } from "../RotateComponent";

interface headingProps {
  width?: string | number;
  background?: string;
  height?: string;
  leftBorderRadius?: string | number;
  marginTop?: string | number;
  functionOnContainer?: any;
  isWhiteColor?: boolean;
  imageStyleProp?: React.CSSProperties;
}

function ArrowComponent(props: headingProps) {
  const {
    width,
    background,
    height,
    leftBorderRadius,
    isWhiteColor,
    functionOnContainer,
    imageStyleProp,
  } = props;

  return (
    <div
      style={{
        display: "flex",
        backgroundColor: background ? background : "",
        width: width ? width : "auto",
        height: height ? height : "auto",
        borderTopLeftRadius: leftBorderRadius ? leftBorderRadius : 0,
        borderBottomLeftRadius: leftBorderRadius ? leftBorderRadius : 0,
        ...imageStyleProp,
      }}
      onClick={functionOnContainer ? functionOnContainer : undefined}
    >
      <div
        className="img"
        style={{
          width: "25px",
          height: "75%",
          marginLeft: 5,
          marginTop: "2px",
          borderLeft: leftBorderRadius ? leftBorderRadius : 0,
        }}
      >
        {isWhiteColor == true ? (
          <RotateComponent rotate={IRotateChildren.RIGHT}>
            <img
              style={{ width: "100%", height: "100%", backgroundClip: "" }}
              src={imgBlackarrow}
              alt="imgBlackarrow"
            ></img>
          </RotateComponent>
        ) : (
          <RotateComponent rotate={IRotateChildren.DEFAULT}>
            <img
              style={{ width: "100%", height: "100%", backgroundClip: "" }}
              src={imgWhitearrow}
              alt="imgWhitearrow"
            ></img>
          </RotateComponent>
        )}
      </div>
    </div>
  );
}

export default ArrowComponent;
