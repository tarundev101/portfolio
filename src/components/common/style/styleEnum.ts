export enum ClassName {
  blue = "button-hov-light-blue",
  grey = "button-hov-light-grey",
  blueBorderAndText = "button-hov-light-blue-border",
  purple = "hov-light-purple",
  fadeBlue = "fade-blue",
  black_fade = "fade-black",
}
