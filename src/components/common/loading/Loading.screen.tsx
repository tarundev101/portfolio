import React from "react";
// import Star from "/assets/onecard_assets/Star.svg";
import Star from "./../../../assets/SVG/Star.svg";
import "./loading.css";
const LoadingSpinner = ({ width }: { width?: number }) => {
  return (
    <img
      src={Star}
      alt=""
      className="animate-spin"
      width={width ? width : "70px"}
    />
  );
};

export default LoadingSpinner;
