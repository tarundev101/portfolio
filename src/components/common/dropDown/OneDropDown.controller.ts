/* eslint-disable no-empty */
import { useEffect, useRef, useState } from "react";
import { useOneDropDownControllerType } from "./OneDropDown.model";

function useOneDropDownController({
  arrowKeyAllowed,
  setIsfilterTableOpen,
  isArrowKeyPressed,
  data,
  EnterClicked,
  height,
  onDropDownClick,
}: useOneDropDownControllerType) {
  //below three state is used for setting selected option
  const [selectedValue, setSelectedValue] = useState<string>("");
  const [selectedLabel, setSelectedLabel] = useState<string | number>("");
  const [selectedIndex, setSelectedIndex] = useState<string | number>(-1);

  const [isSelectiveIndex, setIsSelectiveIndex] = useState<number>(0); // It is used for arrow select index
  const divRef = useRef<HTMLDivElement | any>(null);
  const [scrollPosition, setScrollPosition] = useState(0);
  function onDropDownValueSelect(
    selectedItem: string | number,
    selectedLablearg: string | number,
    index: number
  ) {
    setSelectedValue(`${selectedItem}`);
    setSelectedLabel(selectedLablearg);
    setIsSelectiveIndex(index);
    setSelectedIndex(index);
    if (onDropDownClick) {
      onDropDownClick({
        value: `${selectedItem}`,
        selectedIndex: index,

        filteredDataForValue: data || [],
      });
    }

    closeDropDown();
  }
  function closeDropDown() {
    if (setIsfilterTableOpen) setIsfilterTableOpen(false);
  }

  const handleHeightForDropdown = (upOrDown: "up" | "down" | "scroll") => {
    if (arrowKeyAllowed == true) {
      if (data) {
        const result = 0;
        let scrollPositionFromBottom = scrollPosition;
        if (upOrDown == "up") {
          const { offsetTop } = divRef.current;

          if (scrollPosition < 0) {
            scrollPositionFromBottom = scrollPosition + 20;
          }
        }
        if (upOrDown == "down") {
          if (isSelectiveIndex * 30 > height) {
            scrollPositionFromBottom =
              height / data.length + scrollPosition - 30;
          }
        }
        if (upOrDown == "scroll") {
          if (window.scrollY > 0) {
            scrollPositionFromBottom = scrollPosition + 20;
          }
        }

        setScrollPosition(scrollPositionFromBottom);
      }
    }
  };
  useEffect(() => {
    if (arrowKeyAllowed == true) {
      if (
        data &&
        data.length > 0 &&
        isArrowKeyPressed?.downArrow !== -1 &&
        isSelectiveIndex + 1 < data.length
      ) {
        handleHeightForDropdown("down");
        setIsSelectiveIndex(isSelectiveIndex + 1);
      }
      if (
        data &&
        data.length > 0 &&
        isArrowKeyPressed?.upArrow !== -1 &&
        isSelectiveIndex - 1 > -1
      ) {
        handleHeightForDropdown("up");

        setIsSelectiveIndex(isSelectiveIndex - 1);
      }
    }
  }, [isArrowKeyPressed]);

  useEffect(() => {
    if (arrowKeyAllowed == true) {
      if (
        EnterClicked &&
        EnterClicked == true &&
        data &&
        isSelectiveIndex >= 0 &&
        isSelectiveIndex < data.length
      ) {
        setSelectedValue(`${data[isSelectiveIndex].value}`);
        setSelectedLabel(data[isSelectiveIndex].label);
        setSelectedIndex(isSelectiveIndex);
      }
    }
  }, [EnterClicked]);

  useEffect(() => {
    setIsSelectiveIndex(-1);
  }, [data]);

  const handleMouseHover = (
    e:
      | React.MouseEvent<HTMLDivElement, MouseEvent>
      | React.MouseEvent<HTMLOptionElement, MouseEvent>
      | any,
    index?: number
  ) => {
    // setIsSelectiveIndex(parseInt(e.target.id))
    // handleHeightForDropdown('scroll')
    if (index) setIsSelectiveIndex(index);
  };
  useEffect(() => {
    const handleScroll = () => {
      if (divRef.current) {
        const currentPosition = divRef.current.scrollTop;
        setScrollPosition(currentPosition);
      }
    };

    if (divRef.current) {
      divRef.current.addEventListener("scroll", handleScroll);
    }

    return () => {
      if (divRef.current) {
        divRef.current.removeEventListener("scroll", handleScroll);
      }
    };
  }, []);

  const handleHover = (index: number) => {
    setIsSelectiveIndex(index);
  };

  const handleLeave = () => {
    setIsSelectiveIndex(-1);
  };
  return {
    handleHover,
    handleLeave,
    divRef,
    selectedValue,
    selectedLabel,
    onDropDownValueSelect,
    isSelectiveIndex,
    selectedIndex,
    handleMouseHover,
    scrollPosition,
    setScrollPosition,
    handleHeightForDropdown,
  };
}

export default useOneDropDownController;
