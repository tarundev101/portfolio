/* eslint-disable react/jsx-no-undef */
import React from "react";
import { OneInputText } from "../input/oneInput";
import { DropDownWithInputType } from "./OneDropDown.model";
import useDropDownWrapperController from "./DropDownWrapper.controller";
import Down from "assets/SVG/CircleArrowDown.svg";
import Up from "assets/SVG/CircleArrowUp.svg";
export default function DropDownWithInput(props: DropDownWithInputType) {
  const {
    showArrow = true,
    zIndex,
    inputImg,
    width,
    boxStyle,
    placeHolder,
    showPlaceHolderOnly = false,
    autoFocus,
    inputProps,
    inputStyleProps,
    inputTextAllowed = true,
    openDropDown = true,
  } = props;
  const {
    wrapperRef,
    inputValue,
    setinputValue,
    handleOnClickFn,
    DropMenu,
    ShowDropDown,
    dataForDropDown,
    handleOnKeyDownFunction,
    isFIlterOptionForUi,
  } = useDropDownWrapperController(props);

  const imgStyle = () => {
    const style = {
      width: "8%",
      height: "20px",
      position: "absolute",
      top: "22.5px",
    };
    if (inputImg && inputImg.position == "right") {
      return { ...style, right: "0%" };
    }
    return {
      ...style,
      left: "0%",
    };
  };
  const inputStyle = () => {
    const style = {
      zIndex: 100,
      // backgroundColor: "red",
      caretColor: isFIlterOptionForUi == false && "transparent",
      cursor: isFIlterOptionForUi == false && "pointer",
      ...inputStyleProps,
    };

    if (inputImg && inputImg.position == "left") {
      return { ...style, width: "95%", marginLeft: "0px", paddingLeft: "35px" };
    }
    if (!inputImg?.img) {
      return {
        ...style,
        width: "100%",
      };
    }
    return {
      ...style,
      width: "98%",
      marginLeft: "0%",
      // cursor: "pointer",
    };
  };
  const dropdownBoxStyle = () => {
    const style = {
      // zIndex: 100,
      zIndex: zIndex ? zIndex : 100,
    };

    if (inputImg && inputImg.position == "left") {
      return { ...style, width: "100%", marginLeft: "0px" };
    }
    if (!inputImg?.img) {
      return {
        ...style,
        width: "100%",
      };
    }
    return {
      ...style,
      width: "100%",
      marginLeft: "0%",
    };
  };

  return (
    <>
      <div
        style={{
          // zIndex: 100,
          position: "relative",
          width: `${width?.inputWidth}px` || "100%",

          ...boxStyle,
        }}
        ref={wrapperRef}
        onClick={() => handleOnClickFn()}
      >
        <>
          {!inputImg?.img && showArrow == true && (
            <img
              // onClick={() => handleOnClickFn()}
              src={ShowDropDown ? Up : Down}
              alt=""
              style={{ position: "absolute", top: "30%", right: "0%" }}
            />
          )}
        </>
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            cursor: openDropDown == true ? "pointer" : "",
          }}
        >
          <OneInputText
            inputStyleProps={{
              backgroundColor: openDropDown == false ? "#dbdbdb" : "",
              ...(inputStyle() as React.CSSProperties),
            }}
            value={showPlaceHolderOnly == false ? inputValue : placeHolder}
            onChangeFunction={inputTextAllowed ? setinputValue : undefined}
            placeHolder={placeHolder ? placeHolder : ""}
            // onEnterKeyPressFunction={}
            onClickFunction={() => openDropDown == true && handleOnClickFn()}
            onKeyDownFunction={handleOnKeyDownFunction}
            type="text"
            autoFocus={autoFocus}
            {...inputProps}
          />

          {ShowDropDown == true &&
            dataForDropDown &&
            dataForDropDown.length > 0 && (
              <div
                style={{
                  marginTop: "50px",
                  position: "absolute",
                  top: 0,

                  ...(dropdownBoxStyle() as React.CSSProperties),
                }}
              >
                {DropMenu()}
              </div>
            )}
        </div>
        {inputImg?.img && (
          <div style={{ ...(imgStyle() as React.CSSProperties) }}>
            {inputImg?.img}
          </div>
        )}
      </div>
    </>
  );
}
