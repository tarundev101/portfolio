/* eslint-disable no-empty */
import { useCallback, useEffect, useRef, useState } from "react";
import useOneDrop from "./OneDropDown.screen";
import { DropDownWithInputType } from "./OneDropDown.model";
import { useOverLayScreen } from "../OverLayScreenClose/OverlayScreenClose.screen";

function useDropDownWrapperController({
  isDropDownValReq = false,
  dropDownIndexForArray = -1,
  onInputValueChange,
  height,
  rowHeight,
  width,
  data,
  onInPutEnterClick,
  isFIlterOptionForUi = true,
  dropDownDivCss,
  zIndex,
  arrowKeyAllowed = false,
}: DropDownWithInputType) {
  const [ShowDropDown, setShowDropDown] = useState<boolean>(false);
  const [EnterClicked, setEnterClicked] = useState<boolean>(false);

  const [arrowKeyPressed, setArrowKeyPressed] = useState<{
    upArrow: number;
    downArrow: number;
  }>({ upArrow: -1, downArrow: -1 });
  const [dataForDropDown, setDataForDropDown] = useState(data);
  const [inputValue, setinputValue] = useState<string>("");

  const DropDownProps = {
    arrowKeyAllowed: arrowKeyAllowed,
    zIndex: zIndex ? zIndex : 100,
    data: dataForDropDown || null,
    height: height || 200,
    width: width?.dropdowWidth || "180px",
    rowHeight: rowHeight || "20px",
    setIsfilterTableOpen: setShowDropDown,
    isArrowKeyPressed: arrowKeyPressed,
    EnterClicked,
    mainDivStyle: dropDownDivCss,
    onDropDownClick: onInPutEnterClick,
  };
  const { selectedValue, DropMenu, selectedLabel, isSelectiveIndex } =
    useOneDrop({
      position: "relative",
      ...DropDownProps,
    });

  const wrapperRef = useRef<HTMLDivElement>(null);
  useOverLayScreen(wrapperRef, () => {
    setShowDropDown(false);
  });
  useEffect(() => {
    if (onInPutEnterClick && isDropDownValReq == true)
      onInPutEnterClick({
        value: `${selectedValue}`,
        selectedIndex: isSelectiveIndex,
        dropDownIndexForArray: dropDownIndexForArray,
        filteredDataForValue: dataForDropDown || [],
      });
  }, [selectedValue]);
  useEffect(() => {
    if (ShowDropDown == false) {
      if (selectedLabel == inputValue) {
        if (onInputValueChange)
          onInputValueChange({
            value: inputValue,
            dropDownIndexForArray,
          });
        setShowDropDown(false);
      } else {
        setShowDropDown(true);
      }
    }
    if (isFIlterOptionForUi == true) {
      filterdata(inputValue, setDataForDropDown);
    }
  }, [inputValue]);
  useEffect(() => {
    setShowDropDown(false);
    setinputValue(`${selectedLabel}`);
  }, [selectedLabel]);

  const filterdata = useCallback(
    (
      inputValue: string,
      stateforFilter: React.Dispatch<React.SetStateAction<any>>
    ) => {
      if (data) {
        const filteredArrdestination = data.filter((item: any) => {
          if (
            item.label
              .toString()
              .toLowerCase()
              .includes(inputValue.toLowerCase()) ||
            item.value
              .toString()
              .toLowerCase()
              .includes(inputValue.toLowerCase())
          ) {
            return item;
          }
        });

        stateforFilter(filteredArrdestination);
      }
    },
    [data]
  );
  const handleOnClickFn = () => {
    // if (ShowDropDown == false)
    setShowDropDown(!ShowDropDown);
    if (EnterClicked == true) setEnterClicked(false);
  };

  const handleOnKeyDownFunction = (keyPressed: string) => {
    //arrow click
    setEnterClicked(false);

    if (keyPressed == "ArrowUp") {
      setArrowKeyPressed({
        upArrow: arrowKeyPressed.upArrow + 1,
        downArrow: -1,
      });
    }
    if (keyPressed == "ArrowDown") {
      setArrowKeyPressed({
        upArrow: -1,
        downArrow: arrowKeyPressed.downArrow + 1,
      });
    }
    if (keyPressed == "Enter") {
      if (onInPutEnterClick)
        onInPutEnterClick({
          value: inputValue,
          selectedIndex: isSelectiveIndex,
          dropDownIndexForArray: dropDownIndexForArray,
          filteredDataForValue: dataForDropDown || [],
        });
      setEnterClicked(true);
    }
  };

  return {
    wrapperRef,
    inputValue,
    setinputValue,
    handleOnClickFn,
    DropMenu,
    ShowDropDown,
    dataForDropDown,
    handleOnKeyDownFunction,
    isFIlterOptionForUi,
  };
}

export default useDropDownWrapperController;
