import React, { useCallback } from "react";

import { OneDropdownPropsType, useOneDropType } from "./OneDropDown.model";
import useOneDropDownController from "./OneDropDown.controller";
import "./OneDropDown.scss";
import {
  globelStyle,
  oneColor,
  oneFontFamily,
} from "../CommonStyleSheetVariable";
import { OneText } from "../text/OneText";

function useOneDrop({
  data,
  height,
  width,
  rowHeight,
  setIsfilterTableOpen,
  isArrowKeyPressed,
  EnterClicked,
  onDropDownClick,
  position,
  mainDivStyle,
  zIndex,
  arrowKeyAllowed,
}: useOneDropType) {
  const {
    selectedValue,
    selectedLabel,
    onDropDownValueSelect,
    isSelectiveIndex,
    selectedIndex,
    divRef,
    scrollPosition,
    handleHover,
    handleLeave,
  } = useOneDropDownController({
    arrowKeyAllowed,
    setIsfilterTableOpen,
    height,
    data,
    isArrowKeyPressed,
    EnterClicked,
    onDropDownClick,
  });

  function DropDown({
    data,
    width,
    height,
    rowHeight,
    singleRowStyle,
    mainDivStyle,
  }: OneDropdownPropsType) {
    return (
      <div
        ref={divRef}
        style={{
          fontFamily: oneFontFamily.regular,
          width: width ? width : "450px",
          maxHeight: height ? `${height}px` : "350px",
          boxShadow: globelStyle.dropdownBox,
          position: position ? position : "absolute",
          overflowY: "scroll",
          background: "#FFFFFF",
          borderRadius: "3%",
          zIndex: zIndex ? zIndex : 100,
          ...mainDivStyle,
        }}
      >
        <div>
          {data?.map((item, index) => {
            return (
              <div
                onClick={() =>
                  onDropDownValueSelect(item.value, item.label, index)
                }
                onMouseEnter={() => handleHover(index)}
                onMouseLeave={handleLeave}
                // onKeyDown={handleKeyPress}
                key={index}
                style={{
                  transform: `translateY(${scrollPosition}px)`,
                  height: rowHeight ? rowHeight : "20px",
                  fontFamily: oneFontFamily.regular,
                  display: "flex",
                  // fontWeight: 900,
                  alignItems: "center",
                  paddingLeft: "5%",
                  cursor: "pointer",
                  backgroundColor:
                    selectedValue == item.value
                      ? oneColor.button_background_selected
                      : isSelectiveIndex !== -1 && index == isSelectiveIndex
                        ? oneColor.button_background_selected
                        : "",
                  ...singleRowStyle,
                }}
              >
                {item.imgurl && (
                  <img
                    src={item.imgurl}
                    alt="img url"
                    width={"15px"}
                    height={"15px"}
                    style={{ marginRight: "10px" }}
                  />
                )}
                <OneText family={"regular"}>{item.label}</OneText>
              </div>
            );
          })}
        </div>
      </div>
    );
  }
  const DropMenu = useCallback(() => {
    return (
      <DropDown
        data={data}
        width={width}
        height={height}
        rowHeight={rowHeight}
        mainDivStyle={mainDivStyle}
      />
    );
  }, [data, isSelectiveIndex]);
  return {
    selectedValue,
    DropMenu,
    selectedLabel,
    selectedIndex,
    isSelectiveIndex,
  };
}

export default useOneDrop;
