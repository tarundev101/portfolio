import { ReactNode } from "react";
import { OneInputPropsType } from "../input/oneInput";

export type ArrayOfObjectForDropDown = MainObject[];

interface MainObject {
  label: string | number;
  value: string | number;
  imgurl?: string;
}

export interface OneDropdownPropsType
  extends React.HTMLAttributes<HTMLDivElement> {
  boxStyle?: React.CSSProperties;
  inputStyleProps?: React.CSSProperties;
  width?: string | number;
  height?: string | number;
  rowHeight?: string | number;
  data: ArrayOfObjectForDropDown | null;
  background?: {
    backgroundPath: string;
    backgroundPosition: "left" | "right";
  };
  placeHolder?: string;
  type?: string;
  singleRowStyle?: React.CSSProperties;
  mainDivStyle?: React.CSSProperties;
}
export interface onInPutEnterClickType {
  value: string;
  dropDownIndexForArray?: number;
  selectedIndex?: string | number;
  filteredDataForValue?: {
    label: number | string;
    value: number | string;
  }[];
}
export interface onInputValueChangeType {
  value: string;
  dropDownIndexForArray: number;
}
export interface DropDownWithInputType {
  showArrow?: boolean;
  arrowKeyAllowed?: useOneDropType["arrowKeyAllowed"];
  showPlaceHolderOnly?: boolean;
  openDropDown?: boolean;
  zIndex?: useOneDropType["zIndex"];
  inputStyleProps?: React.CSSProperties;
  onInPutEnterClick?: ({
    value,
    selectedIndex,
    dropDownIndexForArray,
  }: onInPutEnterClickType) => void;
  inputTextAllowed?: boolean;
  onInputValueChange?: ({ value }: onInputValueChangeType) => void;
  height?: number;
  width?: { inputWidth?: number; dropdowWidth?: string };
  rowHeight?: string;
  autoFocus?: boolean;
  data?: onInPutEnterClickType["filteredDataForValue"];
  inputProps?: OneInputPropsType;
  isFIlterOptionForUi?: boolean;
  dropDownIndexForArray?: number;
  isDropDownValReq?: boolean;
  placeHolder?: string;
  boxStyle?: React.CSSProperties;
  inputImg?: {
    position: "left" | "right";
    img?: ReactNode;
    imgStyle?: React.CSSProperties | undefined;
  };
  dropDownDivCss?: React.CSSProperties | undefined;
}
export interface useOneDropType {
  arrowKeyAllowed?: boolean;

  zIndex?: number;
  position?: "absolute" | "relative" | "fixed";
  mainDivStyle?: React.CSSProperties | undefined;
  data: ArrayOfObjectForDropDown | null;
  height: number;
  width: string | number;
  rowHeight: string;
  setIsfilterTableOpen: React.Dispatch<React.SetStateAction<boolean>>;
  isArrowKeyPressed?: useOneDropDownControllerType["isArrowKeyPressed"];
  EnterClicked?: boolean;
  onDropDownClick?: useOneDropDownControllerType["onDropDownClick"]; //same as enter event from input
}

export interface useOneDropDownControllerType {
  arrowKeyAllowed: useOneDropType["arrowKeyAllowed"];
  setIsfilterTableOpen: useOneDropType["setIsfilterTableOpen"];
  isArrowKeyPressed?: {
    upArrow: number;
    downArrow: number;
  };
  data: OneDropdownPropsType["data"];
  height: number;
  EnterClicked?: boolean;
  onDropDownClick?: ({
    value,
    selectedIndex,
  }: onInPutEnterClickType) => void | undefined;
}
