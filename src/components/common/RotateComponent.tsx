//
import React, { ReactNode } from "react";

export enum IRotateChildren {
  DEFAULT = 0,
  LEFT = 270,
  RIGHT = 90,
  DOWN = 180,
}
export interface IrotateComponentProps {
  children?: ReactNode;
  rotate: IRotateChildren;
}
export const RotateComponent = (props: IrotateComponentProps) => {
  const { children, rotate } = props;

  return <div style={{ transform: `rotate(${rotate}deg)` }}>{children}</div>;
};
