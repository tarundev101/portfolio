// import Cross from '../../../../assets/SVG/Cross.svg'
import "./GenricPopUpStyle.scss";
import crossIcon from "assets/SVG/Cross.svg";
import { ReactNode } from "react";

type props = {
  closeAllowed?: boolean;
  open: boolean;
  children: ReactNode;
  containerWidth?: string;
  containerHeight?: string;
  closeModal: React.Dispatch<React.SetStateAction<boolean>>;
  animationside?: "left" | "right" | "bottom" | "top";
  zIndex?: number; //it should be above 100
};
export const GennricPopUp = (props: props) => {
  const {
    open,
    children,
    containerWidth,
    containerHeight,
    closeModal,
    animationside,
    zIndex,
    closeAllowed = true,
  } = props;
  if (!open) {
    return null;
  }
  return (
    <div
      className="modal-overlay"
      style={{
        position: "fixed",
        top: "0",
        left: "0",
        bottom: "0",
        right: "0",
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        background: "rgba(0, 0, 0, 0.2)",
        zIndex: zIndex ? zIndex : 100,
      }}
      onClick={() => {
        if (closeAllowed == true) {
          closeModal(false);
        }
      }}
    >
      <div
        className="modal-box"
        onClick={(e) => e.stopPropagation()}
        style={{
          // minWidth: 1100,
          width: containerWidth ? containerWidth : "60%",
          height: containerHeight ? containerHeight : "60%",
          overflowX: "hidden",
          overflowY: "scroll",
          backgroundColor: "white",
          borderRadius: "1rem",
          animation: `${animationside || ""}.3s linear`,
          zIndex: 1,
          /* display: block; */
          /* padding: 1rem; */
          // border-radius: 1rem;
        }}
      >
        <div
          style={{
            paddingLeft: "0.1rem",
            position: "relative",
          }}
        >
          {closeAllowed == true && (
            <div
              style={{
                top: "1.5rem",
                right: "2rem",
                cursor: "pointer",
                position: "absolute",
                zIndex: zIndex ? zIndex : 100,
              }}
              onClick={() => {
                closeModal(false);
              }}
            >
              <img src={crossIcon} />
            </div>
          )}
        </div>
        <div style={{ margin: "0rem" }}>{open && children}</div>
      </div>
    </div>
  );
};
