// import Cross from '../../../../assets/SVG/Cross.svg'
import "./GenricPopUpStyle.scss";
import crossIcon from "./../../../assets/SVG/Cross.svg";
import MaximizeScreen from "./../../../assets/SVG/MaximizeScreen.svg";
import MiniIcom from "./../../../assets/png/mini.png";
import leftArrow from "./../../../assets/png/left.png";

import {
  PopupPageWithMinimisePropsType,
  usePageMinimiseController,
} from "./PopupPageWithMinimise.cotroller";
import { IRotateChildren, RotateComponent } from "../RotateComponent";
import { oneColor } from "../CommonStyleSheetVariable";

export const PopUpPageMinimise = (props: PopupPageWithMinimisePropsType) => {
  const {
    open,
    children,
    containerWidth,
    containerHeight,
    closeModal,
    animationside,
  } = props;

  const {
    minimise,
    placementAlign,
    setPlacementAlign,
    setminimise,
    handleAlignLeftOrRight,
    config,
  } = usePageMinimiseController(props);

  if (!open) {
    return null;
  }
  const controlButton = () => {
    return (
      <>
        <div
          style={{
            paddingLeft: "0.1rem",
            zIndex: 10,
          }}
        >
          <div
            style={{
              top: config.controlButton.top,
              right: config.controlButton.right,
              cursor: config.cursor,
              position: "absolute",
              display: "flex",
              flexDirection: "row",
            }}
          >
            {placementAlign == "left" ? (
              <div
                onClick={() => setPlacementAlign("right")}
                style={{
                  width: "20px",
                  height: "20px",
                  position: "absolute",
                  right: "50px",
                  bottom: "10px",
                }}
              >
                <RotateComponent rotate={IRotateChildren.DOWN}>
                  <img
                    style={{ width: "100%", height: "100%" }}
                    src={leftArrow}
                  />
                </RotateComponent>
                {/* right */}
              </div>
            ) : (
              <div
                onClick={() => setPlacementAlign("left")}
                style={{
                  width: "20px",
                  height: "20px",
                  position: "absolute",
                  right: "50px",
                  bottom: "5px",
                }}
              >
                <img
                  style={{ width: "100%", height: "100%" }}
                  src={leftArrow}
                />
                {/* left */}
              </div>
            )}
            {minimise == false ? (
              <div
                onClick={() => setminimise(true)}
                style={{
                  width: "26px",
                  height: "26px",
                  position: "absolute",
                  bottom: "2px",
                  right: "22px",
                }}
              >
                <img style={{ width: "100%", height: "100%" }} src={MiniIcom} />
                {/* min */}
              </div>
            ) : (
              <div
                onClick={() => setminimise(false)}
                style={{
                  width: "37px",
                  height: "37px",
                  position: "absolute",
                  bottom: "-3px",
                  right: "15px",
                }}
              >
                <img
                  style={{ width: "100%", height: "100%" }}
                  src={MaximizeScreen}
                />
                {/* max */}
              </div>
            )}

            <div onClick={() => closeModal(false)}>
              <img src={crossIcon} />
            </div>
          </div>
        </div>
      </>
    );
  };
  return (
    <div
      className="modalPopup-overlay"
      style={{
        position: "fixed",
        bottom: "0",
        right: "0%",
        display: "flex",
        width: config.modelOverlay.width,
        height: config.modelOverlay.height,
        zIndex: 100,
        animation: `${animationside || ""}.3s linear`,
        transition: "all 1s ease",

        ...handleAlignLeftOrRight(),
      }}
    >
      {controlButton()}

      <div
        className="modal-box"
        onClick={(e) => e.stopPropagation()}
        style={{
          boxShadow: `  0 0 10px ${oneColor.div_boxShadow}`,

          width: containerWidth ? containerWidth : "96%",
          height: containerHeight ? containerHeight : "96%",
          overflowX: "hidden",
          overflowY: "scroll",
          backgroundColor: "white",
          borderRadius: "1rem",
          zIndex: 1,
        }}
      >
        <div style={{ margin: "0rem" }}>
          {open && minimise == false && children}
        </div>
      </div>
    </div>
  );
};
