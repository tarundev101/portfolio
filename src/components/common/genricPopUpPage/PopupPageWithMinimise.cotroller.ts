import React, { useState, ReactNode } from "react";

export type PopupPageWithMinimisePropsType = {
  open: boolean;
  children: ReactNode | any;
  containerWidth?: string;
  containerHeight?: string;
  closeModal: React.Dispatch<React.SetStateAction<boolean>>;
  animationside?: "left" | "right" | "bottom" | "top";
  placement?: "left" | "right";
};
export const usePageMinimiseController = ({
  placement = "right",
}: PopupPageWithMinimisePropsType) => {
  const [minimise, setminimise] = useState<boolean>(false);
  const [placementAlign, setPlacementAlign] =
    useState<PopupPageWithMinimisePropsType["placement"]>(placement);
  const handleAlignLeftOrRight = () => {
    if (placementAlign == "right")
      return {
        right: "0%",
      };
    if (placementAlign == "left") return { left: "0%" };
  };
  const config = {
    modelOverlay: {
      width: minimise == false ? "30%" : "10%",
      height: minimise == false ? "100vh" : "30px",
    },
    controlButton: {
      top: minimise == false ? "1.5rem" : "0.2rem",
      right: "1.3rem",
      marginRight: "10px",
    },
    cursor: "pointer",
  };
  return {
    minimise,
    setminimise,
    placementAlign,
    setPlacementAlign,
    handleAlignLeftOrRight,
    config,
  };
};
