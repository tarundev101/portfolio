import { paramsIdType } from "components/common/nav/RoutingHandling";

export const handleCreatingPath = (idFromParamsTab: paramsIdType | null) => {
  if (idFromParamsTab?.t_id4)
    return `/${idFromParamsTab?.t_id1}/${idFromParamsTab?.t_id2}/${idFromParamsTab?.t_id3}/${idFromParamsTab?.t_id4}`;
  if (idFromParamsTab?.t_id3)
    return `/${idFromParamsTab?.t_id1}/${idFromParamsTab?.t_id2}/${idFromParamsTab?.t_id3}`;
  if (idFromParamsTab?.t_id2)
    return `/${idFromParamsTab?.t_id1}/${idFromParamsTab?.t_id2}`;
  if (idFromParamsTab?.t_id1) return `/${idFromParamsTab?.t_id1}`;
};
