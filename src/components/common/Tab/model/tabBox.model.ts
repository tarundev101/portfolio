import { componentProps } from "components/Table/TableComponent";

interface useTabboxProps {
  tabs: Tab[];
  type: TabType;
  initialTabIndex: number;
}
export enum TabType {
  Small = "small",
  Medium = "medium",
  Rectangle = "rectangle",
  Chip = "chip",
}
export interface Tab {
  id: string;
  label: componentProps | string;
  content: React.ReactNode;
  type?: "node";
}
export type labelType = componentProps;
export interface useTabBoxControllerProps {
  tabs: Tab[];
  type: TabType;
  initialTabIndex: number;
  tabNestedIndex: number;
}
export interface nestedSelectedTabType {
  t_id1: string;
  t_id2: string | null;
  t_id3: string | null;
  t_id4: string | null;
}
