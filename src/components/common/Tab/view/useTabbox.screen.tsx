import { OneText } from "components/common/sidenav/OneText";
import React, { useMemo } from "react";
import { useTabBoxController } from "./useTabbox.controller";
import { Tab, TabType } from "../model/tabBox.model";
import { TableComponent } from "components/Table/TableComponent";
import { Color, oneColor } from "components/common/CommonStyleSheetVariable";

interface TabProps {
  tabs: Tab[];
  type?: "small" | "medium" | "rectangle";
  initialTabIndex?: number | string;
}
interface sizeTabProps extends TabProps {
  OnClick: (
    id: string,
    e?: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => void;
  activid: string;
  isMedium?: boolean;
}
const MediumSizeTab = ({ tabs, activid, OnClick, isMedium }: sizeTabProps) => {
  return (
    <>
      {tabs.map((tab, index) => {
        return (
          <div
            key={tab.id}
            onClick={(e) => OnClick(tab.id, e)}
            style={{
              display: "inline-flex",
              flexDirection: "column",
              marginRight: isMedium ? "40px" : "30px",
              alignItems: "center",
            }}
          >
            {typeof tab.label == "string" ? (
              <>
                <OneText
                  family="medium"
                  size={isMedium ? "m" : "l"}
                  color={activid == tab.id ? "blue" : "lightGrey"}
                  textStyle={{
                    padding: isMedium ? "20px 0px" : "10px 0px",
                  }}
                >
                  {tab.label}
                </OneText>
                <div
                  style={{
                    width: isMedium ? "24px" : "19px",
                    height: "4px",
                    backgroundColor:
                      activid == tab.id ? "#4a7ff7" : "transparent",
                    borderTopLeftRadius: "10px",
                    borderTopRightRadius: "10px",
                    borderBottomLeftRadius: isMedium ? "0px" : "10px",
                    borderBottomRightRadius: isMedium ? "0px" : "10px",
                    // transition: 'ease-in 0.4s',
                  }}
                />
              </>
            ) : (
              <>
                <>
                  <TableComponent
                    labelProps={{
                      color: activid == tab.id ? "blue" : "lightGrey",
                    }}
                    {...tab.label}
                    containerStyle={{
                      backgroundColor:
                        activid == tab.id ? Color.p_blue_50 : Color.s_grey_50,
                      ...tab.label.containerStyle,
                    }}
                  />
                </>
              </>
            )}
          </div>
        );
      })}
    </>
  );
};

const ChipTab = ({ tabs, activid, OnClick, isMedium }: sizeTabProps) => {
  return (
    <>
      {tabs.map((tab, index) => {
        return (
          <div
            key={tab.id}
            onClick={(e) => OnClick(tab.id, e)}
            style={{
              display: "inline-flex",
              flexDirection: "column",
              marginRight: "24px",
              alignItems: "center",
            }}
          >
            {typeof tab.label == "string" ? (
              <>
                <div
                  style={{
                    backgroundColor:
                      activid == tab.id ? Color.p_blue_50 : Color.s_grey_50,
                    // height: '30px',
                    padding: "8px 16px",
                    borderRadius: "20px",
                  }}
                >
                  <OneText
                    family="medium"
                    size={isMedium ? "m" : "l"}
                    color={activid == tab.id ? "blue" : "lightGrey"}
                    textStyle={
                      {
                        // padding: '10px 0px',
                      }
                    }
                  >
                    {tab.label}
                  </OneText>
                </div>
                {/* <div
                  style={{
                    width: isMedium ? '24px' : '19px',
                    height: '4px',
                    backgroundColor:
                      activid == tab.id ? '#4a7ff7' : 'transparent',
                    borderTopLeftRadius: '10px',
                    borderTopRightRadius: '10px',
                    borderBottomLeftRadius: isMedium ? '0px' : '10px',
                    borderBottomRightRadius: isMedium ? '0px' : '10px',
                    // transition: 'ease-in 0.4s',
                  }}
                /> */}
              </>
            ) : (
              <>
                <>
                  <TableComponent
                    labelProps={{
                      color: activid == tab.id ? "blue" : "lightGrey",
                    }}
                    {...tab.label}
                    containerStyle={{
                      backgroundColor:
                        activid == tab.id ? Color.p_blue_50 : Color.s_grey_50,
                      ...tab.label.containerStyle,
                    }}
                  />
                </>
              </>
            )}
          </div>
        );
      })}
    </>
  );
};

export const useTabbox = (
  // useTabboxProps:useTabboxProps
  tabs: Tab[],
  type: TabType = TabType.Medium,
  initialTabIndex = 0,
  tabNestedIndex = 0
) => {
  const props = { tabs, type, initialTabIndex, tabNestedIndex };
  const { previousPath, previousUrlPath, activeTab, handleTabClick } =
    useTabBoxController(props);

  const TabBar = useMemo(() => {
    switch (type) {
      case TabType.Medium:
        return (
          <MediumSizeTab
            tabs={tabs}
            activid={activeTab}
            OnClick={handleTabClick}
            isMedium={true}
          />
        );
      case TabType.Chip:
        return (
          <ChipTab
            tabs={tabs}
            activid={activeTab}
            OnClick={handleTabClick}
            isMedium={true}
          />
        );
    }
  }, [type, activeTab, tabs]);
  const Content = useMemo(() => {
    // const activeSelectedTab = tabs.filter((tab) => tab.id === activeTab)
    // handleFilterSelectedTab()
    return tabs.find((tab) => tab.id === activeTab)?.content;
  }, [activeTab, tabs]);
  // const Content = () => {
  //   return tabs.find((tab) => tab.id === activeTab)?.content
  // }
  return {
    TabBar,
    Content,
    activeTab,
  };
};
