import { useEffect, useState } from "react";
import { useLocation, useParams } from "react-router-dom";
import { useTabBoxControllerProps } from "../model/tabBox.model";
import {
  handleTabRouting,
  paramsIdType,
} from "components/common/nav/RoutingHandling";

export const useTabBoxController = (props: useTabBoxControllerProps) => {
  const [previousPath, setPreviousPath] = useState("");
  const [previousUrlPath, setPreviousUrlPath] = useState("");
  let { initialTabIndex } = props;
  const { tabs, type, tabNestedIndex } = props;
  const location = useLocation();
  // const [idFromParamsTab, setIdFromParamsTab] = useState<paramsIdType | null>(
  //   null
  // )

  const handleParam = ({ t_id1, t_id2, t_id3, t_id4 }: paramsIdType) => {
    return handleTabRouting({ t_id1, t_id2, t_id3, t_id4 }, tabNestedIndex);
  };

  const { t_id1, t_id2, t_id3, t_id4 } = useParams();
  initialTabIndex =
    handleParam({ t_id1, t_id2, t_id3, t_id4 }) || initialTabIndex;
  const [activeTab, setActiveTab] = useState<string>(tabs[initialTabIndex]?.id);

  useEffect(() => {
    const pathBeforeParams = location.pathname.split("/:")[0];
    setPreviousPath(pathBeforeParams);
    setPreviousUrlPath(window.location.href);
  }, []);

  // useEffect(() => {
  //   const currentPath = window.location.pathname

  //   if (previousPath.includes(currentPath) && idFromParamsTab) {
  //     const index = handleParam(idFromParamsTab)

  //     if (index) {
  //       setActiveTab(tabs[index]?.id)
  //     }
  //   }
  // }, [idFromParamsTab])

  const handleTabClick = (
    id: string,
    e?: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => {
    // e ? e.stopPropagation() : null;
    e?.preventDefault();
    setActiveTab(id);
  };
  return { previousPath, previousUrlPath, activeTab, handleTabClick };
};
