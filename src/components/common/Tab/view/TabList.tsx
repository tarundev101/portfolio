import React, { useState } from 'react'

interface TabListProps {
  tabs: TabData[]
}
export interface TabData {
  label: string
  content: React.ReactNode
}

interface TabProps {
  label: string
  active: boolean
  onClick: () => void
}

const Tab: React.FC<TabProps> = ({ label, active, onClick }) => {
  const tabStyles: React.CSSProperties = {
    // Customize styles as per your design
    padding: '10px',
    backgroundColor: active ? 'lightblue' : 'white',
    cursor: 'pointer',
  }

  return (
    <div style={tabStyles} onClick={onClick}>
      {label}
    </div>
  )
}

const TabList: React.FC<TabListProps> = ({ tabs }) => {
  const [activeTab, setActiveTab] = useState(0)

  const handleTabClick = (index: number) => {
    setActiveTab(index)
  }

  return (
    <div>
      <div>
        {tabs.map((tab, index) => (
          <Tab
            key={index}
            label={tab.label}
            active={index === activeTab}
            onClick={() => handleTabClick(index)}
          />
        ))}
      </div>
      <div>{tabs[activeTab].content}</div>
    </div>
  )
}

export default TabList
