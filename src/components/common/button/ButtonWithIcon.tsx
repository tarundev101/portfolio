import React from "react";
import { OneText } from "../text/OneText";
import type { OneTextProps } from "../text/OneText";

import "./../style/hover.css";
import { globelStyle } from "../CommonStyleSheetVariable";

interface Buttonprops extends OneTextProps {
  title: string;
  imgurl?: string;
  onclickHandler?: () => void;
  iconsize?: string;
  titleStyle?: React.CSSProperties;
  btnStyle?: React.CSSProperties;
  iconStyle?: React.CSSProperties;
  classname?: string;
  iconPosition?: "left" | "right";
}

const ButtonWithIcon = ({
  title,
  imgurl,
  onclickHandler,
  iconsize = "15px",
  titleStyle = {
    // size: "m",
    // family: "medium",
    // color: "darkBlack",
  },
  btnStyle,
  iconStyle,
  iconPosition = "left",
  classname,
}: Buttonprops) => {
  return (
    <div
      onClick={onclickHandler}
      className={classname}
      style={{
        display: "inline-flex",
        border: "1px solid #e9efff",
        borderRadius: "10px",
        padding: "10px",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        cursor: "pointer",
        ...btnStyle,
      }}
    >
      {imgurl && iconPosition == "left" && (
        <img
          src={imgurl}
          alt="img url"
          width={iconsize}
          style={{ marginRight: "10px", ...iconStyle }}
        />
      )}

      <div
        style={{
          cursor: "pointer",
          fontWeight: 100,
          fontFamily: globelStyle.fontFamily["regular"],
          // fontSize:
          // font
          // isColorAllowedFromOutside: true,

          ...titleStyle,
        }}
      >
        {title}
      </div>
      {imgurl && iconPosition == "right" && (
        <img
          src={imgurl}
          alt="img url"
          width={iconsize}
          style={{ marginLeft: "10px", ...iconStyle }}
        />
      )}
    </div>
  );
};

export default ButtonWithIcon;
