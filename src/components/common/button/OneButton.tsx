import React from "react";
import { oneColor } from "../CommonStyleSheetVariable";
import { OneText } from "../text/OneText";
export interface Buttonprops
  extends React.ButtonHTMLAttributes<HTMLDivElement> {
  variant?: "outlined" | "filled";
  title: string;
  width?: string;
  boxstyles?: React.CSSProperties;
  textStyle?: React.CSSProperties;
  btnStyle?: React.CSSProperties;
}
const BackgroundColor = {
  filled: oneColor.button_background,
  outlined: oneColor.button_background,
};
const color = {
  outlined: oneColor.button_background,
  filled: oneColor.button_background,
};
export const OneButton = ({
  variant = "outlined",
  title,
  width,
  boxstyles,
  textStyle,
  btnStyle,
  ...btnprops
}: Buttonprops) => {
  return (
    <div
      style={{
        backgroundColor: BackgroundColor[variant],
        cursor: "pointer",
        display: "inline-flex",
        // height: '1.875rem',
        padding: "8px",
        justifyContent: "center",
        alignItems: "center",
        // gap: '0.5rem',
        // color: color[variant],
        borderRadius: "8px",
        // border: '.06px solid',
        marginRight: "10px",
        ...boxstyles,
        ...btnStyle,
      }}
      {...btnprops}
    >
      <OneText
        family="medium"
        size="s"
        textStyle={{
          width: width,
          color: color[variant],
          cursor: "pointer",

          ...textStyle,
        }}
      >
        {title}
      </OneText>
    </div>
  );
};
