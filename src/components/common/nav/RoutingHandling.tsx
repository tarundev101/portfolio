import { useSearchParams } from 'react-router-dom'
interface handleTabRoutingType {
  paramsFromUrl: paramsIdType
  nestedTabIndex: number
}
export interface paramsIdType {
  t_id1: string | undefined | null
  t_id2: string | undefined | null
  t_id3: string | undefined | null
  t_id4: string | undefined | null
}
export const handleTabRouting = (
  paramsFromUrl: handleTabRoutingType['paramsFromUrl'],
  nestedTabIndex: handleTabRoutingType['nestedTabIndex']
) => {
  const navOption = 0

  const id1 = paramsFromUrl.t_id1 ? paramsFromUrl.t_id1 : 0
  const id2 = paramsFromUrl.t_id2 ? paramsFromUrl.t_id2 : 0
  const id3 = paramsFromUrl.t_id3 ? paramsFromUrl.t_id3 : 0
  const id4 = paramsFromUrl.t_id4 ? paramsFromUrl.t_id4 : 0

  switch (nestedTabIndex) {
    case 1:
      return Number(id1)
    case 2:
      return Number(id2)
    case 3:
      return Number(id3)
    case 4:
      return Number(id4)
    default:
      return 0
  }

  return navOption
}
