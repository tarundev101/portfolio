import React, { useRef, useEffect } from "react";

const ScrollComponent = ({ content }: { content?: any }) => {
  // Create a ref for the scrollable div
  const scrollRef = useRef<HTMLDivElement>(null);

  // Function to scroll to the end
  const scrollToBottom = () => {
    if (scrollRef.current) {
      scrollRef.current.scrollIntoView({
        behavior: "smooth",
        block: "end",
        inline: "end",
      });
    }
  };

  // Scroll to bottom when component mounts or content changes
  useEffect(() => {
    scrollToBottom();
  }, [content]);

  return (
    <div style={{ width: "300px", overflowX: "scroll", whiteSpace: "nowrap" }}>
      {/* Scrollable div */}
      <div
        ref={scrollRef}
        style={{ display: "inline-block", minWidth: "100%" }}
      >
        {/* Add your content here */}
        {content}
      </div>
    </div>
  );
};

export default ScrollComponent;
