import React, { ReactNode } from "react";
import { OneText, OneTextProps } from "./OneText";
import { DotHeader } from "./DotHeader";
import { globelStyle, oneFontFamily } from "../CommonStyleSheetVariable";
import { OneBadge, Badgeprops } from "./Badge";

export interface KeyValueTitleStringprops {
  data: KeyvalueData[] | null;
  columnFractions?: string[];
  containerStyle?: React.CSSProperties;
  boxStyle?: React.CSSProperties;
  title?: string | undefined;
  gap?: string;
  vertical?: boolean;
  padding?: boolean;
  titleStyle?: React.CSSProperties;
  subtitleStyle?: React.CSSProperties;
}
export interface KeyvalueData extends Badgeprops {
  key?: string;
  value?: string | null | undefined | number | ReactNode;
  keyTextStyle?: OneTextProps;
  valueTextStyle?: OneTextProps;
  RenderNode?: ReactNode;
}

export const KeyValueTitleString = ({
  data,
  padding = false,
  columnFractions = ["2fr", "3fr", "2fr", "3fr"],
  containerStyle = { margin: padding ? globelStyle.generalPadding : "0px" },
  title,
  gap = "0px 40px",
  boxStyle,
  vertical = false,
  titleStyle,
  subtitleStyle,
}: KeyValueTitleStringprops) => {
  return (
    <div
      style={{
        ...boxStyle,
      }}
    >
      {title && <DotHeader>{title}</DotHeader>}
      <div
        style={{
          display: "grid",
          gridTemplateColumns: columnFractions.join(" "),
          gap: gap,
          ...containerStyle,
        }}
      >
        {data?.map((item, index) => {
          const { RenderNode } = item;
          const datatoreturn = (
            <React.Fragment key={`${index}parentKey`}>
              <OneText
                family="medium"
                size="m"
                color="lightGrey"
                textStyle={{ marginBottom: "10px", ...titleStyle }}
                {...item.keyTextStyle}
                key={`${index}newKey`}
              >
                {item.key}
              </OneText>
              {item.type && (
                <OneBadge
                  type={item.type}
                  badgeStyle={item.badgeStyle}
                  key={`${index}oldKey`}
                >
                  {item.value}
                </OneBadge>
              )}
              {!item.type && (
                <OneText
                  family={`regular`}
                  size="m"
                  color="darkBlack"
                  textStyle={{ marginBottom: "0px", ...subtitleStyle }}
                  {...item.valueTextStyle}
                  key={`${index}oldforKey`}
                >
                  {item.value}
                </OneText>
              )}
            </React.Fragment>
          );
          const dataToShow = RenderNode ? RenderNode : datatoreturn;
          return vertical ? (
            <div key={`mainkey${index}`}>{dataToShow}</div>
          ) : (
            dataToShow
          );
        })}
      </div>
    </div>
  );
};
