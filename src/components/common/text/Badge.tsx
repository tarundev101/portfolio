import React, { ReactNode } from "react";
import { OneText, OneTextProps } from "./OneText";
export interface Badgeprops {
  children?: ReactNode;
  type?: "orange" | "red" | "blue" | "green" | "yellow" | null | undefined;
  badgeStyle?: React.CSSProperties;
  fontFamily?: OneTextProps["family"];
}
export interface badgecolorType {
  orange?: string;
  blue?: string;
  red?: string;
  green?: string;
  yellow?: string;
}
export const badgecolor: badgecolorType = {
  orange: "#F08F64",
  blue: " #4A7FF7 ",
  red: "#F06464",
  green: " #18AA1B",
  yellow: "#E8C400",
};
export const badgeBackgroundColor = {
  orange: "#FEF4F0",
  blue: "#EDF2FE",
  red: "#FEF0F0  ",
  green: "#E8F7E8",
  yellow: "#FFFBE6",
};
export const OneBadge = (props: Badgeprops) => {
  const { children, type, badgeStyle, fontFamily } = props;
  if (!type) return null;
  return (
    <OneText
      family={fontFamily || "medium"}
      size="s"
      textStyle={{
        color: type ? badgecolor[type] : "black",
        backgroundColor: type ? badgeBackgroundColor[type] : "white",
        textAlign: "center",
        borderRadius: "15px",
        padding: "5px 8px",
        display: "inline-block",
        ...badgeStyle,
      }}
    >
      {children}
    </OneText>
  );
};

export type BadgepropsType = keyof Badgeprops;

export function getBadgeType(
  searchedValue: string | null | undefined,
  arrayOfValue: { [key: string]: string[] }
): Badgeprops["type"] | undefined | "blue" {
  if (!searchedValue) {
    return "blue";
  }
  let resultType: Badgeprops["type"] = "blue";
  Object.keys(arrayOfValue).map((item, index) => {
    if (arrayOfValue[item].includes(searchedValue.toLowerCase())) {
      resultType = item as Badgeprops["type"];
    }
  });
  return resultType;
}
