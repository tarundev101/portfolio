import React from "react";
import { Box } from "@mui/material";
import { isDisabled } from "@testing-library/user-event/dist/utils";
// import { getCaptalizeString } from './genericFunction/CapitalizeString'
// import img from './../../assets/svg/Billing.svg';
// import { ReactComponent as billingicon } from "./../../assets/svg/Billing.svg";

interface DividerProps {
  containerWidth: string | number;
  containerHeight?: string | number;
  color?: string;
  topPosition?: string | number;
  dividerStyle?: React.CSSProperties;
  divider?: "dashed" | "solid";
}

const CustomDivider = (props: DividerProps) => {
  const { color, containerWidth, divider = "dashed", dividerStyle } = props;

  return (
    <>
      <div
        style={{
          margin: "auto",
          width: containerWidth ? containerWidth : 0,
          borderBottom: `1px ${divider} ${color ? color : "#96a8b4"}`,
          ...dividerStyle,
        }}
      ></div>
    </>
  );
};

export default CustomDivider;
