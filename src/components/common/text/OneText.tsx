import React from "react";
import { globelStyle } from "../CommonStyleSheetVariable";
import { fontFamily } from "../CommonStyleSheetVariable";

export interface OneTextProps {
  children?: React.ReactNode;
  textStyle?: React.CSSProperties;
  size?: "m" | "s" | "l" | "xl";
  family?: keyof fontFamily;
  isColorAllowedFromOutside?: boolean;
  color?:
    | "black"
    | "darkGrey"
    | "lightGrey"
    | "blue"
    | "green"
    | "darkBlack"
    | "textGrey"
    | "red";

  lineHeight?: number | string;
}

export const OneText = ({
  children,
  size,
  textStyle,
  family,
  color,
  lineHeight,
  isColorAllowedFromOutside = false,
}: OneTextProps) => {
  const FontSize = {
    l: "1.5rem",
    m: "1rem",
    s: "0.8",
    xl: "0.5",
  };
  const FontColour = {
    black: "#000000",
    darkGrey: "#707070",
    lightGrey: "#96a8b4",
    blue: "#4a7ff7",
    green: "#18aa1b",
    darkBlack: "#191919",
    textGrey: "#909090",
    red: "red",
  };
  return (
    <div
      style={{
        // fontSize: size ? FontSize[size] : FontSize["m"],
        fontFamily: family
          ? globelStyle.fontFamily[family]
          : globelStyle.fontFamily["medium"],
        color: color ? FontColour[color] : FontColour["black"],
        lineHeight: lineHeight ? `${lineHeight}px` : "18px",
        padding: 0,
        cursor: "default",
        ...textStyle,
      }}
    >
      {children}
    </div>
  );
};
