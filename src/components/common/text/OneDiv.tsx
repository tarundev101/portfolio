import React from "react";
import { globelStyle } from "../CommonStyleSheetVariable";
export interface OneDivprops extends React.HTMLAttributes<HTMLDivElement> {
  children?: React.ReactNode;
  boxStyle?: React.CSSProperties;
}
export const OneDiv = ({ children, boxStyle, ...boxProps }: OneDivprops) => {
  return (
    <div
      style={{
        backgroundColor: globelStyle.backgroundColor,
        borderRadius: globelStyle.gen_borderRadius_M,
        boxShadow: globelStyle.boxShadow,
        ...boxStyle,
      }}
      {...boxProps}
    >
      {children}
    </div>
  );
};
