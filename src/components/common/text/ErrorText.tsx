import React from "react";
import { fontFamily, globelStyle } from "../CommonStyleSheetVariable";

export interface OneErrorProps {
  children?: string;
  textStyle?: React.CSSProperties;
  size?: "m" | "s" | "l" | "xl";
  family?: keyof fontFamily;
  color?: "red";

  lineHeight?: number | string;
}

export const OneError = ({
  children,
  size,
  textStyle,
  family,
  color,
  lineHeight,
}: OneErrorProps) => {
  const FontSize = {
    l: "16px",
    m: "14px",
    s: "12px",
    xl: "20px",
  };
  const FontColour = {
    red: "red",
  };
  return (
    <>
      {children !== "" && (
        <div
          style={{
            fontSize: size ? FontSize[size] : FontSize["m"],
            fontFamily: family
              ? globelStyle.fontFamily[family]
              : globelStyle.fontFamily["medium"],
            color: color ? FontColour[color] : FontColour["red"],
            lineHeight: lineHeight ? `${lineHeight}px` : "18px",
            padding: 0,
            cursor: "default",
            ...textStyle,
          }}
        >
          {children}
        </div>
      )}
    </>
  );
};
