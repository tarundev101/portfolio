export interface changeKeysForArrayType {
  keystoChanges: { [key: string]: string };
  arrayOfObjtochange?: { [key: string]: any }[] | any[];
}

export const changeKeysForSingleObject = (
  keystoChanges: { [key: string]: string },
  objtochange?: { [key: string]: string } | any
) => {
  const obj: { [key: string]: string } = {};
  Object.keys(keystoChanges).map((key) => {
    obj[keystoChanges[key]] = objtochange ? objtochange[key] : "-";
  });
  return obj;
};
export const changeKeysForObjOfArray = ({
  keystoChanges,
  arrayOfObjtochange,
}: changeKeysForArrayType) => {
  const res = [];
  if (arrayOfObjtochange) {
    console.log(arrayOfObjtochange, "arrayOfObjtochange");

    for (let i = 0; i < arrayOfObjtochange.length; i++) {
      const obj: { [key: string]: string } = {};
      Object.keys(keystoChanges).map((key) => {
        obj[keystoChanges[key]] = arrayOfObjtochange[i]
          ? arrayOfObjtochange[i][key]
          : "-";
      });

      res.push(obj);
    }
  }

  return res;
};
