import { Col } from "antd";

export const globelStyle = {
  box_shadow_color: "1px solid grey",
  box_color: "#E9EFFF",
  display_Flex: "flex",
  dashboardBackgroundColor: "#fafbfb",
  HoverColorForDashboard: "#4a7ff7",
  fontWeight_400: 400,
  fontWeight_900: 900,
  fontWeight_500: 500,
  gen_borderRadius: "10px",
  gen_borderRadius_M: "16px",
  backgroundColor: "white",
  fontSize: 12,
  HeadingFontSize: 14,
  HeadingFontSixteenSize: 16,
  FontSize_18: 18,
  FontSize_17: 18,
  genric_lightBlue: "#ebf8ff",
  genric_padding: "12px",
  genric_paddingThirty: "30px",

  fontColor: "#333",
  fontGreenColor: "#18aa1b",
  fontGreyColor: "grey",
  fontRedColor: "red",

  genricBorder: " 1px solid #E9EFFF",
  genricHeadingColor: "#96A8B4",
  primaryBackGroundColor: "#E6F5FF",
  addonsBackgroundColor: "#ECE6FF",
  addonsTextColor: "#6847D2",
  marginFromTab: "30px",
  marginForBox: "12px",
  paddingForBox: "12px",

  borderRadiousAndColor: {
    borderRadius: "15px",
    border: " 1px solid #E9EFFF",
  },
  genericColumnheader: "#EDF2FE",
  alternaterowcolor: "#FAFBFB",
  tablehovercolor: "#DEE4E8",
  fontFamily: {
    semiBold: "Inter SemiBold",
    medium: "Inter Medium",
    regular: "Inter Regular",
    normal: "Inter",
    bold: "Inter Bold",
  },
  colors: {
    pb900: "#1F3568",
    secGulGrey200: "#CFD7DD",
  },
  boxShadow: "0 0 1px 1px #E9EFFF",
  dropdownBox: "0 0 1px 1px #ACC4FB",
  marginBottom: "24px",
  paddingHorizontal: "24px",
  paddingVertical: "16px",
  dotheaderTopMargin: "16px",
  generalPadding: "16px 24px",
};
export interface fontFamily {
  semiBold: string;
  medium: string;
  regular: string;
  normal: string;
  bold: string;
}
export enum oneFontFamily {
  semiBold = "Inter SemiBold",
  medium = "Inter Medium",
  regular = "Inter Regular",
  normal = "Inter",
  bold = "Inter Bold",
}
export enum Color {
  p_black_50 = "#0c0707",
  p_blue_50 = "#EDF2FE",
  p_blue_20 = "#ebf8ff",

  p_blue_500 = "#4A7FF7",
  s_grey_50 = "#F5F6F8",
  // s_grey_100 = "#F5F6F8",

  grey_20 = "#D3D3D3",
  s_grey_100 = "#D5d5d8",

  s_grey_500 = "#96A8B4",
  p_blue_200 = "#ACC4FB",
  s_white_500 = "#FFFFFF",
  purple_50 = "#F0EDFB",
  overShade_gray_hover = "#EEF1F3",
  overShade_gray_100 = "#DEE4E8",
  yellow_50 = "#FFFBE6",
  red_50 = "#FEF0F0",
  orenge_50 = "#FEF4F0",
  p_gull_grey = "#FAFBFB",

  orange_600 = "#DA825B",
  orange_500 = "#F08F64",
  green_50 = "#E8F7E8",
  green_500 = "#18AA1B",
  p_Black = "#000000",
  m_purple = "#a241d6",
  s_purple = "#9c5fbb",
  l_purple = "#7f05bd",
  l_white = "#ffffff",
  l_black = "#120000",
  l_green_blue_shade = "rgb(94 234 212 / var(--tw-text-opacity))",
}
export enum oneColor {
  pageBackGround = Color.p_black_50,
  transparentBlack = "rgba(45, 212, 191, 0.1)",
  button_background = Color.l_purple,
  button_background_selected = Color.p_blue_200,
  l_green_blue_shade = Color.l_green_blue_shade,
  button_background_hover = Color.m_purple,
  div_Background = Color.p_black_50,
  div_Background_hover = Color.l_black,
  div_boxShadow = Color.s_grey_500,
  button_text = Color.l_white,
  button_text_hover = Color.l_white,
  text = Color.l_white,
}
export enum oneFontWeight {
  weight_500 = "500",
  weight_400 = "400",
  weight_900 = "900",
}
