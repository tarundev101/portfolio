import { OneText } from "components/common/sidenav/OneText";
import { OneDiv } from "components/common/text/OneDiv";
import React from "react";
import { GridColDef } from "@mui/x-data-grid";
import { TableComponent } from "components/Table/TableComponent";
import {
  getFormattedDateForTable,
  oneDateFormatter,
} from "components/common/date/GenricDateConversion";
import { NewPaginationTable } from "components/Table/NewPaginationTable";
import { DotHeader } from "components/common/sidenav/DotHeader";
import { Tooltip } from "@mui/material";
import {
  IExpandableColumn,
  NewExpandabletable,
} from "components/Table/ExpandableTable/NewExpandabletable";
import { getAmount } from "utilities/numbers/numberUtilities";
import { KeyValueTitleString } from "components/common/text/KeyValueTitleString";
import { StickyExpandabletable } from "components/Table/StickyExpandableTable/StickyExpandable.screen";
function StickyTableExample() {
  const dummyDetails = [
    {
      date: 1234567890,
      amount: 20,
      text: "just ",
      longText: "A tale of sofwtare developer is to make some problem solved",
    },
    {
      date: 1234567890,
      amount: 100,

      text: "just ",
      longText: "A tale of sofwtare developer is to make some problem solved",
    },
  ];
  const column: IExpandableColumn[] = [
    {
      fieldName: "date",
      header: "Date",
      align: "left",
      rendercell: (rowData) => {
        return (
          <span className="table-cell-trucate">
            <OneText family="medium" size="m">
              {rowData.date ? getFormattedDateForTable(rowData.date) : "-"}
            </OneText>
          </span>
        );
      },
    },
    {
      fieldName: "amount",
      header: "Amount",
      align: "left",
      rendercell: (rowData) => {
        return (
          <span className="table-cell-trucate">
            <OneText family="medium" size="m">
              {rowData.amount ? getAmount(rowData.amount).actualAmount : "-"}
            </OneText>
          </span>
        );
      },
    },

    {
      fieldName: "qwer",
      header: "Status",
      align: "left",
      rendercell: (rowData) => {
        return (
          <span className="table-cell-trucate">
            <OneText family="medium" size="m">
              <TableComponent chips="eligible" />
            </OneText>
          </span>
        );
      },
    },
  ];
  const requiredDataForExpandedView = [
    {
      key: "Stage",
      value: `Stage`,
    },
    {
      key: "Date",

      value: `-`,
    },
    {
      key: "Initiate Status",

      value: `-`,
    },
  ];
  return (
    <OneDiv
      boxStyle={{
        width: "95%",
        height: "85vh",
        margin: "2% auto",
        position: "relative",
        overflow: "scroll",
      }}
    >
      <DotHeader>Expandabletable Table</DotHeader>
      <div style={{ padding: "20px", width: "100%" }}>
        <StickyExpandabletable
          column={column}
          row={dummyDetails}
          renderComponent={() => (
            <div className="" style={{ width: "100%", backgroundColor: "" }}>
              <KeyValueTitleString
                data={
                  requiredDataForExpandedView ? requiredDataForExpandedView : []
                }
                columnFractions={["1fr", "1fr", "1fr", "1fr"]}
                boxStyle={{
                  backgroundColor: "none",
                  padding: "0px 8px ",
                  boxShadow: "none",
                  marginTop: "24px",
                }}
              />
            </div>
          )}
          Styles={{
            minWidth: 700,
            borderRadius: "10px 10px 0px 0px",
          }}
          maxHeight={400}
            hideFooter
          rowPerPage={6}
        />
      </div>
    </OneDiv>
  );
}

export default StickyTableExample;
