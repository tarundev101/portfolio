import { OneText } from "components/common/sidenav/OneText";
import { OneDiv } from "components/common/text/OneDiv";
import React from "react";
import { GridColDef } from "@mui/x-data-grid";
import { TableComponent } from "components/Table/TableComponent";
import { oneDateFormatter } from "components/common/date/GenricDateConversion";
import { NewPaginationTable } from "components/Table/NewPaginationTable";
import { DotHeader } from "components/common/sidenav/DotHeader";
import { Tooltip } from "@mui/material";
function SimpleTable() {
  const dummyDetails = [
    {
      associationDate: 1234567890,
      text: "just ",
      longText: "A tale of sofwtare developer is to make some problem solved",
    },
    {
      associationDate: 1234567890,
      text: "just ",
      longText: "A tale of sofwtare developer is to make some problem solved",
    },
  ];
  const columnForReferalDetails: GridColDef[] = [
    {
      headerName: "Date",
      sortable: false,
      field: "associationDate",
      renderCell: ({ row }) => {
        return (
          <TableComponent label={`${oneDateFormatter(row.associationDate)}`} />
        );
      },
      flex: 1,
    },
    {
      headerName: "Text",
      sortable: false,
      field: "text",
      renderCell: ({ row }) => {
        const entryMode = row.text;
        if (entryMode == null) {
          return "-";
        }
        return entryMode ? "Reference" : "URL";
      },
      flex: 1,
    },
    {
      field: "longText",
      headerName: "Long Text",
      sortable: false,

      flex: 1,
      renderCell: ({ row }) => {
        return (
          <>
            <Tooltip title={row.longText || "-"}>
              <span>{row.longText || "-"}</span>
            </Tooltip>
          </>
        );
      },
    },
  ];
  return (
    <OneDiv
      boxStyle={{
        width: "95%",
        height: "85vh",
        margin: "2% auto",
        position: "relative",
        overflow: "scroll",
      }}
    >
      <DotHeader>Simple Table</DotHeader>

      <div style={{ padding: "20px", width: "100%" }}>
        <NewPaginationTable
          rows={dummyDetails ? dummyDetails : []}
          columns={columnForReferalDetails}
          specifyId={() => `${Math.random() * 12345}`}
          hideFooter
          noOfrow={dummyDetails ? dummyDetails.length : 0}
        />
      </div>
    </OneDiv>
  );
}

export default SimpleTable;
