import { Tab, TabType } from "components/common/Tab/model/tabBox.model";
import { useTabbox } from "components/common/Tab/view/useTabbox.screen";
import React from "react";
import { globelStyle } from "components/common/CommonStyleSheetVariable";
import SimpleTable from "./SimpleTable";
import ExpandableTableExample from "./ExpandableTableForDummy.screen";
import StickyTableExample from "./StickyTableForDummy.screenn";

function Dashboard() {
  const Tabs: Tab[] = [
    {
      id: "0",
      label: "Simple Table",
      content: <SimpleTable />,
    },
    {
      id: "1",
      label: "Expandable Table",
      content: <ExpandableTableExample />,
    },
    {
      id: "2",
      label: "Sticky Table",
      content: <StickyTableExample />,
    },
  ];
  const { TabBar, Content } = useTabbox(Tabs, TabType.Medium, 0, 1);
  return (
    <div style={{ backgroundColor: globelStyle.dashboardBackgroundColor }}>
      <div
        style={{
          paddingLeft: "40px",
          backgroundColor: globelStyle.backgroundColor,
          position: "fixed",
          top: 0,
          width: "100%",
          zIndex: 1,
        }}
      >
        {TabBar}
      </div>
      <div
        style={{
          height: "1px",
          marginTop: "60px",
        }}
      />
      <div style={{ minHeight: "100vh" }}>{Content}</div>
    </div>
  );
}

export default Dashboard;
