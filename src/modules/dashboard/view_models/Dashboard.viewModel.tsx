import { useCallback } from "react";
import useApiServiceHook from "global/network/useApiServiceHook";
import Dashboard_Service from "../api/services/Dashboard.service";

function useDashboardViewModel() {
  const [getDashboardData, dashboardAllData, dashboardLoading, dashboardError] =
    useApiServiceHook<any>();

  const getDashboardCall = useCallback(async () => {
    await getDashboardData(Dashboard_Service.getAllDetailForDashboard.get());
  }, []);
  return {
    getDashboardCall,
    oneNudgeAllData: dashboardAllData?.data?.data || null,
    dashboardLoading,
    dashboardError,
  };
}
export default useDashboardViewModel;
