export interface IDashboardResponse {
  success: boolean;
  id: string;
}
