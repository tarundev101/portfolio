import { wrapperForApiMethod } from "global/network/apiUtilities";

// this is for API fetch
const Dashboard_Service = Object.freeze({
  getAllDetailForDashboard: {
    get: () => {
      return wrapperForApiMethod({
        method: "get",
        url: `dashboard`,
      });
    },
  },
});
export default Dashboard_Service;
