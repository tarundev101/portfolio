/* eslint-disable react-hooks/rules-of-hooks */
import { useCallback, useContext } from "react";
import { Context as AppContext } from "global/reducers/AppContextReducer";
import useApiServiceHook from "global/network/useApiServiceHook";
import { APP_SERVICES } from "../api/services/App.services";
import { bre_platform_Hosted } from "config/common.config";
import {
  AuthResponseForOnePanel,
  IsetupResponse,
  loginCredentialType,
} from "../api/models/Auth.model";

function useAppNav() {
  const { setLogin } = useContext(AppContext);

  const [
    getLoginForBank,
    loginForBankResponse,
    loginForBankLoading,
    loginForBankError,
  ] = useApiServiceHook<IsetupResponse>();
  const [
    getLoginForOnePanel,
    loginForOnePanelResponse,
    loginForOnePanelLoading,
    loginForOnePanelError,
  ] = useApiServiceHook<AuthResponseForOnePanel>();

  return {
    loginResponse: loginForBankResponse?.data || null, //we can use this if required in future for now we are setting data to context
    loginLoading: loginForBankLoading,
    loginError: loginForBankError,
  };
}

export default useAppNav;
