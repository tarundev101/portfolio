/* eslint-disable no-empty */
import { Context } from "global/reducers/AppContextReducer";
import { useContext } from "react";
import { useNavigate } from "react-router-dom";
// import { Context as AppContext } from "../reducers/AppContextReducer";

// dont't add any useeffect in this controller
function useNavigateForNestedTabController() {
  const navigate = useNavigate();
  const { state: AppState } = useContext(Context);

  const NavigateForNestedTab = (route: string) => {
    // setRoute(AppState.routeKey + 1);
    navigate(`${route}`);
  };
  const setRouteKeyForRefresh = () => {
    // handleCreatingTabUrlForRefresh()
  };

  const defaultHeader = { ...AppState.login };
  return {
    defaultHeader,
    NavigateForNestedTab,
    setRouteKeyForRefresh,
  };
}

export default useNavigateForNestedTabController;
