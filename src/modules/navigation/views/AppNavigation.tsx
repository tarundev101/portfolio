import React, { useContext } from "react";
import { Routes, Route } from "react-router-dom";
// import AppLayout from "layouts/AppLayout";
// import { Context as AppContext } from "global/reducers/AppContextReducer";
import { base_prefix } from "config/common.config";
import AppLayout from "layouts/AppLayout";
// import GraphIndex from "modules/graphEditor/views/index.screen";
import { UrlArray } from "../utils/UrlArray.util";
// import FolderMainScreen from "modules/CreateFolder/views/FolderMain.screen";
import useAppNavigationController from "./AppNavigation.controller";
import Resume from "modules/myResume/views/Resume.main.screen";
import Dashboard from "modules/dashboard/views/Dashboard.screen";
// import CreatingNestedTabRoute from "../utils/Navigation.utils";
// import { UrlArray } from "modules/searchUrl/utils/UrlArray.util";

function AppNavigation() {
  // const { state: AppState } = useContext(AppContext)
  const { isLoadingAll } = useAppNavigationController();

  return (
    <Routes>
      {/* it is used to hide Nav section */}
      <Route path={`${base_prefix}`} element={<Resume />} />

      <Route path={`${base_prefix}`} element={<AppLayout hideSideNav={true} />}>
        {/* <Route
          path={`${base_prefix}${UrlArray.graphFolder}`}
          element={<FolderMainScreen />}
        /> */}
        <Route path={`${base_prefix}`} element={<Dashboard />} />
      </Route>

      <Route path={`${base_prefix}`} element={<AppLayout />}>
        <Route
          path={`${base_prefix}${UrlArray.Dashboard}`}
          element={<Dashboard />}
        />
      </Route>

      {/* <Route path={`${base_prefix}/graph`} element={<GraphIndex />} /> */}

      {/* ----------------- */}
    </Routes>
  );
}

export default AppNavigation;
