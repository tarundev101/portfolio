/* eslint-disable no-empty */
import { useContext, useEffect, useState } from "react";
import useAppNav from "../view_model/AppNavigation.viewModel";

function useAppNavigationController() {
  const [isLoadingAll, setIsLoadingAll] = useState<boolean>(false);

  const { loginLoading, loginError } = useAppNav();

  return { isLoadingAll };
}

export default useAppNavigationController;
