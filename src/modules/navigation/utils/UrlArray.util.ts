import { DropDownWithInputType } from "components/common/dropDown/OneDropDown.model";

export enum UrlArray { // always use camel case for this
  home = "/",
  Dashboard = "dashboard",
}

export const UrlArrForSearch = [
  {
    label: `Home`,
    value: UrlArray.home,
  },
  {
    label: `Dashboard`,
    value: UrlArray.Dashboard,
  },
];
