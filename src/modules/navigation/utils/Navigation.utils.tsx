// import { PermissionAuthForUser } from "navigation/api/models/Auth.model";

interface TabRouteTab {
  // for dynamic route
  path: string;
  element: JSX.Element;
}

const CreatingNestedTabRoute = (
  // for dynamic route
  base_prefix: string,
  path: string,
  component: JSX.Element
) => {
  const pathForRoute = `${base_prefix}/${path}`;
  const createdRoute: TabRouteTab[] = MappingRoute(
    pathForRoute,
    CreateDynamicRouteList,
    component
  );

  return createdRoute;
};

export default CreatingNestedTabRoute;

const CreateDynamicRouteList = [
  // for dynamic route
  "",
  ":t_id1",
  ":t_id1/:t_id2",
  ":t_id1/:t_id2/:t_id3",
  ":t_id1/:t_id2/:t_id3/:t_id4",
  ":t_id1/:t_id2/:t_id3/:t_id4/:t_id5",
];
const MappingRoute = (
  // for dynamic route
  path: string,
  CreateDynamicRouteList: string[],
  component: TabRouteTab["element"]
) => {
  const route = [];
  for (let i = 0; i < CreateDynamicRouteList.length; i++) {
    route.push({
      path: `${path}/${CreateDynamicRouteList[i]}`,
      element: component,
    });
  }
  return route;
};

// export const getUserAccessType = (data: PermissionAuthForUser[]) => {
//   for (let i = 0; i < data.length; i++) {
//     if (data[i].path == ("onex" || "onex-pre-release")) {
//       return data[i].role;
//     }
//   }
// };
