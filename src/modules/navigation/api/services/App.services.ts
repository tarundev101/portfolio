import API_URLS from "global/api/urls";
import apiInstance from "global/network/apiInstance";
import {
  handleApiResponse,
  wrapperForApiMethod,
} from "global/network/apiUtilities";

export const APP_SERVICES = Object.freeze({
  login_bre_for_bank: {
    get: () => {
      const params = {
        // from: "bre",
      };
      return wrapperForApiMethod({
        method: "get",
        url: `${API_URLS.login_setup_For_Bank()}`,
        params,
        statuscode: [200],
        //   body,
      });
      //   };
    },
  },
  login_bre_for_onePanel: {
    get: () => {
      const params = {
        from: "bre",
      };
      return async () => {
        return handleApiResponse(
          await apiInstance.get(API_URLS.login_setup_For_OnePanel(), {
            // for live comment
            // withCredentials: true,

            headers: {
              accept: "application/json",
              // role:'SBM_12345678_ADMIN',
            },
          }),
          [200]
        );
      };
      // return wrapperForApiMethod({
      //   method: "get",
      //   url: `${API_URLS.login_setup_For_OnePanel()}`,
      //   ...params,

      //   otherParams: {
      //     withCredentials: true,

      //     headers: {
      //       accept: "application/json",
      //       // role:'SBM_12345678_ADMIN',
      //     },
      //   },
      //   statuscode: [200],
      //   //   body,
      // });
      //   };
    },
  },
});
