export interface LoginApiResponse {
  result: Result;
  userName: string;
  role: string;
  tenantId: string;
}

export interface Result {
  success: boolean;
}
export interface loginCredentialType {
  // result: Result;
  userName: string;
  role: string;
  tenantId: string;
}

// use below to bank setup api IsetupResponse
export interface IsetupResponse {
  success: boolean;
  id: string;
  meta: ImetaSetup;
  data: Partial<ISetupData>;
}

export interface ISetupData {
  user: string;
  bankLogo: string;
  cardLogo: string;

  favicon: string;
  bankCode: string;
  tenantId: number | string;
  bankName: string;
  mitc: string;
  tnc: string;
  kfs: string;
  privacyPolicy: string;
  productName?: string;
  productType?: string;
  productCode?: string;
  role?: string;
  socEnable: boolean;
  rbiBOEnable: boolean;
  complaintsEnable: boolean;
  fixedDepositEnable: boolean;
  prefectTaskApproval: boolean;
  prefectProcessTaskKeys: Partial<PrefectProcessTaskKey>[];
}
export interface PrefectProcessTaskKey {
  processDefinitionKey: string;
  taskDefinitionKey: string;
}
export interface ImetaSetup {
  took: number;
  status: number;
  request?: any;
}
// ending bank setup api IsetupResponse

// below response from one panel for login

export interface AuthResponseForOnePanel {
  success: boolean;
  data: AuthDataOePannel;
}

export interface AuthDataOePannel {
  jwt: string;
  user: UserAuthForOePanel;
  iat: number;
}

export interface UserAuthForOePanel {
  id: number;
  username: string;
  email: string;
  provider: string;
  confirmed: boolean;
  blocked: boolean;
  createdAt: string;
  updatedAt: string;
  permissions: PermissionAuthForUser[];
}

export interface PermissionAuthForUser {
  id: number;
  path: string;
  role: "read" | "admin";
}

// ending from one panel for login
