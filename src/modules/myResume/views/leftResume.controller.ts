import { getSectionIndex } from "global/reducers/Store";
import React, { useState } from "react";
import bankConsoleIcon from "assets/png/bankconsole.png";
import oldConsoleIcon from "assets/png/oldCxConsole.png";
import newConsoleIcon from "assets/png/newConsole.png";

export const useLeftResumeController = () => {
  const [showCreatePolicyPage, setShowCreatePolicyPage] =
    useState<boolean>(false);
  const selectedSectionIndex = getSectionIndex();
  const section = [
    {
      IMAGE: newConsoleIcon,
    },
    {
      IMAGE: bankConsoleIcon,
    },
    {
      IMAGE: oldConsoleIcon,
    },
  ];
  return {
    section,
    selectedSectionIndex,
    showCreatePolicyPage,
    setShowCreatePolicyPage,
  };
};
