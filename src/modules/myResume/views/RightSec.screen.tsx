import { oneColor } from "components/common/CommonStyleSheetVariable";
import { OneText } from "components/common/sidenav/OneText";
import { ClassName } from "components/common/style/styleEnum";
import { OneDiv } from "components/common/text/OneDiv";
import React, { useEffect, useRef, useState } from "react";

interface ExperienceItem {
  worked: string;
  title: string;
  description: string;
  techUsed: string[];
  navLink: string;
}

function RightSecResume() {
  const cursor = "pointer";
  const experience: ExperienceItem[] = [
    {
      worked: "June 2023 - PRESENT",
      title: " Software Engineer - FPL Technologies ",
      description:
        "As a Frontend Developer at FPL Technologies, I developed the frontend of the Business Rule Engine (BRE) from scratch, enabling the creation of business model rules for the bank's onboarding section and internal tools. I successfully migrated the CX Console 2.0 from JavaScript to TypeScript, significantly enhancing its performance and maintainability. Additionally, I created the Bank Console, which is provided to banks to efficiently resolve customer queries. I also maintain the internal tools section of the CX Console using JavaScript, which displays customer credit card transaction details, aiding our support team in resolving customer issues effectively. Furthermore, I gained proficiency in the Model-View-ViewModel (MVVM) architecture, which has enhanced my frontend development skills.",
      techUsed: [
        "React",
        "TypeScript",
        "Zustand",
        "Jdm Editor",
        "Javascript",
        "GitLab",
        "Loopback",
        "EC2",
      ],
      navLink: "https://www.fplabs.tech/",
    },
    {
      worked: "Aug 2021 - May 2023",
      title: " Software Engineer | Tranie -  MountBlue Technologies",
      description:
        "From December 2022 to May 2023, I worked with FPL Technologies, where I contributed to various projects and enhancements. Prior to this, from December 2021 to December 2022, I developed internal tools for Infoedge. My responsibilities included using Puppeteer for server-side rendering to improve search engine optimization for Google bots, working on the Naukri Data Catalog project, and creating a Self Service Injection Platform from scratch. Additionally, I completed an intensive three-month training in the MERN stack, further enhancing my skills and expertise.",
      techUsed: [
        "Node.js",
        "React",
        "Javascript",
        "GitLab",
        "Html",
        "CSS",
        "SCSS",
        "Chart.js",
        "Puppeteer",
        "React-Ace",
      ],
      navLink: "https://www.mountblue.io/",
    },
    {
      worked: "April 2019 - April 2021",
      title: "Quality Engineer - P.K Industry ",
      description:
        "I was responsible for quality inspection of plastic products throughout the manufacturing process. This included stage-wise inspection, from raw materials to finished products, ensuring high standards and quality control at every step.",
      techUsed: [],
      navLink:
        "https://pkindustriesgurgaon.tradeindia.com/company-profile.html",
    },
  ];

  const screenWidth = window.innerWidth;
  const [currentIndex, setCurrentIndex] = useState<number>(-1);
  const divRefs = useRef<(HTMLDivElement | null)[]>([]);
  console.log(currentIndex, "currentIndex");

  useEffect(() => {
    // const handleScroll = () => {
    //   const topOffset = 100; // Adjust this value as needed
    //   const current = divRefs.current.findIndex((ref) => {
    //     if (ref) {
    //       const { top, bottom } = ref.getBoundingClientRect();
    //       return top <= topOffset && bottom >= topOffset;
    //     }
    //     return false;
    //   });
    //   if (current !== -1) {
    //     setCurrentIndex(current);
    //   }
    // };
    // const scrollContainer = document.getElementById("scroll-container");
    // scrollContainer?.addEventListener("scroll", handleScroll);
    // return () => {
    //   scrollContainer?.removeEventListener("scroll", handleScroll);
    // };
  }, []);

  return (
    <div
      id="scroll-container"
      style={{
        overflow: "scroll",
        width: "85%",
        height: "70vh",
        color: oneColor.text,
      }}
    >
      <>
        {/* {screenWidth < 700 && <OneText color="white">About</OneText>} */}
        <OneText color="white" size="xl">
          ABOUT
        </OneText>
        <OneText size="l" color="white" textStyle={{ marginBottom: "2%" }}>
          Back in 2019, my interest in solving coding problems on the HackerRank
          platform sparked a passion that led me to pursue a career in software
          engineering. My main goal is to create user-friendly and accessible
          web interfaces. Currently, I focus on developing accessible user
          interfaces and business rule engines at FPL Technologies. When I'm not
          at my computer, I enjoy watching sunsets and practicing meditation to
          unwind.
        </OneText>
        <>
          <OneText color="white" size="xl">
            EXPERIENCE
          </OneText>
          {experience.map((item, index) => (
            <div
              key={index}
              ref={(el) => (divRefs.current[index] = el)}
              className={ClassName.black_fade}
              onMouseEnter={() => setCurrentIndex(index)}
              onMouseLeave={() => setCurrentIndex(-1)}
              style={{
                display: "flex",
                justifyContent: "space-between",
                gap: "60px",
                margin: "12px 12px 0 0",
                padding: "12px",
                borderRadius: "12px",
                cursor: cursor,
                backgroundColor:
                  currentIndex === index
                    ? "rgba(255, 255, 255, 0.1)"
                    : "transparent",
              }}
              onClick={() => {
                if (item.navLink.length !== 0)
                  window.open(item.navLink, "_blank");
              }}
            >
              <OneText color="white" textStyle={{ cursor: cursor }}>
                {item.worked}
              </OneText>
              <div style={{ width: "65%", cursor: cursor }}>
                <OneText color="white" textStyle={{ cursor: cursor }}>
                  {item.title}
                </OneText>
                <OneText
                  color="lightGrey"
                  size="l"
                  textStyle={{ margin: "16px 0 16px 0", cursor: cursor }}
                >
                  {item.description}
                </OneText>
                <div
                  style={{
                    display: "flex",
                    gap: "16px ",
                    flexWrap: "wrap",
                    cursor: cursor,
                  }}
                >
                  {item.techUsed.map((techItem, techIndex) => (
                    <OneDiv
                      key={techIndex}
                      style={{
                        color: oneColor.l_green_blue_shade,
                        backgroundColor: oneColor.transparentBlack,
                        padding: "10px 8px",
                        borderRadius: "10px",
                        cursor: cursor,
                        margin: "0 12px 0 0",
                      }}
                    >
                      {techItem}
                    </OneDiv>
                  ))}
                </div>
              </div>
            </div>
          ))}
        </>
      </>
    </div>
  );
}

export default RightSecResume;
