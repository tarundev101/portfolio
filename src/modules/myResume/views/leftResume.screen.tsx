import React, { useState } from "react";
import { oneColor } from "components/common/CommonStyleSheetVariable";
import { OneText } from "components/common/sidenav/OneText";

import github from "assets/png/gitGubLogo.png";
import linkedIn from "assets/png/linkedIn.png";
import instagram from "assets/png/instagram.png";
import gmail from "assets/png/gmail.png";
import { useLeftResumeController } from "./leftResume.controller";

function LeftSecResume() {
  const { section, selectedSectionIndex } = useLeftResumeController();
  const [hoveredIndex, setHoveredIndex] = useState<number | null>(null);
  return (
    <div
      style={{
        position: "relative",
        // width: "100%",
        // backgroundColor: "red",
        // minHeight: "60%",
        // height: "100px",
        color: oneColor.text,
        // padding: "20% 10%",
      }}
    >
      <>
        <OneText size="xxxl" color="white">
          Tarun Dev
        </OneText>
        <OneText size="xl" color="white" textStyle={{ margin: "4% 0" }}>
          Senior Frontend Engineer
        </OneText>
        <OneText size="l" color="white">
          I build pixel-perfect, engaging and accessable digital experiences.
        </OneText>
      </>
      <>
        <OneText size="xl" color="white" textStyle={{ margin: "4% 0" }}>
          PROJECTS
        </OneText>
        <div
          style={{
            display: "flex",
            gap: "8px",
            // flexWrap: "wrap",
            backgroundColor: "rgba(255, 255, 255, 0.1)",
            // backgroundColor: "red",

            borderRadius: "12px",
            padding: "1rem",
            width: "90%",
            height: "30%",
          }}
        >
          {section.map((item, index) => {
            const isHovered = hoveredIndex === index;
            const imageStyle = {
              marginTop: "10px",
              borderRadious: isHovered ? "5px" : "0",
              border: isHovered
                ? `1px solid ${oneColor.l_green_blue_shade}`
                : oneColor.div_Background_hover,

              width: isHovered ? "100%" : "40%", // Increase width on hover
              height: isHovered ? "90%" : "40%", // Increase height on hover
              transition: "all 0.3s ease", // Smooth transition
              transform: isHovered ? "scale(1.2)" : "scale(1)", // Scale effect
              zIndex: isHovered ? 1 : 0, // Bring hovered image to front
            };

            return (
              <div
                key={index}
                style={imageStyle}
                onMouseEnter={() => setHoveredIndex(index)}
                onMouseLeave={() => setHoveredIndex(null)}
              >
                <img
                  src={item.IMAGE}
                  alt=""
                  style={{ width: "100%", height: "100%" }} // Ensure the image covers the div
                />
              </div>
            );
          })}
        </div>
      </>
      <div
        style={{
          width: "70%",
          position: "absolute",
          bottom: "2%",
          // backgroundColor: "red",
          height: "40px",
          marginTop: "4%",
          display: "grid",
          gridTemplateColumns: "repeat(7, 1fr)",
        }}
      >
        {/* link for */}
        <div style={{ width: "40px" }}>
          <img
            src={github}
            height={"100%"}
            width={"100%"}
            alt="github"
            //   style={{ ...commonImgStyle, ...iconStyle }}
          />
        </div>
        <div style={{ width: "40px" }}>
          <img
            src={linkedIn}
            height={"100%"}
            width={"100%"}
            alt="linkedIn"
            //   style={{ ...commonImgStyle, ...iconStyle }}
          />
        </div>
        <div style={{ width: "40px" }}>
          <img
            src={instagram}
            height={"100%"}
            width={"100%"}
            alt="linkedIn"
            //   style={{ ...commonImgStyle, ...iconStyle }}
          />
        </div>
        <div style={{ width: "40px" }}>
          <img
            src={gmail}
            height={"100%"}
            width={"100%"}
            alt="linkedIn"
            //   style={{ ...commonImgStyle, ...iconStyle }}
          />
        </div>
      </div>
    </div>
  );
}

export default LeftSecResume;
