import { oneColor } from "components/common/CommonStyleSheetVariable";
import React from "react";
import LeftSecResume from "./leftResume.screen";
import RightSecResume from "./RightSec.screen";
import dashboardIcon from "assets/JPG/backgroundforResume.jpg";
import "./css/LampLight.css";
import { useMainResumeController } from "./Resume.main.controller";
import { OneDiv } from "components/common/text/OneDiv";
import { OneText } from "components/common/sidenav/OneText";
import useNavigateForNestedTabController from "modules/navigation/views/NavigationForNestedTab.controller";
import { base_prefix } from "config/common.config";
import { UrlArray } from "modules/navigation/utils/UrlArray.util";
function Resume() {
  const { position } = useMainResumeController();
  const { NavigateForNestedTab } = useNavigateForNestedTabController();
  return (
    <div
      style={{
        width: "100vw",
        position: "fixed",
        top: 0,
        backgroundColor: oneColor.pageBackGround,
        height: "100vh",
        backgroundImage: `url(${dashboardIcon})`,
        backgroundSize: "cover",
        backgroundPosition: "center",
        backgroundRepeat: "no-repeat",
        opacity: 0.9,
      }}
    >
      {/* resume */}
      <OneDiv
        onClick={() =>
          NavigateForNestedTab(`${base_prefix}${UrlArray.Dashboard}`)
        }
        boxStyle={{
          position: "absolute",
          top: "2rem",
          right: "2rem",
          padding: "4px 8px",
          backgroundColor: "rgba(255, 255, 255, 0.1)",
          cursor: "pointer",
        }}
      >
        <OneText color="white" textStyle={{ cursor: "pointer" }}>
          Go to Project
        </OneText>
      </OneDiv>
      <div
        // className="lamp-light"
        style={{
          // top: `${position.y}`,
          // left: position.x,
          display: "grid",
          gridTemplateColumns: "repeat(2, 1fr)",
          //   height: "100%",
          // backgroundColor: "red",
          padding: "5% 0% 0 10% ",
          height: "100vh",
        }}
      >
        <LeftSecResume />
        <RightSecResume />
      </div>
    </div>
  );
}

export default Resume;
