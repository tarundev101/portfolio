/* eslint-disable react-hooks/rules-of-hooks */
import { useContext } from "react";
import { Context as AppContext } from "global/reducers/AppContextReducer";
import useApiServiceHook from "global/network/useApiServiceHook";

function useAppNav() {
  const { setLogin } = useContext(AppContext);

  return {
    dummyForResponse: "rookie wolf",
  };
}

export default useAppNav;
